CREATE TABLE `hcup_pat_sex_complete` 
(
  `visitlink` varchar(16) DEFAULT NULL,
  
  `NULL_ct` int(4)  DEFAULT NULL,
  `visit_ct` int(4) DEFAULT NULL, 

  `null_sex_flag` int(1) DEFAULT NULL, 

  UNIQUE KEY `visitlink_UNIQUE` (`visitlink`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO hcup_pat_sex_complete (visitlink)
SELECT    visitlink 
FROM    hcup_pat_visitlink_uni; -- 301 sec

UPDATE hcup_pat_sex_complete
SET NULL_ct = 0;

#2013 - calculating null_ct for sex
UPDATE hcup_pat_sex_complete
INNER JOIN (
  SELECT visitlink, sex_null
  FROM (
    SELECT visitlink,
      IFNULL(SUM(female != 1 AND female != 0), 0) sex_null
    FROM 2013_sid_core
    GROUP BY visitlink
      ) t1
  GROUP BY visitlink) t2
ON hcup_pat_sex_complete.visitlink = t2.visitlink 
OR hcup_pat_sex_complete.visitlink IS NULL AND t2.visitlink IS NULL
SET NULL_ct = IF(NULL_ct IS NULL, 0, NULL_ct) + t2.sex_null;

UPDATE hcup_pat_sex_complete
INNER JOIN (
  SELECT visitlink, sex_null
  FROM (
    SELECT visitlink,
      IFNULL(SUM(female != 1 AND female != 0), 0) sex_null
    FROM 2013_sedd_core
    GROUP BY visitlink
      ) t1
  GROUP BY visitlink) t2
ON hcup_pat_sex_complete.visitlink = t2.visitlink 
OR hcup_pat_sex_complete.visitlink IS NULL AND t2.visitlink IS NULL
SET NULL_ct = IF(NULL_ct IS NULL, 0, NULL_ct) + t2.sex_null;

UPDATE hcup_pat_sex_complete
INNER JOIN (
  SELECT visitlink, sex_null
  FROM (
    SELECT visitlink,
      IFNULL(SUM(female != 1 AND female != 0), 0) sex_null
    FROM 2013_sasd_core
    GROUP BY visitlink
      ) t1
  GROUP BY visitlink) t2
ON hcup_pat_sex_complete.visitlink = t2.visitlink 
OR hcup_pat_sex_complete.visitlink IS NULL AND t2.visitlink IS NULL
SET NULL_ct = IF(NULL_ct IS NULL, 0, NULL_ct) + t2.sex_null;

#2014 - calculating null_cut for sex
UPDATE hcup_pat_sex_complete
INNER JOIN (
  SELECT visitlink, sex_null
  FROM (
    SELECT visitlink,
      IFNULL(SUM(female != 1 AND female != 0), 0) sex_null
    FROM 2014_sid_core
    GROUP BY visitlink
      ) t1
  GROUP BY visitlink) t2
ON hcup_pat_sex_complete.visitlink = t2.visitlink 
OR hcup_pat_sex_complete.visitlink IS NULL AND t2.visitlink IS NULL
SET NULL_ct = IF(NULL_ct IS NULL, 0, NULL_ct) + t2.sex_null;

UPDATE hcup_pat_sex_complete
INNER JOIN (
  SELECT visitlink, sex_null
  FROM (
    SELECT visitlink,
      IFNULL(SUM(female != 1 AND female != 0), 0) sex_null
    FROM 2014_sedd_core
    GROUP BY visitlink
      ) t1
  GROUP BY visitlink) t2
ON hcup_pat_sex_complete.visitlink = t2.visitlink 
OR hcup_pat_sex_complete.visitlink IS NULL AND t2.visitlink IS NULL
SET NULL_ct = IF(NULL_ct IS NULL, 0, NULL_ct) + t2.sex_null;

UPDATE hcup_pat_sex_complete
INNER JOIN (
  SELECT visitlink, sex_null
  FROM (
    SELECT visitlink,
      IFNULL(SUM(female != 1 AND female != 0), 0) sex_null
    FROM 2014_sasd_core
    GROUP BY visitlink
      ) t1
  GROUP BY visitlink) t2
ON hcup_pat_sex_complete.visitlink = t2.visitlink 
OR hcup_pat_sex_complete.visitlink IS NULL AND t2.visitlink IS NULL
SET NULL_ct = IF(NULL_ct IS NULL, 0, NULL_ct) + t2.sex_null;

UPDATE hcup_pat_sex_complete
SET visit_ct = 0;

#calculating visit_ct
UPDATE hcup_pat_sex_complete
INNER JOIN (
  SELECT  visitlink,
          IF(sasd_2013_ct IS NULL, 0, sasd_2013_ct) sasd_2013_ct,
          IF(sedd_2013_ct IS NULL, 0, sedd_2013_ct) sedd_2013_ct,
          IF(sid_2013_ct IS NULL, 0, sid_2013_ct) sid_2013_ct,
          IF(sasd_2014_ct IS NULL, 0, sasd_2014_ct) sasd_2014_ct,
          IF(sedd_2014_ct IS NULL, 0, sedd_2014_ct) sedd_2014_ct,
          IF(sid_2014_ct IS NULL, 0, sid_2014_ct) sid_2014_ct
  FROM  hcup_pat_visitlink_uni
  GROUP BY visitlink
          ) t1
ON hcup_pat_sex_complete.visitlink = t1.visitlink
OR hcup_pat_sex_complete.visitlink IS NULL AND t1.visitlink IS NULL
SET visit_ct = sasd_2013_ct + sedd_2013_ct + sid_2013_ct + sasd_2014_ct + sedd_2014_ct + sid_2014_ct; 

#calculating null_sex_flag
UPDATE hcup_pat_sex_complete
SET null_sex_flag = IF(null_ct = visit_ct, 1, 0);

SELECT COUNT(*), null_sex_flag FROM hcup_pat_sex_complete GROUP BY null_sex_flag; 
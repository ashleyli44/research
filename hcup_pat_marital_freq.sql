CREATE TABLE `hcup_pat_marital_freq` 
(
  `ID` int NOT NULL AUTO_INCREMENT,
  `visitlink` varchar(16) DEFAULT NULL,
  `marital_status` varchar(16) DEFAULT NULL, 
  `daystoevent` int(10) DEFAULT NULL,

  PRIMARY KEY (ID)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*
  Creation of table to hold all visitlinks w/ associated marital_status and daystoevent 
*/
#2013
INSERT INTO hcup_pat_marital_freq (visitlink, marital_status, daystoevent)
SELECT visitlink, maritalstatusub04, CAST(daystoevent AS UNSIGNED) daystoevent
FROM 2013_sid_core
WHERE maritalstatusub04 IS NOT NULL AND maritalstatusub04 <> '';

INSERT INTO hcup_pat_marital_freq (visitlink, marital_status, daystoevent)
SELECT visitlink, maritalstatusub04, CAST(daystoevent AS UNSIGNED) daystoevent
FROM 2013_sedd_core
WHERE maritalstatusub04 IS NOT NULL AND maritalstatusub04 <> '';

INSERT INTO hcup_pat_marital_freq (visitlink, marital_status, daystoevent)
SELECT visitlink, maritalstatusub04, CAST(daystoevent AS UNSIGNED) daystoevent
FROM 2013_sasd_core
WHERE maritalstatusub04 IS NOT NULL AND maritalstatusub04 <> '';

#2014
INSERT INTO hcup_pat_marital_freq (visitlink, marital_status, daystoevent)
SELECT visitlink, maritalstatusub04, CAST(daystoevent AS UNSIGNED) daystoevent
FROM 2014_sid_core
WHERE maritalstatusub04 IS NOT NULL AND maritalstatusub04 <> '';

INSERT INTO hcup_pat_marital_freq (visitlink, marital_status, daystoevent)
SELECT visitlink, maritalstatusub04, CAST(daystoevent AS UNSIGNED) daystoevent
FROM 2014_sedd_core
WHERE maritalstatusub04 IS NOT NULL AND maritalstatusub04 <> '';

INSERT INTO hcup_pat_marital_freq (visitlink, marital_status, daystoevent)
SELECT visitlink, maritalstatusub04, CAST(daystoevent AS UNSIGNED) daystoevent
FROM 2014_sasd_core
WHERE maritalstatusub04 IS NOT NULL AND maritalstatusub04 <> '';
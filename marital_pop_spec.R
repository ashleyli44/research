library(data.table)
library(plyr)
#setting up db
library(RMySQL)

con <- dbConnect(MySQL(), user = "root", password = "", dbname = "hcup", host = "localhost")
rs <- dbSendQuery(con, "select * from hcup_pat_marital_freq")
newDF1 <- fetch(rs, n = -1)

db <- as.data.table(newDF1)
setkey(db, visitlink)
db <- db[order(visitlink, daystoevent)]

db[, ID := NULL]

#reports the last non null marital status for each visitlink
db <- db[db[, .I[which.max(daystoevent)], by = visitlink]$V1]
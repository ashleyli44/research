CREATE TABLE `hcup_comorbidity` 
(
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `visitlink` varchar(16) DEFAULT NULL,
  `dq_flag` int(1) NOT NULL DEFAULT 0,
  #completeness measures
  `dx1` varchar(16) NOT NULL DEFAULT 0,
  `dx2` varchar(16) NOT NULL DEFAULT 0,
  `dx3` varchar(16) NOT NULL DEFAULT 0,
  `dx4` varchar(16) NOT NULL DEFAULT 0,
  `dx5` varchar(16) NOT NULL DEFAULT 0,
  `dx6` varchar(16) NOT NULL DEFAULT 0,
  `dx7` varchar(16) NOT NULL DEFAULT 0,
  `dx8` varchar(16) NOT NULL DEFAULT 0,
  `dx9` varchar(16) NOT NULL DEFAULT 0,
  `dx10` varchar(16) NOT NULL DEFAULT 0,
  `dx11` varchar(16) NOT NULL DEFAULT 0,
  `dx12` varchar(16) NOT NULL DEFAULT 0,
  `dx13` varchar(16) NOT NULL DEFAULT 0,
  `dx14` varchar(16) NOT NULL DEFAULT 0,	
  `dx15` varchar(16) NOT NULL DEFAULT 0,
  `dx16` varchar(16) NOT NULL DEFAULT 0,
  `dx17` varchar(16) NOT NULL DEFAULT 0,
  `dx18` varchar(16) NOT NULL DEFAULT 0,
  `dx19` varchar(16) NOT NULL DEFAULT 0,
  `dx20` varchar(16) NOT NULL DEFAULT 0,
  `dx21` varchar(16) NOT NULL DEFAULT 0,
  `dx22` varchar(16) NOT NULL DEFAULT 0,
  `dx23` varchar(16) NOT NULL DEFAULT 0,
  `dx24` varchar(16) NOT NULL DEFAULT 0,
  `dx25` varchar(16) NOT NULL DEFAULT 0,
  `dx26` varchar(16) NOT NULL DEFAULT 0,
  `dx27` varchar(16) NOT NULL DEFAULT 0,
  `dx28` varchar(16) NOT NULL DEFAULT 0,
  `dx29` varchar(16) NOT NULL DEFAULT 0,

  `2013_sasd` int(1) NOT NULL DEFAULT 0,
  `2013_sid` int(1) NOT NULL DEFAULT 0,
  `2013_sedd` int(1) NOT NULL DEFAULT 0,

  `2014_sasd` int(1) NOT NULL DEFAULT 0,
  `2014_sid` int(1) NOT NULL DEFAULT 0,
  `2014_sedd` int(1) NOT NULL DEFAULT 0,

  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

SET SQL_SAFE_UPDATES = 0;

INSERT INTO hcup_comorbidity (visitlink, dx1, dx2, dx3, dx4, dx5, dx6, dx7, dx8, dx9, dx10, dx11, dx12, dx13, dx14, dx15, dx16, dx17, dx18, dx19, dx20, dx21, dx22, dx23, dx24, dx25, dx26, dx27, dx28, dx29)
SELECT visitlink, dx1, dx2, dx3, dx4, dx5, dx6, dx7, dx8, dx9, dx10, dx11, dx12, dx13, dx14, dx15, dx16, dx17, dx18, dx19, dx20, dx21, dx22, dx23, dx24, dx25, dx26, dx27, dx28, dx29
FROM 2013_sasd_core
WHERE visitlink IS NOT NULL;

INSERT INTO hcup_comorbidity (visitlink, dx1, dx2, dx3, dx4, dx5, dx6, dx7, dx8, dx9, dx10, dx11, dx12, dx13, dx14, dx15, dx16, dx17, dx18, dx19, dx20, dx21, dx22, dx23, dx24, dx25, dx26, dx27, dx28, dx29)
SELECT visitlink, dx1, dx2, dx3, dx4, dx5, dx6, dx7, dx8, dx9, dx10, dx11, dx12, dx13, dx14, dx15, dx16, dx17, dx18, dx19, dx20, dx21, dx22, dx23, dx24, dx25, dx26, dx27, dx28, dx29
FROM 2013_sid_core
WHERE visitlink IS NOT NULL;

INSERT INTO hcup_comorbidity (visitlink, dx1, dx2, dx3, dx4, dx5, dx6, dx7, dx8, dx9, dx10, dx11, dx12, dx13, dx14, dx15, dx16, dx17, dx18, dx19, dx20, dx21, dx22, dx23, dx24, dx25, dx26, dx27, dx28, dx29)
SELECT visitlink, dx1, dx2, dx3, dx4, dx5, dx6, dx7, dx8, dx9, dx10, dx11, dx12, dx13, dx14, dx15, dx16, dx17, dx18, dx19, dx20, dx21, dx22, dx23, dx24, dx25, dx26, dx27, dx28, dx29
FROM 2013_sedd_core
WHERE visitlink IS NOT NULL;

INSERT INTO hcup_comorbidity (visitlink, dx1, dx2, dx3, dx4, dx5, dx6, dx7, dx8, dx9, dx10, dx11, dx12, dx13, dx14, dx15, dx16, dx17, dx18, dx19, dx20, dx21, dx22, dx23, dx24, dx25, dx26, dx27, dx28, dx29)
SELECT visitlink, dx1, dx2, dx3, dx4, dx5, dx6, dx7, dx8, dx9, dx10, dx11, dx12, dx13, dx14, dx15, dx16, dx17, dx18, dx19, dx20, dx21, dx22, dx23, dx24, dx25, dx26, dx27, dx28, dx29
FROM 2014_sasd_core
WHERE visitlink IS NOT NULL;

INSERT INTO hcup_comorbidity (visitlink, dx1, dx2, dx3, dx4, dx5, dx6, dx7, dx8, dx9, dx10, dx11, dx12, dx13, dx14, dx15, dx16, dx17, dx18, dx19, dx20, dx21, dx22, dx23, dx24, dx25, dx26, dx27, dx28, dx29)
SELECT visitlink, dx1, dx2, dx3, dx4, dx5, dx6, dx7, dx8, dx9, dx10, dx11, dx12, dx13, dx14, dx15, dx16, dx17, dx18, dx19, dx20, dx21, dx22, dx23, dx24, dx25, dx26, dx27, dx28, dx29
FROM 2014_sid_core
WHERE visitlink IS NOT NULL;

INSERT INTO hcup_comorbidity (visitlink, dx1, dx2, dx3, dx4, dx5, dx6, dx7, dx8, dx9, dx10, dx11, dx12, dx13, dx14, dx15, dx16, dx17, dx18, dx19, dx20, dx21, dx22, dx23, dx24, dx25, dx26, dx27, dx28, dx29)
SELECT visitlink, dx1, dx2, dx3, dx4, dx5, dx6, dx7, dx8, dx9, dx10, dx11, dx12, dx13, dx14, dx15, dx16, dx17, dx18, dx19, dx20, dx21, dx22, dx23, dx24, dx25, dx26, dx27, dx28, dx29
FROM 2014_sedd_core
WHERE visitlink IS NOT NULL;

UPDATE hcup_comorbidity
INNER JOIN (
  SELECT visitlink
  FROM hcup_pat_visitlink_uni
  WHERE visitlink IS NOT NULL AND sasd_2013_ct IS NOT NULL) t
ON hcup_comorbidity.visitlink = t.visitlink
OR hcup_comorbidity.visitlink IS NULL AND t.visitlink IS NULL
SET 2013_sasd = 1;

UPDATE hcup_comorbidity
INNER JOIN (
  SELECT visitlink
  FROM hcup_pat_visitlink_uni
  WHERE visitlink IS NOT NULL AND sedd_2013_ct IS NOT NULL) t
ON hcup_comorbidity.visitlink = t.visitlink
OR hcup_comorbidity.visitlink IS NULL AND t.visitlink IS NULL
SET 2013_sedd = 1;

UPDATE hcup_comorbidity
INNER JOIN (
  SELECT visitlink
  FROM hcup_pat_visitlink_uni
  WHERE visitlink IS NOT NULL AND sid_2013_ct IS NOT NULL) t
ON hcup_comorbidity.visitlink = t.visitlink
OR hcup_comorbidity.visitlink IS NULL AND t.visitlink IS NULL
SET 2013_sid = 1;

UPDATE hcup_comorbidity
INNER JOIN (
  SELECT visitlink
  FROM hcup_pat_visitlink_uni
  WHERE visitlink IS NOT NULL AND sasd_2014_ct IS NOT NULL) t
ON hcup_comorbidity.visitlink = t.visitlink
OR hcup_comorbidity.visitlink IS NULL AND t.visitlink IS NULL
SET 2014_sasd = 1;

UPDATE hcup_comorbidity
INNER JOIN (
  SELECT visitlink
  FROM hcup_pat_visitlink_uni
  WHERE visitlink IS NOT NULL AND sedd_2014_ct IS NOT NULL) t
ON hcup_comorbidity.visitlink = t.visitlink
OR hcup_comorbidity.visitlink IS NULL AND t.visitlink IS NULL
SET 2014_sedd = 1;

UPDATE hcup_comorbidity
INNER JOIN (
  SELECT visitlink
  FROM hcup_pat_visitlink_uni
  WHERE visitlink IS NOT NULL AND sid_2014_ct IS NOT NULL) t
ON hcup_comorbidity.visitlink = t.visitlink
OR hcup_comorbidity.visitlink IS NULL AND t.visitlink IS NULL
SET 2014_sid = 1;

UPDATE hcup_comorbidity
INNER JOIN (
  SELECT visitlink
  FROM hcup_pat_pop_spec_flagged
  GROUP BY visitlink) t
ON hcup_comorbidity.visitlink = t.visitlink
OR hcup_comorbidity.visitlink IS NULL AND t.visitlink IS NULL
SET dq_flag = 1;

#Utilization Information

#-----------------------------------------BASE Population------------------------------------
#Stats for in-patient services 2013
SELECT SUM(sid_2013_ct), COUNT(sid_2013_ct), AVG(sid_2013_ct), STD(sid_2013_ct) FROM hcup_pat_visitlink_uni WHERE visitlink IS NOT NULL;
#Total number of in-patient hospital visits (2013) = 664,704 
#Total number of in-patients in 2013 = 471,542
#Average number of in-patient hospitalizations (2013): 1.4097 
#Standard Devision of in-patient hospitalizations (2013): 1.1085955922923447

#Stats for in-patient services 2014
SELECT SUM(sid_2014_ct), COUNT(sid_2014_ct), AVG(sid_2014_ct), STD(sid_2014_ct) FROM hcup_pat_visitlink_uni WHERE visitlink IS NOT NULL;
#Total number of in-patient hospital visits (2014) = 642,126
#Total number of in-patients in 2014 = 460,125
#Average number of in-patient hospitalizations (2014) = 1.3955
#Standard Deviation of in-patient hospitalizations (2014) = 1.0649099996237212

#Stats for in-patient services 2013 - 2014
SELECT AVG(IFNULL(sid_2013_ct,0) + IFNULL(sid_2014_ct,0)) as average,
	STD(IFNULL(sid_2013_ct,0) + IFNULL(sid_2014_ct,0) ) AS stdev
FROM hcup_pat_visitlink_uni
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Average number of in-patient hospitalizations = 0.4004
#Standard Deviation of in-patient hospitalizations  = 1.0213579493252645

#Stats for outpatient services 2013
SELECT SUM(sasd_2013_ct), COUNT(sasd_2013_ct), AVG(sasd_2013_ct), STD(sasd_2013_ct) FROM hcup_pat_visitlink_uni WHERE visitlink IS NOT NULL;
#Total number of ambulatory vists in 2013 = 3,331,232
#Total number of ambulatory patients in 2013 = 1,075,498
#Average number visits / patients in 2013 = 3.0974
#Standard Deviation of ambulatory visits / patient (2013) = 3.945958412310803

#Stats for outpatient services 2014
SELECT SUM(sasd_2014_ct), COUNT(sasd_2014_ct), AVG(sasd_2014_ct), STD(sasd_2014_ct) FROM hcup_pat_visitlink_uni WHERE visitlink IS NOT NULL;
#Total number of ambulatory vists in 2014 = 3,385,956
#Total number of ambulatory patients in 2014 = 1,077,155
#Average number patients in 2014 = 3.1434
#Standard Deviation of ambulatory visits / patient (2014) = 4.02589769857356

#Stats for outpatient services 2013 - 2014
SELECT AVG(IFNULL(sasd_2013_ct,0) + IFNULL(sasd_2014_ct,0)) as average,
	STD(IFNULL(sasd_2013_ct,0) + IFNULL(sasd_2014_ct,0) ) AS stdev
FROM hcup_pat_visitlink_uni
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Average = 2.0580
#STD = 4.7435124812950855

#Stats for emergency services 2013
SELECT SUM(sedd_2013_ct), COUNT(sedd_2013_ct), AVG(sedd_2013_ct), STD(sedd_2013_ct) FROM hcup_pat_visitlink_uni WHERE visitlink IS NOT NULL;
#Total number of ED visits in 2013 = 2,184,517
#Total number of ED patients in 2013 = 1,304,604
#Average number of patients in 2013 = 1.6745
#Standard Deviation of ED visits per patient (2013) = 1.6890792025600625

#Stats for the emergency services 2014
SELECT SUM(sedd_2014_ct), COUNT(sedd_2014_ct), AVG(sedd_2014_ct), STD(sedd_2014_ct) FROM hcup_pat_visitlink_uni WHERE visitlink IS NOT NULL;
#Total number of ED visits in 2014 = 2,208,155
#Total number of ED patients in 2014 = 1,310,346
#Average number of patients in 2014 = 1.6852
#Standard Deviation of ED visits per patient (2014) = 1.6911232568644503

#Stats for emergency services 2013 - 2014
SELECT AVG(IFNULL(sedd_2013_ct,0) + IFNULL(sedd_2014_ct,0)) as average,
	STD(IFNULL(sedd_2013_ct,0) + IFNULL(sedd_2014_ct,0) ) AS stdev
FROM hcup_pat_visitlink_uni
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#AVG = 1.3459
#STD = 2.265835191981867

#Stats for entire 2013 and 2014 and all databases
SELECT AVG(IFNULL(sid_2013_ct,0) + IFNULL(sid_2014_ct,0) + IFNULL(sasd_2013_ct,0) + IFNULL(sasd_2014_ct,0) + IFNULL(sedd_2013_ct,0) + IFNULL(sedd_2014_ct,0)) as average,
	STD(IFNULL(sid_2013_ct,0) + IFNULL(sid_2014_ct,0) + IFNULL(sasd_2013_ct,0) + IFNULL(sasd_2014_ct,0) + IFNULL(sedd_2013_ct,0) + IFNULL(sedd_2014_ct,0)) AS stdev
FROM hcup_pat_visitlink_uni
WHERE visitlink IS NOT NULL;
#Avg number of visits / patient 2013 and 2014 = 3.8043
#STD 2013 and 2014 = 5.733681849963741

#Stats for entire 2013 across all databases
SELECT AVG(IFNULL(sid_2013_ct,0) + IFNULL(sasd_2013_ct,0) + IFNULL(sedd_2013_ct,0)) as average,
	STD(IFNULL(sid_2013_ct,0) + IFNULL(sasd_2013_ct,0) + IFNULL(sedd_2013_ct,0)) AS stdev
FROM hcup_pat_visitlink_uni
WHERE visitlink IS NOT NULL;
#Avg number of visits / patient 2013 = 1.8936
#STD 2013 = 3.3077841472489404

#Stats for entire 2014 across all databases
SELECT AVG(IFNULL(sid_2014_ct,0) + IFNULL(sasd_2014_ct,0) + IFNULL(sedd_2014_ct,0)) as average,
	STD(IFNULL(sid_2014_ct,0) + IFNULL(sasd_2014_ct,0) + IFNULL(sedd_2014_ct,0)) AS stdev
FROM hcup_pat_visitlink_uni
WHERE visitlink IS NOT NULL;
#Avg number of visits / patients 2014 = 1.9107
#STD 2014 = 3.3586998605937564

#Average LOS In-patient 2013
SELECT AVG(LOS), STD(LOS) FROM 2013_sid_core WHERE visitlink IS NOT NULL;
#Average LOS = 4.359572198627654
#STD of LOS (2013) = 6.0908512448950916

#Average LOS In-patient 2014
SELECT AVG(LOS), STD(LOS) FROM 2014_sid_core WHERE visitlink IS NOT NULL;
#Average LOS = 4.461206030620302
#STD of LOS (2014) = 6.2529802367598055

#Avg LOS in-patient 2013 and 2014
SELECT AVG(LOS), STD(LOS) FROM
((SELECT LOS from 2013_sid_core where visitlink IS NOT NULL) UNION ALL
	(SELECT LOS FROM 2014_sid_core WHERE visitlink IS NOT NULL)) t;
#Avg LOS = 4.409511150698181
#STD of LOS = 6.171256661489767

#---------------------------------FLAGGED POPULATION---------------------------------------------------------------
#Stats for in-patient services 2013
SELECT SUM(sid_2013_ct), COUNT(sid_2013_ct), AVG(sid_2013_ct), STD(sid_2013_ct) FROM hcup_pat_visitlink_uni 
INNER JOIN hcup_pat_pop_spec_flagged ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_flagged.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Total number of in-patient hospital visits (2013) = 67,316
#Total number of in-patients in 2013 = 43,992
#Average number of in-patient hospitalizations (2013) = 1.5302
#Standard Deviation of in-patient hospitalizations (2013) = 1.4061369471769258

#Stats for in-patient services 2014
SELECT SUM(sid_2014_ct), COUNT(sid_2014_ct), AVG(sid_2014_ct), STD(sid_2014_ct) FROM hcup_pat_visitlink_uni 
INNER JOIN hcup_pat_pop_spec_flagged ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_flagged.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Total number of in-patient hospital visits (2014) = 73,905
#Total number of in-patients in 2014 = 50,994
#Average number of in-patient hospitalizations (2014) = 1.4493
#Standard Deviation of in-patient hospitalizations (2014) = 1.2224238625033554

#Stats for 2013 and 2014 inpatient services flagged population
SELECT AVG(IFNULL(sid_2013_ct,0) + IFNULL(sid_2014_ct,0)) as average,
	STD(IFNULL(sid_2013_ct,0) + IFNULL(sid_2014_ct,0) ) AS stdev
FROM hcup_pat_visitlink_uni
INNER JOIN hcup_pat_pop_spec_flagged ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_flagged.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Average number of visits / patient in 2013 and 2014 = 0.4595
#STD of visits / patient in 2013 and 2014 = 1.2152916354576475

#Stats for outpatient services 2013
SELECT SUM(sasd_2013_ct), COUNT(sasd_2013_ct), AVG(sasd_2013_ct), STD(sasd_2013_ct) FROM hcup_pat_visitlink_uni 
INNER JOIN hcup_pat_pop_spec_flagged ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_flagged.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Total number of ambulatory vists in 2013 = 333235
#Total number of ambulatory patients in 2013 = 105106
#Average number patients in 2013 = 3.1705
#Standard Deviation of ambulatory visits / patient (2013) = 4.074551627862668

#Stats for outpatient services 2014
SELECT SUM(sasd_2014_ct), COUNT(sasd_2014_ct), AVG(sasd_2014_ct), STD(sasd_2014_ct) FROM hcup_pat_visitlink_uni 
INNER JOIN hcup_pat_pop_spec_flagged ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_flagged.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Total number of ambulatory vists in 2014 = 381,946
#Total number of ambulatory patients in 2014 = 117,729
#Average number patients in 2014 = 3.2443
#Standard Deviation of ambulatory visits / patient (2014) = 4.207322951069803

#Stats for 2013 and 2014 outpatient services flagged population
SELECT AVG(IFNULL(sasd_2013_ct,0) + IFNULL(sasd_2014_ct,0) ) as average,
	STD(IFNULL(sasd_2013_ct,0) + IFNULL(sasd_2014_ct,0)) AS stdev
FROM hcup_pat_visitlink_uni
INNER JOIN hcup_pat_pop_spec_flagged ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_flagged.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Average number of visits / patient in 2013 and 2014 = 2.3269
#STD of visits / patient in 2013 and 2014 = 5.027620211534663

#Stats for emergency services 2013
SELECT SUM(sedd_2013_ct), COUNT(sedd_2013_ct), AVG(sedd_2013_ct), STD(sedd_2013_ct) FROM hcup_pat_visitlink_uni 
INNER JOIN hcup_pat_pop_spec_flagged ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_flagged.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Total number of ED visits in 2013 = 208912
#Total number of ED patients in 2013 = 105201
#Average number of patients in 2013 = 1.9858
#Standard Deviation of ED visits per patient (2013) = 2.4869291609981454

#Stats for the emergency services 2014
SELECT SUM(sedd_2014_ct), COUNT(sedd_2014_ct), AVG(sedd_2014_ct), STD(sedd_2014_ct) FROM hcup_pat_visitlink_uni 
INNER JOIN hcup_pat_pop_spec_flagged ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_flagged.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Total number of ED visits in 2014 = 245462
#Total number of ED patients in 2014 = 131695
#Average number of patients in 2014 = 1.8639
#Standard Deviation of ED visits per patient (2014) = 2.2543921929447306

#Stats for 2013 and 2014 ED flagged population
SELECT AVG(IFNULL(sedd_2013_ct,0) + IFNULL(sedd_2014_ct,0)) as average,
	STD(IFNULL(sedd_2013_ct,0) + IFNULL(sedd_2014_ct,0)) AS stdev
FROM hcup_pat_visitlink_uni
INNER JOIN hcup_pat_pop_spec_flagged ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_flagged.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Average number of visits / patient in 2013 and 2014 = 
#STD of visits / patient in 2013 and 2014 = 

#Stats for 2013 and 2014 flagged population
SELECT AVG(IFNULL(sid_2013_ct,0) + IFNULL(sid_2014_ct,0) + IFNULL(sasd_2013_ct,0) + IFNULL(sasd_2014_ct,0) + IFNULL(sedd_2013_ct,0) + IFNULL(sedd_2014_ct,0)) as average,
	STD(IFNULL(sid_2013_ct,0) + IFNULL(sid_2014_ct,0) + IFNULL(sasd_2013_ct,0) + IFNULL(sasd_2014_ct,0) + IFNULL(sedd_2013_ct,0) + IFNULL(sedd_2014_ct,0)) AS stdev
FROM hcup_pat_visitlink_uni
INNER JOIN hcup_pat_pop_spec_flagged ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_flagged.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Average number of visits / patient in 2013 and 2014 = 4.2648
#STD of visits / patient in 2013 and 2014 = 6.561639135957864

#Stats for 2013 flagged population
SELECT AVG(IFNULL(sid_2013_ct,0) + IFNULL(sasd_2013_ct,0) + IFNULL(sedd_2013_ct,0)) as average,
	STD(IFNULL(sid_2013_ct,0) + IFNULL(sasd_2013_ct,0) + IFNULL(sedd_2013_ct,0) ) AS stdev
FROM hcup_pat_visitlink_uni
INNER JOIN hcup_pat_pop_spec_flagged ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_flagged.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Avg number of visits / patient in 2013 = 1.9830
#STD of visits / patient in 2013 = 3.751363777856248

#Stats for 2014 flagged population
SELECT AVG(IFNULL(sid_2014_ct,0) + IFNULL(sasd_2014_ct,0) + IFNULL(sedd_2014_ct,0)) as average,
	STD(IFNULL(sid_2014_ct,0) + IFNULL(sasd_2014_ct,0) + IFNULL(sedd_2014_ct,0) ) AS stdev
FROM hcup_pat_visitlink_uni
INNER JOIN hcup_pat_pop_spec_flagged ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_flagged.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Avg number of visits / patient in 2014 = 2.2818 
#STD of visits / patient in 2014 = 3.886349099598111

#Average LOS In-patient 2013
SELECT AVG(LOS), STD(LOS) FROM 2013_sid_core 
INNER JOIN hcup_pat_pop_spec_flagged ON 2013_sid_core.visitlink = hcup_pat_pop_spec_flagged.visitlink 
WHERE 2013_sid_core.visitlink IS NOT NULL;
#Average LOS = 4.56995572986303
#STD of LOS (2013) = 6.756061674572421

#Average LOS In-patient 2014
SELECT AVG(LOS), STD(LOS) FROM 2014_sid_core 
INNER JOIN hcup_pat_pop_spec_flagged ON 2014_sid_core.visitlink = hcup_pat_pop_spec_flagged.visitlink 
WHERE 2014_sid_core.visitlink IS NOT NULL;
#Average LOS = 4.658218770296601
#STD of LOS (2014) = 6.9385953306385

#Avg LOS in-patient 2013 and 2014 flagged population
SELECT AVG(LOS), STD(LOS) FROM
((SELECT visitlink, LOS from 2013_sid_core where visitlink IS NOT NULL) UNION ALL
	(SELECT visitlink, LOS FROM 2014_sid_core WHERE visitlink IS NOT NULL)) t
INNER JOIN hcup_pat_pop_spec_flagged ON hcup_pat_pop_spec_flagged.visitlink = t.visitlink;
#Avg LOS = 4.6161466668555
#STD of LOS = 6.852335823732196

#----------------------------------REMAINING POPULATION-----------------------------------

#Stats for 2013 and 2014 across REMAINING population and all services
SELECT AVG(IFNULL(sid_2013_ct,0) + IFNULL(sid_2014_ct,0) + IFNULL(sasd_2013_ct,0) + IFNULL(sasd_2014_ct,0) + IFNULL(sedd_2013_ct,0) + IFNULL(sedd_2014_ct,0)) as average,
	STD(IFNULL(sid_2013_ct,0) + IFNULL(sid_2014_ct,0) + IFNULL(sasd_2013_ct,0) + IFNULL(sasd_2014_ct,0) + IFNULL(sedd_2013_ct,0) + IFNULL(sedd_2014_ct,0)) AS stdev
FROM hcup_pat_visitlink_uni
INNER JOIN hcup_pat_pop_spec_remainder ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_remainder.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Average number of visits 2013 and 2014 = 3.7564
#STD of visits 2013 and 2014 = 5.638482924919535

#Stats for 2013 across all services REMAINING population
SELECT AVG(IFNULL(sid_2013_ct,0) + IFNULL(sasd_2013_ct,0) + IFNULL(sedd_2013_ct,0)) as average,
	STD(IFNULL(sid_2013_ct,0)  + IFNULL(sasd_2013_ct,0) + IFNULL(sedd_2013_ct,0)) AS stdev
FROM hcup_pat_visitlink_uni
INNER JOIN hcup_pat_pop_spec_remainder ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_remainder.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Average number of visits 2013 = 1.8843
#STD of visits 2013 = 3.2580667405430113

#Stats for 2014 across all services REMAINING POPULATION
SELECT AVG( IFNULL(sid_2014_ct,0)  + IFNULL(sasd_2014_ct,0)  + IFNULL(sedd_2014_ct,0)) as average,
	STD( IFNULL(sid_2014_ct,0)  + IFNULL(sasd_2014_ct,0)  + IFNULL(sedd_2014_ct,0)) AS stdev
FROM hcup_pat_visitlink_uni
INNER JOIN hcup_pat_pop_spec_remainder ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_remainder.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Average number of visits 2014 = 1.8721
#STD of visits 2014 = 3.2966114279579815

#Stats for in-patient services 2013 REMAINING POP
SELECT SUM(sid_2013_ct), COUNT(sid_2013_ct), AVG(sid_2013_ct), STD(sid_2013_ct) FROM hcup_pat_visitlink_uni 
INNER JOIN hcup_pat_pop_spec_remainder ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_remainder.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Total number of in-patient hospital visits (2013) = 597,388
#Total number of in-patients in 2013 = 427,532
#Average number of in-patient hospitalizations (2013) = 1.3973
#Standard Deviation of in-patient hospitalizations (2013) = 1.0725412561126124

#Stats for in-patient services 2014 REMAINING POP
SELECT SUM(sid_2014_ct), COUNT(sid_2014_ct), AVG(sid_2014_ct), STD(sid_2014_ct) FROM hcup_pat_visitlink_uni 
INNER JOIN hcup_pat_pop_spec_remainder ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_remainder.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Total number of in-patient hospital visits (2014) = 568,221
#Total number of in-patients in 2014 = 409,131
#Average number of in-patient hospitalizations (2014) = 1.3888
#Standard Deviation of in-patient hospitalizations (2014) = 1.0434186102284781

#Stats for 2013 and 2014 inpatient services REMAINING POP
SELECT AVG(IFNULL(sid_2013_ct,0) + IFNULL(sid_2014_ct,0)) as average,
	STD(IFNULL(sid_2013_ct,0) + IFNULL(sid_2014_ct,0)) AS stdev
FROM hcup_pat_visitlink_uni
INNER JOIN hcup_pat_pop_spec_remainder ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_remainder.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Average number of visits 2013 and 2014 = 0.3943
#STD of visits 2013 and 2014 = 0.998838697171472

#Stats for outpatient services 2013 REMAINING POP
SELECT SUM(sasd_2013_ct), COUNT(sasd_2013_ct), AVG(sasd_2013_ct), STD(sasd_2013_ct) FROM hcup_pat_visitlink_uni 
INNER JOIN hcup_pat_pop_spec_remainder ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_remainder.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Total number of ambulatory vists in 2013 = 2,997,997
#Total number of ambulatory patients in 2013 = 970,392
#Average number patients in 2013 = 3.0895
#Standard Deviation of ambulatory visits / patient (2013) = 3.931696139152804

#Stats for outpatient services 2014 REMAINING POP
SELECT SUM(sasd_2014_ct), COUNT(sasd_2014_ct), AVG(sasd_2014_ct), STD(sasd_2014_ct) FROM hcup_pat_visitlink_uni 
INNER JOIN hcup_pat_pop_spec_remainder ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_remainder.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Total number of ambulatory vists in 2014 = 3,004,010
#Total number of ambulatory patients in 2014 = 959,426
#Average number patients in 2014 = 3.1310
#Standard Deviation of ambulatory visits / patient (2014) = 4.002894033471545

#Stats for 2013 and 2014 outpatient services REMAINING POP
SELECT AVG(IFNULL(sasd_2013_ct,0) + IFNULL(sasd_2014_ct,0)) as average,
	STD(IFNULL(sasd_2013_ct,0) + IFNULL(sasd_2014_ct,0)) AS stdev
FROM hcup_pat_visitlink_uni
INNER JOIN hcup_pat_pop_spec_remainder ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_remainder.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Average number of visits 2013 and 2014 =  2.0301
#STD of visits 2013 and 2014 = 4.712114505083339

#Stats for emergency services 2013 REMAINING POP
SELECT SUM(sedd_2013_ct), COUNT(sedd_2013_ct), AVG(sedd_2013_ct), STD(sedd_2013_ct) FROM hcup_pat_visitlink_uni 
INNER JOIN hcup_pat_pop_spec_remainder ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_remainder.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Total number of ED visits in 2013 = 1,975,605
#Total number of ED patients in 2013 = 1,199,403
#Average number of patients in 2013 = 1.6472
#Standard Deviation of ED visits per patient (2013) = 1.59734172891905

#Stats for the emergency services 2014 REMAINING POP
SELECT SUM(sedd_2014_ct), COUNT(sedd_2014_ct), AVG(sedd_2014_ct), STD(sedd_2014_ct) FROM hcup_pat_visitlink_uni 
INNER JOIN hcup_pat_pop_spec_remainder ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_remainder.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Total number of ED visits in 2014 = 1962693
#Total number of ED patients in 2014 = 1178651
#Average number of patients in 2014 = 1.6652
#Standard Deviation of ED visits per patient (2014) = 1.6148113978555183

#Stats for 2013 and 2014 emergency services REMAINING POP
SELECT AVG(IFNULL(sedd_2013_ct,0) + IFNULL(sedd_2014_ct,0)) as average,
	STD(IFNULL(sedd_2013_ct,0) + IFNULL(sedd_2014_ct,0)) AS stdev
FROM hcup_pat_visitlink_uni
INNER JOIN hcup_pat_pop_spec_remainder ON hcup_pat_visitlink_uni.visitlink = hcup_pat_pop_spec_remainder.visitlink 
WHERE hcup_pat_visitlink_uni.visitlink IS NOT NULL;
#Average number of visits 2013 and 2014 = 1.3321
#STD of visits 2013 and 2014 = 2.1745365374693346

#Average LOS In-patient 2013 REMAINING POP
SELECT AVG(LOS), STD(LOS) FROM 2013_sid_core 
INNER JOIN hcup_pat_pop_spec_remainder ON 2013_sid_core.visitlink = hcup_pat_pop_spec_remainder.visitlink 
WHERE 2013_sid_core.visitlink IS NOT NULL;
#Average LOS = 4.33586602989352
#STD of LOS (2013) = 6.010820348450252

#Average LOS In-patient 2014 REMAINING POP
SELECT AVG(LOS), STD(LOS) FROM 2014_sid_core 
INNER JOIN hcup_pat_pop_spec_remainder ON 2014_sid_core.visitlink = hcup_pat_pop_spec_remainder.visitlink 
WHERE 2014_sid_core.visitlink IS NOT NULL;
#Average LOS = 4.435582055510287
#STD of LOS (2014) = 6.1577371526258124

#Avg LOS in-patient 2013 and 2014 remaining population
SELECT AVG(LOS), STD(LOS) FROM
((SELECT visitlink, LOS from 2013_sid_core where visitlink IS NOT NULL) UNION ALL
	(SELECT visitlink, LOS FROM 2014_sid_core WHERE visitlink IS NOT NULL)) t
INNER JOIN hcup_pat_pop_spec_remainder ON hcup_pat_pop_spec_remainder.visitlink = t.visitlink;
#Avg LOS = 4.384476401116673
#STD of LOS = 6.083088022418996

#----------------------WHOLE POPULATION-------------------------------
SELECT visitlink, IFNULL(dxccs1,0), IFNULL(dxccs2,0), IFNULL(dxccs3,0), IFNULL(dxccs4,0), IFNULL(dxccs5,0), IFNULL(dxccs6,0), IFNULL(dxccs7,0), IFNULL(dxccs8,0), IFNULL(dxccs9,0), IFNULL(dxccs10,0), IFNULL(dxccs11,0), IFNULL(dxccs12,0), IFNULL(dxccs13,0), IFNULL(dxccs14,0), IFNULL(dxccs15,0), IFNULL(dxccs16,0), IFNULL(dxccs17,0), IFNULL(dxccs18,0), IFNULL(dxccs19,0), IFNULL(dxccs20,0), IFNULL(dxccs21,0), IFNULL(dxccs22,0), IFNULL(dxccs23,0), IFNULL(dxccs24,0), IFNULL(dxccs25,0), IFNULL(dxccs26,0), IFNULL(dxccs27,0), IFNULL(dxccs28,0), IFNULL(dxccs29,0)
FROM 2013_sasd_core
WHERE visitlink IS NOT NULL
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2013_sasd_dxccs.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dxccs1,0), IFNULL(dxccs2,0), IFNULL(dxccs3,0), IFNULL(dxccs4,0), IFNULL(dxccs5,0), IFNULL(dxccs6,0), IFNULL(dxccs7,0), IFNULL(dxccs8,0), IFNULL(dxccs9,0), IFNULL(dxccs10,0), IFNULL(dxccs11,0), IFNULL(dxccs12,0), IFNULL(dxccs13,0), IFNULL(dxccs14,0), IFNULL(dxccs15,0), IFNULL(dxccs16,0), IFNULL(dxccs17,0), IFNULL(dxccs18,0), IFNULL(dxccs19,0), IFNULL(dxccs20,0), IFNULL(dxccs21,0), IFNULL(dxccs22,0), IFNULL(dxccs23,0), IFNULL(dxccs24,0), IFNULL(dxccs25,0), IFNULL(dxccs26,0), IFNULL(dxccs27,0), IFNULL(dxccs28,0), IFNULL(dxccs29,0)
FROM 2014_sasd_core
WHERE visitlink IS NOT NULL
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2014_sasd_dxccs.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dxccs1,0), IFNULL(dxccs2,0), IFNULL(dxccs3,0), IFNULL(dxccs4,0), IFNULL(dxccs5,0), IFNULL(dxccs6,0), IFNULL(dxccs7,0), IFNULL(dxccs8,0), IFNULL(dxccs9,0), IFNULL(dxccs10,0), IFNULL(dxccs11,0), IFNULL(dxccs12,0), IFNULL(dxccs13,0), IFNULL(dxccs14,0), IFNULL(dxccs15,0), IFNULL(dxccs16,0), IFNULL(dxccs17,0), IFNULL(dxccs18,0), IFNULL(dxccs19,0), IFNULL(dxccs20,0), IFNULL(dxccs21,0), IFNULL(dxccs22,0), IFNULL(dxccs23,0), IFNULL(dxccs24,0), IFNULL(dxccs25,0), IFNULL(dxccs26,0), IFNULL(dxccs27,0), IFNULL(dxccs28,0), IFNULL(dxccs29,0)
FROM 2013_sedd_core
WHERE visitlink IS NOT NULL
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2013_sedd_dxccs.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dxccs1,0), IFNULL(dxccs2,0), IFNULL(dxccs3,0), IFNULL(dxccs4,0), IFNULL(dxccs5,0), IFNULL(dxccs6,0), IFNULL(dxccs7,0), IFNULL(dxccs8,0), IFNULL(dxccs9,0), IFNULL(dxccs10,0), IFNULL(dxccs11,0), IFNULL(dxccs12,0), IFNULL(dxccs13,0), IFNULL(dxccs14,0), IFNULL(dxccs15,0), IFNULL(dxccs16,0), IFNULL(dxccs17,0), IFNULL(dxccs18,0), IFNULL(dxccs19,0), IFNULL(dxccs20,0), IFNULL(dxccs21,0), IFNULL(dxccs22,0), IFNULL(dxccs23,0), IFNULL(dxccs24,0), IFNULL(dxccs25,0), IFNULL(dxccs26,0), IFNULL(dxccs27,0), IFNULL(dxccs28,0), IFNULL(dxccs29,0)
WHERE visitlink IS NOT NULL
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2014_sedd_dxccs.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dxccs1,0), IFNULL(dxccs2,0), IFNULL(dxccs3,0), IFNULL(dxccs4,0), IFNULL(dxccs5,0), IFNULL(dxccs6,0), IFNULL(dxccs7,0), IFNULL(dxccs8,0), IFNULL(dxccs9,0), IFNULL(dxccs10,0), IFNULL(dxccs11,0), IFNULL(dxccs12,0), IFNULL(dxccs13,0), IFNULL(dxccs14,0), IFNULL(dxccs15,0), IFNULL(dxccs16,0), IFNULL(dxccs17,0), IFNULL(dxccs18,0), IFNULL(dxccs19,0), IFNULL(dxccs20,0), IFNULL(dxccs21,0), IFNULL(dxccs22,0), IFNULL(dxccs23,0), IFNULL(dxccs24,0), IFNULL(dxccs25,0), IFNULL(dxccs26,0), IFNULL(dxccs27,0), IFNULL(dxccs28,0), IFNULL(dxccs29,0)
FROM 2013_sid_core
WHERE visitlink IS NOT NULL
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2013_sid_dxccs.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dxccs1,0), IFNULL(dxccs2,0), IFNULL(dxccs3,0), IFNULL(dxccs4,0), IFNULL(dxccs5,0), IFNULL(dxccs6,0), IFNULL(dxccs7,0), IFNULL(dxccs8,0), IFNULL(dxccs9,0), IFNULL(dxccs10,0), IFNULL(dxccs11,0), IFNULL(dxccs12,0), IFNULL(dxccs13,0), IFNULL(dxccs14,0), IFNULL(dxccs15,0), IFNULL(dxccs16,0), IFNULL(dxccs17,0), IFNULL(dxccs18,0), IFNULL(dxccs19,0), IFNULL(dxccs20,0), IFNULL(dxccs21,0), IFNULL(dxccs22,0), IFNULL(dxccs23,0), IFNULL(dxccs24,0), IFNULL(dxccs25,0), IFNULL(dxccs26,0), IFNULL(dxccs27,0), IFNULL(dxccs28,0), IFNULL(dxccs29,0)
FROM 2014_sid_core
WHERE visitlink IS NOT NULL
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2014_sid_dxccs.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';


#------------------FLAGGED POPULATION---------------------------------------------------------
SELECT visitlink, IFNULL(dxccs1,0), IFNULL(dxccs2,0), IFNULL(dxccs3,0), IFNULL(dxccs4,0), IFNULL(dxccs5,0), IFNULL(dxccs6,0), IFNULL(dxccs7,0), IFNULL(dxccs8,0), IFNULL(dxccs9,0), IFNULL(dxccs10,0), IFNULL(dxccs11,0), IFNULL(dxccs12,0), IFNULL(dxccs13,0), IFNULL(dxccs14,0), IFNULL(dxccs15,0), IFNULL(dxccs16,0), IFNULL(dxccs17,0), IFNULL(dxccs18,0), IFNULL(dxccs19,0), IFNULL(dxccs20,0), IFNULL(dxccs21,0), IFNULL(dxccs22,0), IFNULL(dxccs23,0), IFNULL(dxccs24,0), IFNULL(dxccs25,0), IFNULL(dxccs26,0), IFNULL(dxccs27,0), IFNULL(dxccs28,0), IFNULL(dxccs29,0)
FROM 2013_sasd_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2013_sasd_dxccs_flagged.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dxccs1,0), IFNULL(dxccs2,0), IFNULL(dxccs3,0), IFNULL(dxccs4,0), IFNULL(dxccs5,0), IFNULL(dxccs6,0), IFNULL(dxccs7,0), IFNULL(dxccs8,0), IFNULL(dxccs9,0), IFNULL(dxccs10,0), IFNULL(dxccs11,0), IFNULL(dxccs12,0), IFNULL(dxccs13,0), IFNULL(dxccs14,0), IFNULL(dxccs15,0), IFNULL(dxccs16,0), IFNULL(dxccs17,0), IFNULL(dxccs18,0), IFNULL(dxccs19,0), IFNULL(dxccs20,0), IFNULL(dxccs21,0), IFNULL(dxccs22,0), IFNULL(dxccs23,0), IFNULL(dxccs24,0), IFNULL(dxccs25,0), IFNULL(dxccs26,0), IFNULL(dxccs27,0), IFNULL(dxccs28,0), IFNULL(dxccs29,0)
FROM 2014_sasd_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2014_sasd_dxccs_flagged.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dxccs1,0), IFNULL(dxccs2,0), IFNULL(dxccs3,0), IFNULL(dxccs4,0), IFNULL(dxccs5,0), IFNULL(dxccs6,0), IFNULL(dxccs7,0), IFNULL(dxccs8,0), IFNULL(dxccs9,0), IFNULL(dxccs10,0), IFNULL(dxccs11,0), IFNULL(dxccs12,0), IFNULL(dxccs13,0), IFNULL(dxccs14,0), IFNULL(dxccs15,0), IFNULL(dxccs16,0), IFNULL(dxccs17,0), IFNULL(dxccs18,0), IFNULL(dxccs19,0), IFNULL(dxccs20,0), IFNULL(dxccs21,0), IFNULL(dxccs22,0), IFNULL(dxccs23,0), IFNULL(dxccs24,0), IFNULL(dxccs25,0), IFNULL(dxccs26,0), IFNULL(dxccs27,0), IFNULL(dxccs28,0), IFNULL(dxccs29,0)
FROM 2013_sedd_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2013_sedd_dxccs_flagged.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dxccs1,0), IFNULL(dxccs2,0), IFNULL(dxccs3,0), IFNULL(dxccs4,0), IFNULL(dxccs5,0), IFNULL(dxccs6,0), IFNULL(dxccs7,0), IFNULL(dxccs8,0), IFNULL(dxccs9,0), IFNULL(dxccs10,0), IFNULL(dxccs11,0), IFNULL(dxccs12,0), IFNULL(dxccs13,0), IFNULL(dxccs14,0), IFNULL(dxccs15,0), IFNULL(dxccs16,0), IFNULL(dxccs17,0), IFNULL(dxccs18,0), IFNULL(dxccs19,0), IFNULL(dxccs20,0), IFNULL(dxccs21,0), IFNULL(dxccs22,0), IFNULL(dxccs23,0), IFNULL(dxccs24,0), IFNULL(dxccs25,0), IFNULL(dxccs26,0), IFNULL(dxccs27,0), IFNULL(dxccs28,0), IFNULL(dxccs29,0)
FROM 2014_sedd_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2014_sedd_dxccs_flagged.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dxccs1,0), IFNULL(dxccs2,0), IFNULL(dxccs3,0), IFNULL(dxccs4,0), IFNULL(dxccs5,0), IFNULL(dxccs6,0), IFNULL(dxccs7,0), IFNULL(dxccs8,0), IFNULL(dxccs9,0), IFNULL(dxccs10,0), IFNULL(dxccs11,0), IFNULL(dxccs12,0), IFNULL(dxccs13,0), IFNULL(dxccs14,0), IFNULL(dxccs15,0), IFNULL(dxccs16,0), IFNULL(dxccs17,0), IFNULL(dxccs18,0), IFNULL(dxccs19,0), IFNULL(dxccs20,0), IFNULL(dxccs21,0), IFNULL(dxccs22,0), IFNULL(dxccs23,0), IFNULL(dxccs24,0), IFNULL(dxccs25,0), IFNULL(dxccs26,0), IFNULL(dxccs27,0), IFNULL(dxccs28,0), IFNULL(dxccs29,0)
FROM 2013_sid_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2013_sid_dxccs_flagged.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dxccs1,0), IFNULL(dxccs2,0), IFNULL(dxccs3,0), IFNULL(dxccs4,0), IFNULL(dxccs5,0), IFNULL(dxccs6,0), IFNULL(dxccs7,0), IFNULL(dxccs8,0), IFNULL(dxccs9,0), IFNULL(dxccs10,0), IFNULL(dxccs11,0), IFNULL(dxccs12,0), IFNULL(dxccs13,0), IFNULL(dxccs14,0), IFNULL(dxccs15,0), IFNULL(dxccs16,0), IFNULL(dxccs17,0), IFNULL(dxccs18,0), IFNULL(dxccs19,0), IFNULL(dxccs20,0), IFNULL(dxccs21,0), IFNULL(dxccs22,0), IFNULL(dxccs23,0), IFNULL(dxccs24,0), IFNULL(dxccs25,0), IFNULL(dxccs26,0), IFNULL(dxccs27,0), IFNULL(dxccs28,0), IFNULL(dxccs29,0)
FROM 2014_sid_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2014_sid_dxccs_flagged.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

#--------------------REMAINDER POPULATION---------------------------------------
SELECT visitlink, IFNULL(dxccs1,0), IFNULL(dxccs2,0), IFNULL(dxccs3,0), IFNULL(dxccs4,0), IFNULL(dxccs5,0), IFNULL(dxccs6,0), IFNULL(dxccs7,0), IFNULL(dxccs8,0), IFNULL(dxccs9,0), IFNULL(dxccs10,0), IFNULL(dxccs11,0), IFNULL(dxccs12,0), IFNULL(dxccs13,0), IFNULL(dxccs14,0), IFNULL(dxccs15,0), IFNULL(dxccs16,0), IFNULL(dxccs17,0), IFNULL(dxccs18,0), IFNULL(dxccs19,0), IFNULL(dxccs20,0), IFNULL(dxccs21,0), IFNULL(dxccs22,0), IFNULL(dxccs23,0), IFNULL(dxccs24,0), IFNULL(dxccs25,0), IFNULL(dxccs26,0), IFNULL(dxccs27,0), IFNULL(dxccs28,0), IFNULL(dxccs29,0)
FROM 2013_sasd_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2013_sasd_dxccs_remainder.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dxccs1,0), IFNULL(dxccs2,0), IFNULL(dxccs3,0), IFNULL(dxccs4,0), IFNULL(dxccs5,0), IFNULL(dxccs6,0), IFNULL(dxccs7,0), IFNULL(dxccs8,0), IFNULL(dxccs9,0), IFNULL(dxccs10,0), IFNULL(dxccs11,0), IFNULL(dxccs12,0), IFNULL(dxccs13,0), IFNULL(dxccs14,0), IFNULL(dxccs15,0), IFNULL(dxccs16,0), IFNULL(dxccs17,0), IFNULL(dxccs18,0), IFNULL(dxccs19,0), IFNULL(dxccs20,0), IFNULL(dxccs21,0), IFNULL(dxccs22,0), IFNULL(dxccs23,0), IFNULL(dxccs24,0), IFNULL(dxccs25,0), IFNULL(dxccs26,0), IFNULL(dxccs27,0), IFNULL(dxccs28,0), IFNULL(dxccs29,0)
FROM 2014_sasd_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2014_sasd_dxccs_remainder.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dxccs1,0), IFNULL(dxccs2,0), IFNULL(dxccs3,0), IFNULL(dxccs4,0), IFNULL(dxccs5,0), IFNULL(dxccs6,0), IFNULL(dxccs7,0), IFNULL(dxccs8,0), IFNULL(dxccs9,0), IFNULL(dxccs10,0), IFNULL(dxccs11,0), IFNULL(dxccs12,0), IFNULL(dxccs13,0), IFNULL(dxccs14,0), IFNULL(dxccs15,0), IFNULL(dxccs16,0), IFNULL(dxccs17,0), IFNULL(dxccs18,0), IFNULL(dxccs19,0), IFNULL(dxccs20,0), IFNULL(dxccs21,0), IFNULL(dxccs22,0), IFNULL(dxccs23,0), IFNULL(dxccs24,0), IFNULL(dxccs25,0), IFNULL(dxccs26,0), IFNULL(dxccs27,0), IFNULL(dxccs28,0), IFNULL(dxccs29,0)
FROM 2013_sedd_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2013_sedd_dxccs_remainder.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dxccs1,0), IFNULL(dxccs2,0), IFNULL(dxccs3,0), IFNULL(dxccs4,0), IFNULL(dxccs5,0), IFNULL(dxccs6,0), IFNULL(dxccs7,0), IFNULL(dxccs8,0), IFNULL(dxccs9,0), IFNULL(dxccs10,0), IFNULL(dxccs11,0), IFNULL(dxccs12,0), IFNULL(dxccs13,0), IFNULL(dxccs14,0), IFNULL(dxccs15,0), IFNULL(dxccs16,0), IFNULL(dxccs17,0), IFNULL(dxccs18,0), IFNULL(dxccs19,0), IFNULL(dxccs20,0), IFNULL(dxccs21,0), IFNULL(dxccs22,0), IFNULL(dxccs23,0), IFNULL(dxccs24,0), IFNULL(dxccs25,0), IFNULL(dxccs26,0), IFNULL(dxccs27,0), IFNULL(dxccs28,0), IFNULL(dxccs29,0)
FROM 2014_sedd_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2014_sedd_dxccs_remainder.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dxccs1,0), IFNULL(dxccs2,0), IFNULL(dxccs3,0), IFNULL(dxccs4,0), IFNULL(dxccs5,0), IFNULL(dxccs6,0), IFNULL(dxccs7,0), IFNULL(dxccs8,0), IFNULL(dxccs9,0), IFNULL(dxccs10,0), IFNULL(dxccs11,0), IFNULL(dxccs12,0), IFNULL(dxccs13,0), IFNULL(dxccs14,0), IFNULL(dxccs15,0), IFNULL(dxccs16,0), IFNULL(dxccs17,0), IFNULL(dxccs18,0), IFNULL(dxccs19,0), IFNULL(dxccs20,0), IFNULL(dxccs21,0), IFNULL(dxccs22,0), IFNULL(dxccs23,0), IFNULL(dxccs24,0), IFNULL(dxccs25,0), IFNULL(dxccs26,0), IFNULL(dxccs27,0), IFNULL(dxccs28,0), IFNULL(dxccs29,0)
FROM 2013_sid_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2013_sid_dxccs_remainder.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dxccs1,0), IFNULL(dxccs2,0), IFNULL(dxccs3,0), IFNULL(dxccs4,0), IFNULL(dxccs5,0), IFNULL(dxccs6,0), IFNULL(dxccs7,0), IFNULL(dxccs8,0), IFNULL(dxccs9,0), IFNULL(dxccs10,0), IFNULL(dxccs11,0), IFNULL(dxccs12,0), IFNULL(dxccs13,0), IFNULL(dxccs14,0), IFNULL(dxccs15,0), IFNULL(dxccs16,0), IFNULL(dxccs17,0), IFNULL(dxccs18,0), IFNULL(dxccs19,0), IFNULL(dxccs20,0), IFNULL(dxccs21,0), IFNULL(dxccs22,0), IFNULL(dxccs23,0), IFNULL(dxccs24,0), IFNULL(dxccs25,0), IFNULL(dxccs26,0), IFNULL(dxccs27,0), IFNULL(dxccs28,0), IFNULL(dxccs29,0)
FROM 2014_sid_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2014_sid_dxccs_remainder.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

#-------------------ICD codes to csv-------------------------------------------

#------------WHOLE Population---------------

SELECT visitlink, IFNULL(dx1,0), IFNULL(dx2,0), IFNULL(dx3,0), IFNULL(dx4,0), IFNULL(dx5,0), IFNULL(dx6,0), IFNULL(dx7,0), IFNULL(dx8,0), IFNULL(dx9,0), IFNULL(dx10,0), IFNULL(dx11,0), IFNULL(dx12,0), IFNULL(dx13,0), IFNULL(dx14,0), IFNULL(dx15,0), IFNULL(dx16,0), IFNULL(dx17,0), IFNULL(dx18,0), IFNULL(dx19,0), IFNULL(dx20,0), IFNULL(dx21,0), IFNULL(dx22,0), IFNULL(dx23,0), IFNULL(dx24,0), IFNULL(dx25,0), IFNULL(dx26,0), IFNULL(dx27,0), IFNULL(dx28,0), IFNULL(dx29,0)
FROM 2013_sasd_core
WHERE visitlink IS NOT NULL
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2013_sasd_dx.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dx1,0), IFNULL(dx2,0), IFNULL(dx3,0), IFNULL(dx4,0), IFNULL(dx5,0), IFNULL(dx6,0), IFNULL(dx7,0), IFNULL(dx8,0), IFNULL(dx9,0), IFNULL(dx10,0), IFNULL(dx11,0), IFNULL(dx12,0), IFNULL(dx13,0), IFNULL(dx14,0), IFNULL(dx15,0), IFNULL(dx16,0), IFNULL(dx17,0), IFNULL(dx18,0), IFNULL(dx19,0), IFNULL(dx20,0), IFNULL(dx21,0), IFNULL(dx22,0), IFNULL(dx23,0), IFNULL(dx24,0), IFNULL(dx25,0), IFNULL(dx26,0), IFNULL(dx27,0), IFNULL(dx28,0), IFNULL(dx29,0)
FROM 2013_sedd_core
WHERE visitlink IS NOT NULL
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2013_sedd_dx.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dx1,0), IFNULL(dx2,0), IFNULL(dx3,0), IFNULL(dx4,0), IFNULL(dx5,0), IFNULL(dx6,0), IFNULL(dx7,0), IFNULL(dx8,0), IFNULL(dx9,0), IFNULL(dx10,0), IFNULL(dx11,0), IFNULL(dx12,0), IFNULL(dx13,0), IFNULL(dx14,0), IFNULL(dx15,0), IFNULL(dx16,0), IFNULL(dx17,0), IFNULL(dx18,0), IFNULL(dx19,0), IFNULL(dx20,0), IFNULL(dx21,0), IFNULL(dx22,0), IFNULL(dx23,0), IFNULL(dx24,0), IFNULL(dx25,0), IFNULL(dx26,0), IFNULL(dx27,0), IFNULL(dx28,0), IFNULL(dx29,0)
FROM 2013_sid_core
WHERE visitlink IS NOT NULL
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2013_sid_dx.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dx1,0), IFNULL(dx2,0), IFNULL(dx3,0), IFNULL(dx4,0), IFNULL(dx5,0), IFNULL(dx6,0), IFNULL(dx7,0), IFNULL(dx8,0), IFNULL(dx9,0), IFNULL(dx10,0), IFNULL(dx11,0), IFNULL(dx12,0), IFNULL(dx13,0), IFNULL(dx14,0), IFNULL(dx15,0), IFNULL(dx16,0), IFNULL(dx17,0), IFNULL(dx18,0), IFNULL(dx19,0), IFNULL(dx20,0), IFNULL(dx21,0), IFNULL(dx22,0), IFNULL(dx23,0), IFNULL(dx24,0), IFNULL(dx25,0), IFNULL(dx26,0), IFNULL(dx27,0), IFNULL(dx28,0), IFNULL(dx29,0)
FROM 2014_sasd_core
WHERE visitlink IS NOT NULL
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2014_sasd_dx.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dx1,0), IFNULL(dx2,0), IFNULL(dx3,0), IFNULL(dx4,0), IFNULL(dx5,0), IFNULL(dx6,0), IFNULL(dx7,0), IFNULL(dx8,0), IFNULL(dx9,0), IFNULL(dx10,0), IFNULL(dx11,0), IFNULL(dx12,0), IFNULL(dx13,0), IFNULL(dx14,0), IFNULL(dx15,0), IFNULL(dx16,0), IFNULL(dx17,0), IFNULL(dx18,0), IFNULL(dx19,0), IFNULL(dx20,0), IFNULL(dx21,0), IFNULL(dx22,0), IFNULL(dx23,0), IFNULL(dx24,0), IFNULL(dx25,0), IFNULL(dx26,0), IFNULL(dx27,0), IFNULL(dx28,0), IFNULL(dx29,0)
FROM 2014_sedd_core
WHERE visitlink IS NOT NULL
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2014_sedd_dx.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dx1,0), IFNULL(dx2,0), IFNULL(dx3,0), IFNULL(dx4,0), IFNULL(dx5,0), IFNULL(dx6,0), IFNULL(dx7,0), IFNULL(dx8,0), IFNULL(dx9,0), IFNULL(dx10,0), IFNULL(dx11,0), IFNULL(dx12,0), IFNULL(dx13,0), IFNULL(dx14,0), IFNULL(dx15,0), IFNULL(dx16,0), IFNULL(dx17,0), IFNULL(dx18,0), IFNULL(dx19,0), IFNULL(dx20,0), IFNULL(dx21,0), IFNULL(dx22,0), IFNULL(dx23,0), IFNULL(dx24,0), IFNULL(dx25,0), IFNULL(dx26,0), IFNULL(dx27,0), IFNULL(dx28,0), IFNULL(dx29,0)
FROM 2014_sid_core
WHERE visitlink IS NOT NULL
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2014_sid_dx.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

#----------------Flagged Population------------------
SELECT visitlink, IFNULL(dx1,0), IFNULL(dx2,0), IFNULL(dx3,0), IFNULL(dx4,0), IFNULL(dx5,0), IFNULL(dx6,0), IFNULL(dx7,0), IFNULL(dx8,0), IFNULL(dx9,0), IFNULL(dx10,0), IFNULL(dx11,0), IFNULL(dx12,0), IFNULL(dx13,0), IFNULL(dx14,0), IFNULL(dx15,0), IFNULL(dx16,0), IFNULL(dx17,0), IFNULL(dx18,0), IFNULL(dx19,0), IFNULL(dx20,0), IFNULL(dx21,0), IFNULL(dx22,0), IFNULL(dx23,0), IFNULL(dx24,0), IFNULL(dx25,0), IFNULL(dx26,0), IFNULL(dx27,0), IFNULL(dx28,0), IFNULL(dx29,0)
FROM 2013_sasd_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2013_sasd_dx_flagged.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dx1,0), IFNULL(dx2,0), IFNULL(dx3,0), IFNULL(dx4,0), IFNULL(dx5,0), IFNULL(dx6,0), IFNULL(dx7,0), IFNULL(dx8,0), IFNULL(dx9,0), IFNULL(dx10,0), IFNULL(dx11,0), IFNULL(dx12,0), IFNULL(dx13,0), IFNULL(dx14,0), IFNULL(dx15,0), IFNULL(dx16,0), IFNULL(dx17,0), IFNULL(dx18,0), IFNULL(dx19,0), IFNULL(dx20,0), IFNULL(dx21,0), IFNULL(dx22,0), IFNULL(dx23,0), IFNULL(dx24,0), IFNULL(dx25,0), IFNULL(dx26,0), IFNULL(dx27,0), IFNULL(dx28,0), IFNULL(dx29,0)
FROM 2013_sedd_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2013_sedd_dx_flagged.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dx1,0), IFNULL(dx2,0), IFNULL(dx3,0), IFNULL(dx4,0), IFNULL(dx5,0), IFNULL(dx6,0), IFNULL(dx7,0), IFNULL(dx8,0), IFNULL(dx9,0), IFNULL(dx10,0), IFNULL(dx11,0), IFNULL(dx12,0), IFNULL(dx13,0), IFNULL(dx14,0), IFNULL(dx15,0), IFNULL(dx16,0), IFNULL(dx17,0), IFNULL(dx18,0), IFNULL(dx19,0), IFNULL(dx20,0), IFNULL(dx21,0), IFNULL(dx22,0), IFNULL(dx23,0), IFNULL(dx24,0), IFNULL(dx25,0), IFNULL(dx26,0), IFNULL(dx27,0), IFNULL(dx28,0), IFNULL(dx29,0)
FROM 2013_sid_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2013_sid_dx_flagged.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dx1,0), IFNULL(dx2,0), IFNULL(dx3,0), IFNULL(dx4,0), IFNULL(dx5,0), IFNULL(dx6,0), IFNULL(dx7,0), IFNULL(dx8,0), IFNULL(dx9,0), IFNULL(dx10,0), IFNULL(dx11,0), IFNULL(dx12,0), IFNULL(dx13,0), IFNULL(dx14,0), IFNULL(dx15,0), IFNULL(dx16,0), IFNULL(dx17,0), IFNULL(dx18,0), IFNULL(dx19,0), IFNULL(dx20,0), IFNULL(dx21,0), IFNULL(dx22,0), IFNULL(dx23,0), IFNULL(dx24,0), IFNULL(dx25,0), IFNULL(dx26,0), IFNULL(dx27,0), IFNULL(dx28,0), IFNULL(dx29,0)
FROM 2014_sasd_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2014_sasd_dx_flagged.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dx1,0), IFNULL(dx2,0), IFNULL(dx3,0), IFNULL(dx4,0), IFNULL(dx5,0), IFNULL(dx6,0), IFNULL(dx7,0), IFNULL(dx8,0), IFNULL(dx9,0), IFNULL(dx10,0), IFNULL(dx11,0), IFNULL(dx12,0), IFNULL(dx13,0), IFNULL(dx14,0), IFNULL(dx15,0), IFNULL(dx16,0), IFNULL(dx17,0), IFNULL(dx18,0), IFNULL(dx19,0), IFNULL(dx20,0), IFNULL(dx21,0), IFNULL(dx22,0), IFNULL(dx23,0), IFNULL(dx24,0), IFNULL(dx25,0), IFNULL(dx26,0), IFNULL(dx27,0), IFNULL(dx28,0), IFNULL(dx29,0)
FROM 2014_sedd_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2014_sedd_dx_flagged.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dx1,0), IFNULL(dx2,0), IFNULL(dx3,0), IFNULL(dx4,0), IFNULL(dx5,0), IFNULL(dx6,0), IFNULL(dx7,0), IFNULL(dx8,0), IFNULL(dx9,0), IFNULL(dx10,0), IFNULL(dx11,0), IFNULL(dx12,0), IFNULL(dx13,0), IFNULL(dx14,0), IFNULL(dx15,0), IFNULL(dx16,0), IFNULL(dx17,0), IFNULL(dx18,0), IFNULL(dx19,0), IFNULL(dx20,0), IFNULL(dx21,0), IFNULL(dx22,0), IFNULL(dx23,0), IFNULL(dx24,0), IFNULL(dx25,0), IFNULL(dx26,0), IFNULL(dx27,0), IFNULL(dx28,0), IFNULL(dx29,0)
FROM 2014_sid_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2014_sid_dx_flagged.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';


#----REMAINDER POPULATION-------------------------
SELECT visitlink, IFNULL(dx1,0), IFNULL(dx2,0), IFNULL(dx3,0), IFNULL(dx4,0), IFNULL(dx5,0), IFNULL(dx6,0), IFNULL(dx7,0), IFNULL(dx8,0), IFNULL(dx9,0), IFNULL(dx10,0), IFNULL(dx11,0), IFNULL(dx12,0), IFNULL(dx13,0), IFNULL(dx14,0), IFNULL(dx15,0), IFNULL(dx16,0), IFNULL(dx17,0), IFNULL(dx18,0), IFNULL(dx19,0), IFNULL(dx20,0), IFNULL(dx21,0), IFNULL(dx22,0), IFNULL(dx23,0), IFNULL(dx24,0), IFNULL(dx25,0), IFNULL(dx26,0), IFNULL(dx27,0), IFNULL(dx28,0), IFNULL(dx29,0)
FROM 2013_sasd_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2013_sasd_dx_remainder.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dx1,0), IFNULL(dx2,0), IFNULL(dx3,0), IFNULL(dx4,0), IFNULL(dx5,0), IFNULL(dx6,0), IFNULL(dx7,0), IFNULL(dx8,0), IFNULL(dx9,0), IFNULL(dx10,0), IFNULL(dx11,0), IFNULL(dx12,0), IFNULL(dx13,0), IFNULL(dx14,0), IFNULL(dx15,0), IFNULL(dx16,0), IFNULL(dx17,0), IFNULL(dx18,0), IFNULL(dx19,0), IFNULL(dx20,0), IFNULL(dx21,0), IFNULL(dx22,0), IFNULL(dx23,0), IFNULL(dx24,0), IFNULL(dx25,0), IFNULL(dx26,0), IFNULL(dx27,0), IFNULL(dx28,0), IFNULL(dx29,0)
FROM 2013_sedd_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2013_sedd_dx_remainder.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dx1,0), IFNULL(dx2,0), IFNULL(dx3,0), IFNULL(dx4,0), IFNULL(dx5,0), IFNULL(dx6,0), IFNULL(dx7,0), IFNULL(dx8,0), IFNULL(dx9,0), IFNULL(dx10,0), IFNULL(dx11,0), IFNULL(dx12,0), IFNULL(dx13,0), IFNULL(dx14,0), IFNULL(dx15,0), IFNULL(dx16,0), IFNULL(dx17,0), IFNULL(dx18,0), IFNULL(dx19,0), IFNULL(dx20,0), IFNULL(dx21,0), IFNULL(dx22,0), IFNULL(dx23,0), IFNULL(dx24,0), IFNULL(dx25,0), IFNULL(dx26,0), IFNULL(dx27,0), IFNULL(dx28,0), IFNULL(dx29,0)
FROM 2013_sid_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2013_sid_dx_remainder.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dx1,0), IFNULL(dx2,0), IFNULL(dx3,0), IFNULL(dx4,0), IFNULL(dx5,0), IFNULL(dx6,0), IFNULL(dx7,0), IFNULL(dx8,0), IFNULL(dx9,0), IFNULL(dx10,0), IFNULL(dx11,0), IFNULL(dx12,0), IFNULL(dx13,0), IFNULL(dx14,0), IFNULL(dx15,0), IFNULL(dx16,0), IFNULL(dx17,0), IFNULL(dx18,0), IFNULL(dx19,0), IFNULL(dx20,0), IFNULL(dx21,0), IFNULL(dx22,0), IFNULL(dx23,0), IFNULL(dx24,0), IFNULL(dx25,0), IFNULL(dx26,0), IFNULL(dx27,0), IFNULL(dx28,0), IFNULL(dx29,0)
FROM 2014_sasd_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2014_sasd_dx_remainder.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dx1,0), IFNULL(dx2,0), IFNULL(dx3,0), IFNULL(dx4,0), IFNULL(dx5,0), IFNULL(dx6,0), IFNULL(dx7,0), IFNULL(dx8,0), IFNULL(dx9,0), IFNULL(dx10,0), IFNULL(dx11,0), IFNULL(dx12,0), IFNULL(dx13,0), IFNULL(dx14,0), IFNULL(dx15,0), IFNULL(dx16,0), IFNULL(dx17,0), IFNULL(dx18,0), IFNULL(dx19,0), IFNULL(dx20,0), IFNULL(dx21,0), IFNULL(dx22,0), IFNULL(dx23,0), IFNULL(dx24,0), IFNULL(dx25,0), IFNULL(dx26,0), IFNULL(dx27,0), IFNULL(dx28,0), IFNULL(dx29,0)
FROM 2014_sedd_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2014_sedd_dx_remainder.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT visitlink, IFNULL(dx1,0), IFNULL(dx2,0), IFNULL(dx3,0), IFNULL(dx4,0), IFNULL(dx5,0), IFNULL(dx6,0), IFNULL(dx7,0), IFNULL(dx8,0), IFNULL(dx9,0), IFNULL(dx10,0), IFNULL(dx11,0), IFNULL(dx12,0), IFNULL(dx13,0), IFNULL(dx14,0), IFNULL(dx15,0), IFNULL(dx16,0), IFNULL(dx17,0), IFNULL(dx18,0), IFNULL(dx19,0), IFNULL(dx20,0), IFNULL(dx21,0), IFNULL(dx22,0), IFNULL(dx23,0), IFNULL(dx24,0), IFNULL(dx25,0), IFNULL(dx26,0), IFNULL(dx27,0), IFNULL(dx28,0), IFNULL(dx29,0)
FROM 2014_sid_core
WHERE visitlink IN 
	(SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL)
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/2014_sid_dx_remainder.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

#----------------------Creating LOS Tables-----------------------------
CREATE TABLE `hcup_los` 
(
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `visitlink` varchar(16) DEFAULT NULL,
  `dq_flag` int(1) NOT NULL DEFAULT 0,
  #completeness measures
  `2013_sid_los` int(16) NOT NULL DEFAULT 0,
  `2014_sid_los` int(16) NOT NULL DEFAULT 0,

  `2013_sasd_los` int(16) NOT NULL DEFAULT 0,
  `2014_sasd_los` int(16) NOT NULL DEFAULT 0,
  
  `2013_sedd_los` int(16) NOT NULL DEFAULT 0,
  `2014_sedd_los` int(16) NOT NULL DEFAULT 0,

  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


SET SQL_SAFE_UPDATES = 0;

INSERT INTO hcup_los (visitlink, 2013_sid_los)
SELECT visitlink, los
FROM 2013_sid_core
WHERE visitlink IS NOT NULL AND los IS NOT NULL;

INSERT INTO hcup_los (visitlink, 2014_sid_los)
SELECT visitlink, los
FROM 2014_sid_core
WHERE visitlink IS NOT NULL AND los IS NOT NULL;

INSERT INTO hcup_los (visitlink, 2013_sasd_los)
SELECT visitlink, los
FROM 2013_sasd_core
WHERE visitlink IS NOT NULL AND los IS NOT NULL;

INSERT INTO hcup_los (visitlink, 2014_sasd_los)
SELECT visitlink, los
FROM 2014_sasd_core
WHERE visitlink IS NOT NULL AND los IS NOT NULL;

INSERT INTO hcup_los (visitlink, 2013_sedd_los)
SELECT visitlink, los
FROM 2013_sedd_core
WHERE visitlink IS NOT NULL AND los IS NOT NULL;

INSERT INTO hcup_los (visitlink, 2014_sedd_los)
SELECT visitlink, los
FROM 2014_sedd_core
WHERE visitlink IS NOT NULL AND los IS NOT NULL;

UPDATE hcup_los
INNER JOIN (
  SELECT visitlink
  FROM hcup_pat_pop_spec_flagged
  GROUP BY visitlink) t
ON hcup_los.visitlink = t.visitlink
OR hcup_los.visitlink IS NULL AND t.visitlink IS NULL
SET dq_flag = 1;

SELECT visitlink, dq_flag, 2013_sid_los, 2014_sid_los, 2013_sasd_los, 2014_sasd_los, 2013_sedd_los, 2014_sedd_los
FROM hcup_los
WHERE visitlink IS NOT NULL 
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/hcup_los.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';
#constructs the entire query for 2013
USE hcup;
SELECT  t.visitlink, t.N, t.D, t.I, t.M, t.W, t.X,
    t.N + t.D + t.I + t.M + t.W + t.X AS total 
FROM (
SELECT  visitlink, 
        count(CASE WHEN marital_2013_N_ct > 0 THEN 1 END) AS N,
        count(CASE WHEN marital_2013_D_ct > 0 THEN 1 END) AS D,
        count(CASE WHEN marital_2013_I_ct > 0 THEN 1 END) AS I,
        count(CASE WHEN marital_2013_M_ct > 0 THEN 1 END) AS M,
        count(CASE WHEN marital_2013_W_ct > 0 THEN 1 END) AS W,
        count(CASE WHEN marital_2013_X_ct > 0 THEN 1 END) AS X
FROM hcup_pat_marital
GROUP BY  visitlink) t;

#returns the breakdown by number
USE hcup;
SELECT  COUNT(*), t.N + t.D + t.I + t.M + t.W + t.X AS total 
FROM (
SELECT  visitlink, 
        count(CASE WHEN marital_2013_N_ct > 0 THEN 1 END) AS N,
        count(CASE WHEN marital_2013_D_ct > 0 THEN 1 END) AS D,
        count(CASE WHEN marital_2013_I_ct > 0 THEN 1 END) AS I,
        count(CASE WHEN marital_2013_M_ct > 0 THEN 1 END) AS M,
        count(CASE WHEN marital_2013_W_ct > 0 THEN 1 END) AS W,
        count(CASE WHEN marital_2013_X_ct > 0 THEN 1 END) AS X
FROM hcup_pat_marital
GROUP BY  visitlink) t
GROUP BY total;
/*
  marital_ct represents the number of different marriage status that were reported per person
  for example, 109,770 had two different marriage statuses reported in the same year. A person could
  have gone from single to married within the year 2013. 7, 845 could have gone from single, 
  married, divorced. 
*/

/* 2013
  + ------------- + -------------- +
  | pat_ct        | marital_ct     |
  + ------------- + -------------- +
  | 1,078,899     | 0              | number of people who are null
  | 2,066,700     | 1              |
  | 109,770       | 2              |
  | 7,845         | 3              |
  | 607           | 4              |
  | 42            | 5              |
  | 2             | 6              | the one visitlink is null, the other is 1852608
  + ------------- + -------------- +
*/

#constructs the entire query for 2014
USE hcup;
SELECT  visitlink, t.N, t.D, t.I, t.M, t.W, 
    t.X, t.N + t.D + t.I + t.M + t.W + t.X AS total 
FROM (
SELECT  visitlink, 
    count(CASE WHEN marital_2014_N_ct > 0 THEN 1 END) AS N,
        count(CASE WHEN marital_2014_D_ct > 0 THEN 1 END) AS D,
        count(CASE WHEN marital_2014_I_ct > 0 THEN 1 END) AS I,
        count(CASE WHEN marital_2014_M_ct > 0 THEN 1 END) AS M,
        count(CASE WHEN marital_2014_W_ct > 0 THEN 1 END) AS W,
        count(CASE WHEN marital_2014_X_ct > 0 THEN 1 END) AS X
FROM hcup_pat_marital
GROUP BY  visitlink) t;

#breaksdown the number of patients w/ increasing number of marital changes
USE hcup;
SELECT  COUNT(*), 
    t.N + t.D + t.I + t.M + t.W + t.X AS total 
FROM (
SELECT  visitlink, 
    count(CASE WHEN marital_2014_N_ct > 0 THEN 1 END) AS N,
        count(CASE WHEN marital_2014_D_ct > 0 THEN 1 END) AS D,
        count(CASE WHEN marital_2014_I_ct > 0 THEN 1 END) AS I,
        count(CASE WHEN marital_2014_M_ct > 0 THEN 1 END) AS M,
        count(CASE WHEN marital_2014_W_ct > 0 THEN 1 END) AS W,
        count(CASE WHEN marital_2014_X_ct > 0 THEN 1 END) AS X
FROM hcup_pat_marital
GROUP BY  visitlink) t
GROUP BY  total; 

/* 2014
  + ------------- + -------------- +
  | pat_ct        | marital_ct     |
  + ------------- + -------------- +
  | 1,091,679     | 0              | number of people who are null
  | 2,068,038     | 1              |
  | 97,169        | 2              |
  | 6,509         | 3              |
  | 452           | 4              |
  | 17            | 5              |
  | 1             | 6              | the only 6 marital_ct is the null visitlink
  + ------------- + -------------- +
*/

/*------------------------More Marital analysis------------------*/
CREATE TABLE `hcup_pat_marital_chngs` 
(
  `ID` int NOT NULL AUTO_INCREMENT,
  `visitlink` varchar(16) DEFAULT NULL,
  
  `marital_status_2013sid` varchar(1) DEFAULT NULL,
  `daystoevent_2013sid` int(6) DEFAULT NULL,

  `marital_status_2013sedd` varchar(1) DEFAULT NULL,
  `daystoevent_2013sedd` int(6) DEFAULT NULL,

  `marital_status_2013sasd` varchar(1) DEFAULT NULL,
  `daystoevent_2013sasd` int(6) DEFAULT NULL,

  `marital_status_2014sid` varchar(1) DEFAULT NULL,
  `daystoevent_2014sid` int(6) DEFAULT NULL,

  `marital_status_2014sedd` varchar(1) DEFAULT NULL,
  `daystoevent_2014sedd` int(6) DEFAULT NULL,

  `marital_status_2014sasd` varchar(1) DEFAULT NULL,
  `daystoevent_2014sasd` int(6) DEFAULT NULL,

  `marital_flag_2013sid` int DEFAULT NULL,
  `marital_flag_2013sedd` int DEFAULT NULL,
  `marital_flag_2013sasd` int DEFAULT NULL,

  `marital_flag_2014sid` int(1) DEFAULT NULL,
  `marital_flag_2014sedd` int(1) DEFAULT NULL,
  `marital_flag_2014sasd` int(1) DEFAULT NULL,

  `marital_flag` int(1) DEFAULT NULL,

  PRIMARY KEY (ID)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*
  Had to create a new key b/c visitlinks get duplicated
  Rule: patients who have 3 or more distinct marriage statuses in the space of 60 days are flagged
  Shortcoming, only examines within 1 database/table (2013_sid_core, 2013_sasd_core, etc)
*/

#2013
INSERT INTO hcup_pat_marital_chngs (visitlink, marital_status_2013sid, daystoevent_2013sid)
SELECT visitlink, maritalstatusub04, CAST(daystoevent AS UNSIGNED) daystoevent
FROM 2013_sid_core
WHERE maritalstatusub04 IS NOT NULL AND maritalstatusub04 <> '';

INSERT INTO hcup_pat_marital_chngs (visitlink, marital_status_2013sedd, daystoevent_2013sedd)
SELECT visitlink, maritalstatusub04, CAST(daystoevent AS UNSIGNED) daystoevent
FROM 2013_sedd_core
WHERE maritalstatusub04 IS NOT NULL AND maritalstatusub04 <> '';

INSERT INTO hcup_pat_marital_chngs (visitlink, marital_status_2013sasd, daystoevent_2013sasd)
SELECT visitlink, maritalstatusub04, CAST(daystoevent AS UNSIGNED) daystoevent
FROM 2013_sasd_core
WHERE maritalstatusub04 IS NOT NULL AND maritalstatusub04 <> '';

#2014
INSERT INTO hcup_pat_marital_chngs (visitlink, marital_status_2014sid, daystoevent_2014sid)
SELECT visitlink, maritalstatusub04, CAST(daystoevent AS UNSIGNED) daystoevent
FROM 2014_sid_core
WHERE maritalstatusub04 IS NOT NULL AND maritalstatusub04 <> '';

INSERT INTO hcup_pat_marital_chngs (visitlink, marital_status_2014sedd, daystoevent_2014sedd)
SELECT visitlink, maritalstatusub04, CAST(daystoevent AS UNSIGNED) daystoevent
FROM 2014_sedd_core
WHERE maritalstatusub04 IS NOT NULL AND maritalstatusub04 <> '';

INSERT INTO hcup_pat_marital_chngs (visitlink, marital_status_2014sasd, daystoevent_2014sasd)
SELECT visitlink, maritalstatusub04, CAST(daystoevent AS UNSIGNED) daystoevent
FROM 2014_sasd_core
WHERE maritalstatusub04 IS NOT NULL AND maritalstatusub04 <> '';

SET SQL_SAFE_UPDATES = 0;

UPDATE hcup_pat_marital_chngs
SET marital_flag_2013sid = IF( visitlink IN (SELECT visitlink FROM (
  SELECT visitlink, COUNT(DISTINCT marital_status_2013sid) as total,
  MAX(daystoevent_2013sid) - MIN(daystoevent_2013sid) as diff
  FROM hcup_pat_marital_chngs
  GROUP BY visitlink) t
  WHERE t.total > 2 AND diff <= 60), 1, 0);

UPDATE hcup_pat_marital_chngs
SET marital_flag_2013sasd = IF( visitlink IN (SELECT visitlink FROM (
  SELECT visitlink, COUNT(DISTINCT marital_status_2013sasd) as total,
  MAX(daystoevent_2013sasd) - MIN(daystoevent_2013sasd) as diff
  FROM hcup_pat_marital_chngs
  GROUP BY visitlink) t
  WHERE t.total > 2 AND diff <= 60), 1, 0);

UPDATE hcup_pat_marital_chngs
SET marital_flag_2013sedd = IF( visitlink IN (SELECT visitlink FROM (
  SELECT visitlink, COUNT(DISTINCT marital_status_2013sedd) as total,
  MAX(daystoevent_2013sedd) - MIN(daystoevent_2013sedd) as diff
  FROM hcup_pat_marital_chngs
  GROUP BY visitlink) t
  WHERE t.total > 2 AND diff <= 60), 1, 0);

#2014
UPDATE hcup_pat_marital_chngs
SET marital_flag_2014sid = IF( visitlink IN (SELECT visitlink FROM (
  SELECT visitlink, COUNT(DISTINCT marital_status_2014sid) as total,
  MAX(daystoevent_2014sid) - MIN(daystoevent_2014sid) as diff
  FROM hcup_pat_marital_chngs
  GROUP BY visitlink) t
  WHERE t.total > 2 AND diff <= 60), 1, 0);

UPDATE hcup_pat_marital_chngs
SET marital_flag_2014sasd = IF( visitlink IN (SELECT visitlink FROM (
  SELECT visitlink, COUNT(DISTINCT marital_status_2014sasd) as total,
  MAX(daystoevent_2014sasd) - MIN(daystoevent_2014sasd) as diff
  FROM hcup_pat_marital_chngs
  GROUP BY visitlink) t
  WHERE t.total > 2 AND diff <= 60), 1, 0);

UPDATE hcup_pat_marital_chngs
SET marital_flag_2014sedd = IF( visitlink IN (SELECT visitlink FROM (
  SELECT visitlink, COUNT(DISTINCT marital_status_2014sedd) as total,
  MAX(daystoevent_2014sedd) - MIN(daystoevent_2014sedd) as diff
  FROM hcup_pat_marital_chngs
  GROUP BY visitlink) t
  WHERE t.total > 2 AND diff <= 60), 1, 0);


/*-------------------------------------------------------------------------*/

#number of patients vs # of distinct marital statuses 2013
SELECT COUNT(*), t1.tot FROM (
  SELECT visitlink, COUNT(DISTINCT maritalstatusub04) as tot FROM (
    SELECT visitlink, maritalstatusub04 FROM 2013_sid_core
    UNION SELECT visitlink, maritalstatusub04 FROM 2013_sedd_core
    UNION SELECT visitlink, maritalstatusub04 FROM 2013_sasd_core
    WHERE maritalstatusub04 IS NOT NULL AND maritalstatusub04 <> '' AND visitlink IS NOT NULL) t
  GROUP BY visitlink) t1
GROUP BY t1.tot;

/* Number of patients vs Number of distinct marital statues 2013
  + ------------- + -------------- +
  | pat_ct        | Distinct       |
  |               | MaritalStatus  |
  + ------------- + -------------- +
  | 2,065,975     | 1              |
  | 90,276        | 2              |
  | 6,349         | 3              | 
  | 491           | 4              |
  | 34            | 5              |
  | 2*            | 6              | 
  + ------------- + -------------- +
  *one of these is the null visitlink
*/

#number of patients vs # of distinct marital statuses 2014
SELECT COUNT(*), t1.tot FROM (
SELECT visitlink, COUNT(DISTINCT maritalstatusub04) as tot FROM (
SELECT visitlink, maritalstatusub04 FROM 2014_sid_core
UNION SELECT visitlink, maritalstatusub04 FROM 2014_sedd_core
UNION SELECT visitlink, maritalstatusub04 FROM 2014_sasd_core
WHERE maritalstatusub04 IS NOT NULL AND maritalstatusub04 <> '' AND visitlink IS NOT NULL) t
GROUP BY visitlink) t1
GROUP BY t1.tot;

/* Number of patients vs Number of distinct marital statues 2014
  + ------------- + -------------- +
  | pat_ct        | Distinct       |
  |               | MaritalStatus  |
  + ------------- + -------------- +
  | 2,065,176     | 1              |
  | 82,397        | 2              |
  | 5,315         | 3              | 
  | 376           | 4              |
  | 14            | 5              |
  | 1*            | 6              | 
  + ------------- + -------------- +
  *one of these is the null visitlink
*/

#2013
SELECT COUNT(visitlink), tot FROM (
SELECT visitlink, COUNT(DISTINCT maritalstatusub04) as tot FROM (
SELECT visitlink, maritalstatusub04 FROM 2013_sid_core
UNION SELECT visitlink, maritalstatusub04 FROM 2013_sedd_core
UNION SELECT visitlink, maritalstatusub04 FROM 2013_sasd_core
WHERE maritalstatusub04 IS NOT NULL AND maritalstatusub04 <> '' AND visitlink IS NOT NULL) t
GROUP BY visitlink) T1
WHERE visitlink IN (SELECT visitlink FROM dq_master WHERE maritalFlag = 1)
GROUP BY tot;

#Number of patients who are flagged by the marital flag and their break down in number of 
#distinct marital statuses

/* Number of patients vs Number of distinct marital statues 2013
  + ------------- + -------------- +
  | pat_ct w/     | Distinct       |
  | maritalFlag   | MaritalStatus  |
  + ------------- + -------------- +
  | 34            | 1              |
  | 18            | 2              |
  | 127           | 3              | 
  | 13            | 4              |
  | 2             | 5              |
  + ------------- + -------------- +
  *one of these is the null visitlink
*/

#2014
SELECT COUNT(visitlink), tot FROM (
SELECT visitlink, COUNT(DISTINCT maritalstatusub04) as tot FROM (
SELECT visitlink, maritalstatusub04 FROM 2014_sid_core
UNION SELECT visitlink, maritalstatusub04 FROM 2014_sedd_core
UNION SELECT visitlink, maritalstatusub04 FROM 2014_sasd_core
WHERE maritalstatusub04 IS NOT NULL AND maritalstatusub04 <> '' AND visitlink IS NOT NULL) t
GROUP BY visitlink) T1
WHERE visitlink IN (SELECT visitlink FROM dq_master WHERE maritalFlag = 1)
GROUP BY tot;

#Number of patients who are flagged by the marital flag and their break down in number of 
#distinct marital statuses

/* Number of patients vs Number of distinct marital statues 2014
  + ------------- + -------------- +
  | pat_ct w/     | Distinct       |
  | maritalFlag   | MaritalStatus  |
  + ------------- + -------------- +
  | 39            | 1              |
  | 26            | 2              |
  | 92            | 3              | 
  | 16            | 4              |
  | 1             | 5              |
  + ------------- + -------------- +
  *one of these is the null visitlink
*/
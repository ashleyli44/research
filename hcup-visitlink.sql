####################################################################
#
#	HCUP Database Normalization Process
#	Hadi Kharrazi @ Dec 2017
#	Maryland SID, SEDD, SASD databases
#	Year: 2013 & 2014
#
####################################################################

####### Stage 0 :: Cleaning up the DB

# ------------- SID

SELECT * FROM 2013_sid_chgs WHERE LEFT(`key`, 1) <> '2';
SELECT * FROM 2013_sid_core WHERE LEFT(`key`, 1) <> '2';
SELECT * FROM 2013_sid_dx_pr_grps WHERE LEFT(`key`, 1) <> '2';
SELECT * FROM 2013_sid_severity WHERE LEFT(`key`, 1) <> '2';

SELECT * FROM 2014_sid_chgs WHERE LEFT(`key`, 1) <> '2';
SELECT * FROM 2014_sid_core WHERE LEFT(`key`, 1) <> '2';
SELECT * FROM 2014_sid_dx_pr_grps WHERE LEFT(`key`, 1) <> '2';
SELECT * FROM 2014_sid_severity WHERE LEFT(`key`, 1) <> '2';

DELETE FROM 2013_sid_chgs WHERE LEFT(`key`, 1) <> '2'; -- 2 rows
DELETE FROM 2013_sid_core WHERE LEFT(`key`, 1) <> '2'; -- 0 rows
DELETE FROM 2013_sid_dx_pr_grps WHERE LEFT(`key`, 1) <> '2'; -- 2 rows
DELETE FROM 2013_sid_severity WHERE LEFT(`key`, 1) <> '2'; -- 2 rows

DELETE FROM 2014_sid_chgs WHERE LEFT(`key`, 1) <> '2'; -- 2 rows
DELETE FROM 2014_sid_core WHERE LEFT(`key`, 1) <> '2'; -- 0 rows
DELETE FROM 2014_sid_dx_pr_grps WHERE LEFT(`key`, 1) <> '2'; -- 2 rows
DELETE FROM 2014_sid_severity WHERE LEFT(`key`, 1) <> '2'; -- 2 rows

# ------------- SEDD

SELECT * FROM 2013_sedd_chgs WHERE LEFT(`key`, 1) <> '2';
SELECT * FROM 2013_sedd_core WHERE LEFT(`key`, 1) <> '2';
SELECT * FROM 2013_sedd_dx_pr_grps WHERE LEFT(`key`, 1) <> '2';

SELECT * FROM 2014_sedd_chgs WHERE LEFT(`key`, 1) <> '2';
SELECT * FROM 2014_sedd_core WHERE LEFT(`key`, 1) <> '2';
SELECT * FROM 2014_sedd_dx_pr_grps WHERE LEFT(`key`, 1) <> '2';

DELETE FROM 2013_sedd_chgs WHERE LEFT(`key`, 1) <> '2'; -- 2 rows
DELETE FROM 2013_sedd_core WHERE LEFT(`key`, 1) <> '2'; -- 0 rows
DELETE FROM 2013_sedd_dx_pr_grps WHERE LEFT(`key`, 1) <> '2'; -- 2 rows

DELETE FROM 2014_sedd_chgs WHERE LEFT(`key`, 1) <> '2'; -- 2 rows
DELETE FROM 2014_sedd_core WHERE LEFT(`key`, 1) <> '2'; -- 0 rows
DELETE FROM 2014_sedd_dx_pr_grps WHERE LEFT(`key`, 1) <> '2'; -- 2 rows

# ------------- SASD

SELECT * FROM 2013_sasd_chgs WHERE LEFT(`key`, 1) <> '2';
SELECT * FROM 2013_sasd_core WHERE LEFT(`key`, 1) <> '2';
SELECT * FROM 2013_sasd_dx_pr_grps WHERE LEFT(`key`, 1) <> '2';

SELECT * FROM 2014_sasd_chgs WHERE LEFT(`key`, 1) <> '2';
SELECT * FROM 2014_sasd_core WHERE LEFT(`key`, 1) <> '2';
SELECT * FROM 2014_sasd_dx_pr_grps WHERE LEFT(`key`, 1) <> '2';

DELETE FROM 2013_sasd_chgs WHERE LEFT(`key`, 1) <> '2'; -- 2 rows
DELETE FROM 2013_sasd_core WHERE LEFT(`key`, 1) <> '2'; -- 0 rows
DELETE FROM 2013_sasd_dx_pr_grps WHERE LEFT(`key`, 1) <> '2'; -- 2 rows

DELETE FROM 2014_sasd_chgs WHERE LEFT(`key`, 1) <> '2'; -- 2 rows
DELETE FROM 2014_sasd_core WHERE LEFT(`key`, 1) <> '2'; -- 0 rows
DELETE FROM 2014_sasd_dx_pr_grps WHERE LEFT(`key`, 1) <> '2'; -- 2 rows

####### Stage 1 :: Add the HCUP ~ AHA mapping table

CREATE TABLE `hcup_aha` 
(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ahaid` varchar(12) DEFAULT NULL,
  `dshospid` int(11) DEFAULT NULL,
  `hospid` int(11) DEFAULT NULL,
  `community_nonrehab_nonltac` int(11) DEFAULT NULL,
  `freestanding` int(11) DEFAULT NULL,
  `hfipsstco` int(11) DEFAULT NULL,
  `hospst` varchar(5) DEFAULT NULL,
  `yr` int(11) DEFAULT NULL,
  `db` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ahaid_index` (`ahaid`),
  KEY `dshospid_index` (`dshospid`),
  KEY `hospid_index` (`hospid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `hcup_aha` VALUES (1,'6320580',210001,24065,1,NULL,24043,'MD',2013,'SID'),(2,'6320330',210002,24078,1,NULL,24510,'MD',2013,'SID'),(3,'6320610',210003,24005,1,NULL,24033,'MD',2013,'SID'),(4,'6320813',210004,24080,1,NULL,24031,'MD',2013,'SID'),(5,'6320540',210005,24050,1,NULL,24021,'MD',2013,'SID'),(6,'6320590',210006,24051,1,NULL,24025,'MD',2013,'SID'),(7,'6320220',210008,24077,1,NULL,24510,'MD',2013,'SID'),(8,'6320180',210009,24085,1,NULL,24510,'MD',2013,'SID'),(9,'6320370',210010,24048,1,NULL,24019,'MD',2013,'SID'),(10,'6320250',210011,24008,1,NULL,24510,'MD',2013,'SID'),(11,'6320280',210012,24082,1,NULL,24510,'MD',2013,'SID'),(12,'6320090',210013,24016,1,NULL,24510,'MD',2013,'SID'),(13,'6320120',210015,24073,1,NULL,24005,'MD',2013,'SID'),(14,'6322080',210016,24042,1,NULL,24031,'MD',2013,'SID'),(15,'6320685',210017,24087,1,NULL,24023,'MD',2013,'SID'),(16,'6320690',210018,24057,1,NULL,24031,'MD',2013,'SID'),(17,'6320785',210019,24041,1,NULL,24045,'MD',2013,'SID'),(18,'6320350',210022,24084,1,NULL,24031,'MD',2013,'SID'),(19,'6320020',210023,24045,1,NULL,24003,'MD',2013,'SID'),(20,'6320310',210024,24068,1,NULL,24510,'MD',2013,'SID'),(21,'6320206',210027,24100,1,NULL,24001,'MD',2013,'SID'),(22,'6320670',210028,24075,1,NULL,24037,'MD',2013,'SID'),(23,'6320050',210029,24018,1,NULL,24510,'MD',2013,'SID'),(24,'6320420',210030,24034,1,NULL,24029,'MD',2013,'SID'),(25,'6320500',210032,24047,1,NULL,24015,'MD',2013,'SID'),(26,'6320880',210033,24019,1,NULL,24013,'MD',2013,'SID'),(27,'6320290',210034,24063,1,NULL,24510,'MD',2013,'SID'),(28,'6320640',210035,24062,1,NULL,24017,'MD',2013,'SID'),(29,'6320480',210037,24036,1,NULL,24041,'MD',2013,'SID'),(30,'6320200',210038,24043,1,NULL,24510,'MD',2013,'SID'),(31,'6320730',210039,24033,1,NULL,24009,'MD',2013,'SID'),(32,'6320735',210040,24038,1,NULL,24005,'MD',2013,'SID'),(33,'6320569',210043,24011,1,NULL,24003,'MD',2013,'SID'),(34,'6320125',210044,24035,1,NULL,24005,'MD',2013,'SID'),(35,'6320440',210045,24067,1,NULL,24039,'MD',2013,'SID'),(36,'6320460',210048,24055,1,NULL,24027,'MD',2013,'SID'),(37,'6320515',210049,24009,1,NULL,24025,'MD',2013,'SID'),(38,'6320645',210051,24064,1,NULL,24033,'MD',2013,'SID'),(39,'6320655',210055,24049,1,NULL,24033,'MD',2013,'SID'),(40,'6320127',210056,24007,1,NULL,24510,'MD',2013,'SID'),(41,'6320775',210057,24086,1,NULL,24031,'MD',2013,'SID'),(42,'6320170',210058,24010,0,NULL,24005,'MD',2013,'SID'),(43,'6320008',210060,24091,1,NULL,24033,'MD',2013,'SID'),(44,'6320003',210061,24088,1,NULL,24047,'MD',2013,'SID'),(45,'6320416',210062,24081,1,NULL,24033,'MD',2013,'SID'),(46,'6320260',210063,24074,1,NULL,24005,'MD',2013,'SID'),(47,'6320180',210904,24085,1,NULL,24510,'MD',2013,'SID'),(48,'6320004',213028,24090,0,NULL,24045,'MD',2013,'SID'),(49,'6320013',213029,24094,0,NULL,24031,'MD',2013,'SID'),(50,'6320580',210001,24065,1,NULL,24043,'MD',2014,'SID'),(51,'6320330',210002,24078,1,NULL,24510,'MD',2014,'SID'),(52,'6320610',210003,24005,1,NULL,24033,'MD',2014,'SID'),(53,'6320813',210004,24080,1,NULL,24031,'MD',2014,'SID'),(54,'6320540',210005,24050,1,NULL,24021,'MD',2014,'SID'),(55,'6320590',210006,24051,1,NULL,24025,'MD',2014,'SID'),(56,'6320220',210008,24077,1,NULL,24510,'MD',2014,'SID'),(57,'6320180',210009,24085,1,NULL,24510,'MD',2014,'SID'),(58,'6320370',210010,24048,1,NULL,24019,'MD',2014,'SID'),(59,'6320250',210011,24008,1,NULL,24510,'MD',2014,'SID'),(60,'6320280',210012,24082,1,NULL,24510,'MD',2014,'SID'),(61,'6320090',210013,24016,1,NULL,24510,'MD',2014,'SID'),(62,'6320120',210015,24073,1,NULL,24005,'MD',2014,'SID'),(63,'6322080',210016,24042,1,NULL,24031,'MD',2014,'SID'),(64,'6320685',210017,24087,1,NULL,24023,'MD',2014,'SID'),(65,'6320690',210018,24057,1,NULL,24031,'MD',2014,'SID'),(66,'6320785',210019,24041,1,NULL,24045,'MD',2014,'SID'),(67,'6320350',210022,24084,1,NULL,24031,'MD',2014,'SID'),(68,'6320020',210023,24045,1,NULL,24003,'MD',2014,'SID'),(69,'6320310',210024,24068,1,NULL,24510,'MD',2014,'SID'),(70,'6320206',210027,24100,1,NULL,24001,'MD',2014,'SID'),(71,'6320670',210028,24075,1,NULL,24037,'MD',2014,'SID'),(72,'6320050',210029,24018,1,NULL,24510,'MD',2014,'SID'),(73,'6320420',210030,24034,1,NULL,24029,'MD',2014,'SID'),(74,'6320500',210032,24047,1,NULL,24015,'MD',2014,'SID'),(75,'6320880',210033,24019,1,NULL,24013,'MD',2014,'SID'),(76,'6320290',210034,24063,1,NULL,24510,'MD',2014,'SID'),(77,'6320640',210035,24062,1,NULL,24017,'MD',2014,'SID'),(78,'6320480',210037,24036,1,NULL,24041,'MD',2014,'SID'),(79,'6320200',210038,24043,1,NULL,24510,'MD',2014,'SID'),(80,'6320730',210039,24033,1,NULL,24009,'MD',2014,'SID'),(81,'6320735',210040,24038,1,NULL,24005,'MD',2014,'SID'),(82,'6320569',210043,24011,1,NULL,24003,'MD',2014,'SID'),(83,'6320125',210044,24035,1,NULL,24005,'MD',2014,'SID'),(84,'6320440',210045,24067,1,NULL,24039,'MD',2014,'SID'),(85,'6320460',210048,24055,1,NULL,24027,'MD',2014,'SID'),(86,'6320515',210049,24009,1,NULL,24025,'MD',2014,'SID'),(87,'6320645',210051,24064,1,NULL,24033,'MD',2014,'SID'),(88,'6320655',210055,24049,1,NULL,24033,'MD',2014,'SID'),(89,'6320127',210056,24007,1,NULL,24510,'MD',2014,'SID'),(90,'6320775',210057,24086,1,NULL,24031,'MD',2014,'SID'),(91,'6320170',210058,24010,0,NULL,24005,'MD',2014,'SID'),(92,'6320008',210060,24091,1,NULL,24033,'MD',2014,'SID'),(93,'6320003',210061,24088,1,NULL,24047,'MD',2014,'SID'),(94,'6320416',210062,24081,1,NULL,24033,'MD',2014,'SID'),(95,'6320260',210063,24074,1,NULL,24005,'MD',2014,'SID'),(96,NULL,210065,NULL,NULL,NULL,NULL,'MD',2014,'SID'),(97,'6320180',210904,24085,1,NULL,24510,'MD',2014,'SID'),(98,'6320004',213028,24090,0,NULL,24045,'MD',2014,'SID'),(99,'6320013',213029,24094,0,NULL,24031,'MD',2014,'SID'),(100,'6320140',213300,24095,1,NULL,24510,'MD',2014,'SID'),(101,'6320580',210001,24065,1,NULL,24043,'MD',2013,'SEDD'),(102,'6320330',210002,24078,1,NULL,24510,'MD',2013,'SEDD'),(103,'6320610',210003,24005,1,NULL,24033,'MD',2013,'SEDD'),(104,'6320813',210004,24080,1,NULL,24031,'MD',2013,'SEDD'),(105,'6320540',210005,24050,1,NULL,24021,'MD',2013,'SEDD'),(106,'6320590',210006,24051,1,NULL,24025,'MD',2013,'SEDD'),(107,'6320220',210008,24077,1,NULL,24510,'MD',2013,'SEDD'),(108,'6320180',210009,24085,1,NULL,24510,'MD',2013,'SEDD'),(109,'6320370',210010,24048,1,NULL,24019,'MD',2013,'SEDD'),(110,'6320250',210011,24008,1,NULL,24510,'MD',2013,'SEDD'),(111,'6320280',210012,24082,1,NULL,24510,'MD',2013,'SEDD'),(112,'6320090',210013,24016,1,NULL,24510,'MD',2013,'SEDD'),(113,'6320120',210015,24073,1,NULL,24005,'MD',2013,'SEDD'),(114,'6322080',210016,24042,1,NULL,24031,'MD',2013,'SEDD'),(115,'6320685',210017,24087,1,NULL,24023,'MD',2013,'SEDD'),(116,'6320690',210018,24057,1,NULL,24031,'MD',2013,'SEDD'),(117,'6320785',210019,24041,1,NULL,24045,'MD',2013,'SEDD'),(118,'6320350',210022,24084,1,NULL,24031,'MD',2013,'SEDD'),(119,'6320020',210023,24045,1,NULL,24003,'MD',2013,'SEDD'),(120,'6320310',210024,24068,1,NULL,24510,'MD',2013,'SEDD'),(121,'6320206',210027,24100,1,NULL,24001,'MD',2013,'SEDD'),(122,'6320670',210028,24075,1,NULL,24037,'MD',2013,'SEDD'),(123,'6320050',210029,24018,1,NULL,24510,'MD',2013,'SEDD'),(124,'6320420',210030,24034,1,NULL,24029,'MD',2013,'SEDD'),(125,'6320500',210032,24047,1,NULL,24015,'MD',2013,'SEDD'),(126,'6320880',210033,24019,1,NULL,24013,'MD',2013,'SEDD'),(127,'6320290',210034,24063,1,NULL,24510,'MD',2013,'SEDD'),(128,'6320640',210035,24062,1,NULL,24017,'MD',2013,'SEDD'),(129,'6320480',210037,24036,1,NULL,24041,'MD',2013,'SEDD'),(130,'6320200',210038,24043,1,NULL,24510,'MD',2013,'SEDD'),(131,'6320730',210039,24033,1,NULL,24009,'MD',2013,'SEDD'),(132,'6320735',210040,24038,1,NULL,24005,'MD',2013,'SEDD'),(133,'6320569',210043,24011,1,NULL,24003,'MD',2013,'SEDD'),(134,'6320125',210044,24035,1,NULL,24005,'MD',2013,'SEDD'),(135,'6320440',210045,24067,1,NULL,24039,'MD',2013,'SEDD'),(136,'6320460',210048,24055,1,NULL,24027,'MD',2013,'SEDD'),(137,'6320515',210049,24009,1,NULL,24025,'MD',2013,'SEDD'),(138,'6320645',210051,24064,1,NULL,24033,'MD',2013,'SEDD'),(139,'6320655',210055,24049,1,NULL,24033,'MD',2013,'SEDD'),(140,'6320127',210056,24007,1,NULL,24510,'MD',2013,'SEDD'),(141,'6320775',210057,24086,1,NULL,24031,'MD',2013,'SEDD'),(142,'6320170',210058,24010,0,NULL,24005,'MD',2013,'SEDD'),(143,'6320008',210060,24091,1,NULL,24033,'MD',2013,'SEDD'),(144,'6320003',210061,24088,1,NULL,24047,'MD',2013,'SEDD'),(145,'6320416',210062,24081,1,NULL,24033,'MD',2013,'SEDD'),(146,'6320260',210063,24074,1,NULL,24005,'MD',2013,'SEDD'),(147,NULL,210087,NULL,NULL,NULL,NULL,'MD',2013,'SEDD'),(148,NULL,210088,NULL,NULL,NULL,NULL,'MD',2013,'SEDD'),(149,NULL,210333,NULL,NULL,NULL,NULL,'MD',2013,'SEDD'),(150,'6320180',210904,24085,1,NULL,24510,'MD',2013,'SEDD'),(151,'6320580',210001,24065,1,NULL,24043,'MD',2014,'SEDD'),(152,'6320330',210002,24078,1,NULL,24510,'MD',2014,'SEDD'),(153,'6320610',210003,24005,1,NULL,24033,'MD',2014,'SEDD'),(154,'6320813',210004,24080,1,NULL,24031,'MD',2014,'SEDD'),(155,'6320540',210005,24050,1,NULL,24021,'MD',2014,'SEDD'),(156,'6320590',210006,24051,1,NULL,24025,'MD',2014,'SEDD'),(157,'6320220',210008,24077,1,NULL,24510,'MD',2014,'SEDD'),(158,'6320180',210009,24085,1,NULL,24510,'MD',2014,'SEDD'),(159,'6320370',210010,24048,1,NULL,24019,'MD',2014,'SEDD'),(160,'6320250',210011,24008,1,NULL,24510,'MD',2014,'SEDD'),(161,'6320280',210012,24082,1,NULL,24510,'MD',2014,'SEDD'),(162,'6320090',210013,24016,1,NULL,24510,'MD',2014,'SEDD'),(163,'6320120',210015,24073,1,NULL,24005,'MD',2014,'SEDD'),(164,'6322080',210016,24042,1,NULL,24031,'MD',2014,'SEDD'),(165,'6320685',210017,24087,1,NULL,24023,'MD',2014,'SEDD'),(166,'6320690',210018,24057,1,NULL,24031,'MD',2014,'SEDD'),(167,'6320785',210019,24041,1,NULL,24045,'MD',2014,'SEDD'),(168,'6320350',210022,24084,1,NULL,24031,'MD',2014,'SEDD'),(169,'6320020',210023,24045,1,NULL,24003,'MD',2014,'SEDD'),(170,'6320310',210024,24068,1,NULL,24510,'MD',2014,'SEDD'),(171,'6320206',210027,24100,1,NULL,24001,'MD',2014,'SEDD'),(172,'6320670',210028,24075,1,NULL,24037,'MD',2014,'SEDD'),(173,'6320050',210029,24018,1,NULL,24510,'MD',2014,'SEDD'),(174,'6320420',210030,24034,1,NULL,24029,'MD',2014,'SEDD'),(175,'6320500',210032,24047,1,NULL,24015,'MD',2014,'SEDD'),(176,'6320880',210033,24019,1,NULL,24013,'MD',2014,'SEDD'),(177,'6320290',210034,24063,1,NULL,24510,'MD',2014,'SEDD'),(178,'6320640',210035,24062,1,NULL,24017,'MD',2014,'SEDD'),(179,'6320480',210037,24036,1,NULL,24041,'MD',2014,'SEDD'),(180,'6320200',210038,24043,1,NULL,24510,'MD',2014,'SEDD'),(181,'6320730',210039,24033,1,NULL,24009,'MD',2014,'SEDD'),(182,'6320735',210040,24038,1,NULL,24005,'MD',2014,'SEDD'),(183,'6320569',210043,24011,1,NULL,24003,'MD',2014,'SEDD'),(184,'6320125',210044,24035,1,NULL,24005,'MD',2014,'SEDD'),(185,'6320440',210045,24067,1,NULL,24039,'MD',2014,'SEDD'),(186,'6320460',210048,24055,1,NULL,24027,'MD',2014,'SEDD'),(187,'6320515',210049,24009,1,NULL,24025,'MD',2014,'SEDD'),(188,'6320645',210051,24064,1,NULL,24033,'MD',2014,'SEDD'),(189,'6320655',210055,24049,1,NULL,24033,'MD',2014,'SEDD'),(190,'6320127',210056,24007,1,NULL,24510,'MD',2014,'SEDD'),(191,'6320775',210057,24086,1,NULL,24031,'MD',2014,'SEDD'),(192,'6320008',210060,24091,1,NULL,24033,'MD',2014,'SEDD'),(193,'6320003',210061,24088,1,NULL,24047,'MD',2014,'SEDD'),(194,'6320416',210062,24081,1,NULL,24033,'MD',2014,'SEDD'),(195,'6320260',210063,24074,1,NULL,24005,'MD',2014,'SEDD'),(196,NULL,210065,NULL,NULL,NULL,NULL,'MD',2014,'SEDD'),(197,NULL,210087,NULL,NULL,NULL,NULL,'MD',2014,'SEDD'),(198,NULL,210088,NULL,NULL,NULL,NULL,'MD',2014,'SEDD'),(199,NULL,210333,NULL,NULL,NULL,NULL,'MD',2014,'SEDD'),(200,'6320180',210904,24085,1,NULL,24510,'MD',2014,'SEDD'),(201,'6320580',210001,24065,1,0,24043,'MD',2013,'SASD'),(202,'6320330',210002,24078,1,0,24510,'MD',2013,'SASD'),(203,'6320610',210003,24005,1,0,24033,'MD',2013,'SASD'),(204,'6320813',210004,24080,1,0,24031,'MD',2013,'SASD'),(205,'6320540',210005,24050,1,0,24021,'MD',2013,'SASD'),(206,'6320590',210006,24051,1,0,24025,'MD',2013,'SASD'),(207,'6320220',210008,24077,1,0,24510,'MD',2013,'SASD'),(208,'6320180',210009,24085,1,0,24510,'MD',2013,'SASD'),(209,'6320370',210010,24048,1,0,24019,'MD',2013,'SASD'),(210,'6320250',210011,24008,1,0,24510,'MD',2013,'SASD'),(211,'6320280',210012,24082,1,0,24510,'MD',2013,'SASD'),(212,'6320090',210013,24016,1,0,24510,'MD',2013,'SASD'),(213,'6320120',210015,24073,1,0,24005,'MD',2013,'SASD'),(214,'6322080',210016,24042,1,0,24031,'MD',2013,'SASD'),(215,'6320685',210017,24087,1,0,24023,'MD',2013,'SASD'),(216,'6320690',210018,24057,1,0,24031,'MD',2013,'SASD'),(217,'6320785',210019,24041,1,0,24045,'MD',2013,'SASD'),(218,'6320350',210022,24084,1,0,24031,'MD',2013,'SASD'),(219,'6320020',210023,24045,1,0,24003,'MD',2013,'SASD'),(220,'6320310',210024,24068,1,0,24510,'MD',2013,'SASD'),(221,'6320206',210027,24100,1,0,24001,'MD',2013,'SASD'),(222,'6320670',210028,24075,1,0,24037,'MD',2013,'SASD'),(223,'6320050',210029,24018,1,0,24510,'MD',2013,'SASD'),(224,'6320420',210030,24034,1,0,24029,'MD',2013,'SASD'),(225,'6320500',210032,24047,1,0,24015,'MD',2013,'SASD'),(226,'6320880',210033,24019,1,0,24013,'MD',2013,'SASD'),(227,'6320290',210034,24063,1,0,24510,'MD',2013,'SASD'),(228,'6320640',210035,24062,1,0,24017,'MD',2013,'SASD'),(229,'6320480',210037,24036,1,0,24041,'MD',2013,'SASD'),(230,'6320200',210038,24043,1,0,24510,'MD',2013,'SASD'),(231,'6320730',210039,24033,1,0,24009,'MD',2013,'SASD'),(232,'6320735',210040,24038,1,0,24005,'MD',2013,'SASD'),(233,'6320569',210043,24011,1,0,24003,'MD',2013,'SASD'),(234,'6320125',210044,24035,1,0,24005,'MD',2013,'SASD'),(235,'6320440',210045,24067,1,0,24039,'MD',2013,'SASD'),(236,'6320460',210048,24055,1,0,24027,'MD',2013,'SASD'),(237,'6320515',210049,24009,1,0,24025,'MD',2013,'SASD'),(238,'6320645',210051,24064,1,0,24033,'MD',2013,'SASD'),(239,'6320655',210055,24049,1,0,24033,'MD',2013,'SASD'),(240,'6320127',210056,24007,1,0,24510,'MD',2013,'SASD'),(241,'6320775',210057,24086,1,0,24031,'MD',2013,'SASD'),(242,'6320170',210058,24010,0,0,24005,'MD',2013,'SASD'),(243,'6320008',210060,24091,1,0,24033,'MD',2013,'SASD'),(244,'6320003',210061,24088,1,0,24047,'MD',2013,'SASD'),(245,'6320416',210062,24081,1,0,24033,'MD',2013,'SASD'),(246,'6320260',210063,24074,1,0,24005,'MD',2013,'SASD'),(247,NULL,210087,NULL,NULL,0,NULL,'MD',2013,'SASD'),(248,NULL,210088,NULL,NULL,0,NULL,'MD',2013,'SASD'),(249,NULL,210333,NULL,NULL,0,NULL,'MD',2013,'SASD'),(250,'6320180',210904,24085,1,0,24510,'MD',2013,'SASD'),(251,'6320190',212005,24098,0,0,24510,'MD',2013,'SASD'),(252,'6320004',213028,24090,0,0,24045,'MD',2013,'SASD'),(253,'6320580',210001,24065,1,0,24043,'MD',2014,'SASD'),(254,'6320330',210002,24078,1,0,24510,'MD',2014,'SASD'),(255,'6320610',210003,24005,1,0,24033,'MD',2014,'SASD'),(256,'6320813',210004,24080,1,0,24031,'MD',2014,'SASD'),(257,'6320540',210005,24050,1,0,24021,'MD',2014,'SASD'),(258,'6320590',210006,24051,1,0,24025,'MD',2014,'SASD'),(259,'6320220',210008,24077,1,0,24510,'MD',2014,'SASD'),(260,'6320180',210009,24085,1,0,24510,'MD',2014,'SASD'),(261,'6320370',210010,24048,1,0,24019,'MD',2014,'SASD'),(262,'6320250',210011,24008,1,0,24510,'MD',2014,'SASD'),(263,'6320280',210012,24082,1,0,24510,'MD',2014,'SASD'),(264,'6320090',210013,24016,1,0,24510,'MD',2014,'SASD'),(265,'6320120',210015,24073,1,0,24005,'MD',2014,'SASD'),(266,'6322080',210016,24042,1,0,24031,'MD',2014,'SASD'),(267,'6320685',210017,24087,1,0,24023,'MD',2014,'SASD'),(268,'6320690',210018,24057,1,0,24031,'MD',2014,'SASD'),(269,'6320785',210019,24041,1,0,24045,'MD',2014,'SASD'),(270,'6320350',210022,24084,1,0,24031,'MD',2014,'SASD'),(271,'6320020',210023,24045,1,0,24003,'MD',2014,'SASD'),(272,'6320310',210024,24068,1,0,24510,'MD',2014,'SASD'),(273,'6320206',210027,24100,1,0,24001,'MD',2014,'SASD'),(274,'6320670',210028,24075,1,0,24037,'MD',2014,'SASD'),(275,'6320050',210029,24018,1,0,24510,'MD',2014,'SASD'),(276,'6320420',210030,24034,1,0,24029,'MD',2014,'SASD'),(277,'6320500',210032,24047,1,0,24015,'MD',2014,'SASD'),(278,'6320880',210033,24019,1,0,24013,'MD',2014,'SASD'),(279,'6320290',210034,24063,1,0,24510,'MD',2014,'SASD'),(280,'6320640',210035,24062,1,0,24017,'MD',2014,'SASD'),(281,'6320480',210037,24036,1,0,24041,'MD',2014,'SASD'),(282,'6320200',210038,24043,1,0,24510,'MD',2014,'SASD'),(283,'6320730',210039,24033,1,0,24009,'MD',2014,'SASD'),(284,'6320735',210040,24038,1,0,24005,'MD',2014,'SASD'),(285,'6320569',210043,24011,1,0,24003,'MD',2014,'SASD'),(286,'6320125',210044,24035,1,0,24005,'MD',2014,'SASD'),(287,'6320440',210045,24067,1,0,24039,'MD',2014,'SASD'),(288,'6320460',210048,24055,1,0,24027,'MD',2014,'SASD'),(289,'6320515',210049,24009,1,0,24025,'MD',2014,'SASD'),(290,'6320645',210051,24064,1,0,24033,'MD',2014,'SASD'),(291,'6320655',210055,24049,1,0,24033,'MD',2014,'SASD'),(292,'6320127',210056,24007,1,0,24510,'MD',2014,'SASD'),(293,'6320775',210057,24086,1,0,24031,'MD',2014,'SASD'),(294,'6320170',210058,24010,0,0,24005,'MD',2014,'SASD'),(295,'6320008',210060,24091,1,0,24033,'MD',2014,'SASD'),(296,'6320003',210061,24088,1,0,24047,'MD',2014,'SASD'),(297,'6320416',210062,24081,1,0,24033,'MD',2014,'SASD'),(298,'6320260',210063,24074,1,0,24005,'MD',2014,'SASD'),(299,'6320190',210064,24098,0,0,24510,'MD',2014,'SASD'),(300,NULL,210065,NULL,NULL,0,NULL,'MD',2014,'SASD'),(301,NULL,210087,NULL,NULL,0,NULL,'MD',2014,'SASD'),(302,NULL,210088,NULL,NULL,0,NULL,'MD',2014,'SASD'),(303,NULL,210333,NULL,NULL,0,NULL,'MD',2014,'SASD'),(304,'6320180',210904,24085,1,0,24510,'MD',2014,'SASD'),(305,'6320004',213028,24090,0,0,24045,'MD',2014,'SASD'),(306,'6320013',213029,24094,0,0,24031,'MD',2014,'SASD');

####### Stage 2 :: Creating Master Patient Indexes

# ------------- explore patient-level ids
SELECT		visitlink, COUNT(visitlink) visit_ct, COUNT(DISTINCT dshospid) hosp_ct, COUNT(DISTINCT mrn_r) mrn_ct
FROM		2013_sid_core
GROUP BY	visitlink
HAVING		visit_ct > 10
ORDER BY	visit_ct DESC, hosp_ct DESC, mrn_ct DESC;

SELECT		*, visitlink, dshospid, mrn_r
FROM		2013_sid_core
WHERE		visitlink = '2051936'; -- 11, 1, 1

SELECT		*, visitlink, dshospid, mrn_r -- 11, 2, 1
FROM		2013_sid_core
WHERE		visitlink = '1953262';

SELECT		*, visitlink, dshospid, mrn_r -- 11, 1, 2
FROM		2013_sid_core
WHERE		visitlink = '2270466';

SELECT		*, visitlink, dshospid, mrn_r -- 11, 2, 2
FROM		2013_sid_core
WHERE		visitlink = '1839229';

SELECT		*, visitlink, dshospid, mrn_r -- 38, 5, 8
FROM		2013_sid_core
WHERE		visitlink = '1966712';


# visit link seems to be the closest variables to a unique patient identifier

SELECT		visitlink, COUNT(visitlink) visit_ct, COUNT(DISTINCT zip3) zip_ct
FROM		2013_sid_core
GROUP BY	visitlink
-- HAVING		visit_ct > 10
ORDER BY	zip_ct DESC;

SELECT		*, visitlink, dshospid, mrn_r, zip3 -- 4, 4
FROM		2013_sid_core
WHERE		visitlink = '2223085';


# ------------- table to hold all visit links (with duplicates)
CREATE TABLE `hcup_pat_visitlink_all` 
(
  `visitlink` varchar(16) DEFAULT NULL,
  `db` varchar(4) DEFAULT NULL,
  `yr` varchar(4) DEFAULT NULL,
  `ct` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

# ------------- find the number of vistlinks across data sources (3) and years (2013 & 2014)

# ------------- extract all visitlinks from all years/dbs

INSERT INTO	hcup_pat_visitlink_all (visitlink, db, yr, ct)
SELECT 		visitlink, 'SASD' AS db, '2013' AS yr, COUNT(*) AS ct
FROM 		hcup.2013_sasd_core
GROUP BY	visitlink;

INSERT INTO	hcup_pat_visitlink_all (visitlink, db, yr, ct)
SELECT 		visitlink, 'SEDD' AS db, '2013' AS yr, COUNT(*) AS ct
FROM 		hcup.2013_sedd_core
GROUP BY	visitlink;

INSERT INTO	hcup_pat_visitlink_all (visitlink, db, yr, ct)
SELECT 		visitlink, 'SID' AS db, '2013' AS yr, COUNT(*) AS ct
FROM 		hcup.2013_sid_core
GROUP BY	visitlink;

INSERT INTO	hcup_pat_visitlink_all (visitlink, db, yr, ct)
SELECT 		visitlink, 'SASD' AS db, '2014' AS yr, COUNT(*) AS ct
FROM 		hcup.2014_sasd_core
GROUP BY	visitlink;

INSERT INTO	hcup_pat_visitlink_all (visitlink, db, yr, ct)
SELECT 		visitlink, 'SEDD' AS db, '2014' AS yr, COUNT(*) AS ct
FROM 		hcup.2014_sedd_core
GROUP BY	visitlink;

INSERT INTO	hcup_pat_visitlink_all (visitlink, db, yr, ct)
SELECT 		visitlink, 'SID' AS db, '2014' AS yr, COUNT(*) AS ct
FROM 		hcup.2014_sid_core
GROUP BY	visitlink;

SELECT 		COUNT(visitlink)
FROM		hcup_pat_visitlink_all;
-- 5,699,252

SELECT 		COUNT(DISTINCT visitlink)
FROM		hcup_pat_visitlink_all;
-- 3,263,864

SELECT		db, yr, COUNT(DISTINCT visitlink) visitlink_ct
FROM		hcup_pat_visitlink_all
GROUP BY	db, yr;

/*
	# db, 		yr, 		visitlink_ct
	'SASD', 	'2013', 	'1,075,498'
	'SASD', 	'2014', 	'1,077,155'
	'SEDD', 	'2013', 	'1,304,604'
	'SEDD', 	'2014', 	'1,310,346'
	'SID', 		'2013', 	'471,524'
	'SID', 		'2014', 	'460,125'
*/


# ------------- table to hold all visit links (one row for each visitlink)
CREATE TABLE `hcup_pat_visitlink_uni` 
(
  `visitlink` varchar(16) DEFAULT NULL,
  `sasd_2013` varchar(1) DEFAULT NULL,
  `sasd_2013_ct` int(11) DEFAULT NULL,
  `sedd_2013` varchar(1) DEFAULT NULL,
  `sedd_2013_ct` int(11) DEFAULT NULL,
  `sid_2013` varchar(1) DEFAULT NULL,
  `sid_2013_ct` int(11) DEFAULT NULL,
  `sasd_2014` varchar(1) DEFAULT NULL,
  `sasd_2014_ct` int(11) DEFAULT NULL,
  `sedd_2014` varchar(1) DEFAULT NULL,
  `sedd_2014_ct` int(11) DEFAULT NULL,
  `sid_2014` varchar(1) DEFAULT NULL,
  `sid_2014_ct` int(11) DEFAULT NULL,
  UNIQUE KEY `visitlink_UNIQUE` (`visitlink`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

# ------------- insert all unique visitlinks into one overall table

INSERT INTO	hcup_pat_visitlink_uni(visitlink, sasd_2013, sasd_2013_ct, sasd_2014, sasd_2014_ct, sedd_2013, sedd_2013_ct, sedd_2014, sedd_2014_ct, sid_2013, sid_2013_ct, sid_2014, sid_2014_ct)
SELECT		visitlink,
			MAX(sasd_2013) sasd_2013, MAX(sasd_2013_ct) sasd_2013_ct,
            MAX(sasd_2014) sasd_2014, MAX(sasd_2014_ct) sasd_2014_ct,
            MAX(sedd_2013) sedd_2013, MAX(sedd_2013_ct) sedd_2013_ct,
			MAX(sedd_2014) sedd_2014, MAX(sedd_2014_ct) sedd_2014_ct,
			MAX(sid_2013) sid_2013, MAX(sid_2013_ct) sid_2013_ct,
			MAX(sid_2014) sid_2014, MAX(sid_2014_ct) sid_2014_ct
FROM
(
	SELECT 		visitlink, 
				IF(db = 'sasd' AND yr = '2013', 'Y', NULL) AS sasd_2013, IF(db = 'sasd' AND yr = '2013', SUM(ct), NULL) AS sasd_2013_ct,
				IF(db = 'sasd' AND yr = '2014', 'Y', NULL) AS sasd_2014, IF(db = 'sasd' AND yr = '2014', SUM(ct), NULL) AS sasd_2014_ct,
				IF(db = 'sedd' AND yr = '2013', 'Y', NULL) AS sedd_2013, IF(db = 'sedd' AND yr = '2013', SUM(ct), NULL) AS sedd_2013_ct,
				IF(db = 'sedd' AND yr = '2014', 'Y', NULL) AS sedd_2014, IF(db = 'sedd' AND yr = '2014', SUM(ct), NULL) AS sedd_2014_ct,
				IF(db = 'sid' AND yr = '2013', 'Y', NULL) AS sid_2013, IF(db = 'sid' AND yr = '2013', SUM(ct), NULL) AS sid_2013_ct,
				IF(db = 'sid' AND yr = '2014', 'Y', NULL) AS sid_2014, IF(db = 'sid' AND yr = '2014', SUM(ct), NULL) AS sid_2014_ct
	FROM 		hcup_pat_visitlink_all
	GROUP BY	visitlink, db, yr
) temp
GROUP BY visitlink;

SELECT 		COUNT(visitlink)
FROM		hcup_pat_visitlink_uni;
-- 3,263,864

SELECT 		COUNT(DISTINCT visitlink)
FROM		hcup_pat_visitlink_uni;
-- 3,263,864

# pat/visit stat for each of the DBs and YRs

CREATE TABLE hcup_pat_visitlink_uni_stat
SELECT	sasd_2013_pat, sasd_2013_vis, ROUND(sasd_2013_vis/sasd_2013_pat, 2) sasd_2013_vis_avg,
		sedd_2013_pat, sedd_2013_vis, ROUND(sedd_2013_vis/sedd_2013_pat, 2) sedd_2013_vis_avg,
        sid_2013_pat, sid_2013_vis, ROUND(sid_2013_vis/sid_2013_pat, 2) sid_2013_vis_avg,
        sasd_2014_pat, sasd_2014_vis, ROUND(sasd_2014_vis/sasd_2014_pat, 2) sasd_2014_vis_avg,
		sedd_2014_pat, sedd_2014_vis, ROUND(sedd_2014_vis/sedd_2014_pat, 2) sedd_2014_vis_avg,
        sid_2014_pat, sid_2014_vis, ROUND(sid_2014_vis/sid_2014_pat, 2) sid_2014_vis_avg
FROM
(
SELECT 	SUM(IF(sasd_2013 IS NOT NULL, 1, 0)) sasd_2013_pat,
		SUM(IF(sasd_2013_ct IS NOT NULL, sasd_2013_ct, 0)) sasd_2013_vis,
        SUM(IF(sedd_2013 IS NOT NULL, 1, 0)) sedd_2013_pat,
		SUM(IF(sedd_2013_ct IS NOT NULL, sedd_2013_ct, 0)) sedd_2013_vis,
        SUM(IF(sid_2013 IS NOT NULL, 1, 0)) sid_2013_pat,
		SUM(IF(sid_2013_ct IS NOT NULL, sid_2013_ct, 0)) sid_2013_vis,
        SUM(IF(sasd_2014 IS NOT NULL, 1, 0)) sasd_2014_pat,
		SUM(IF(sasd_2014_ct IS NOT NULL, sasd_2014_ct, 0)) sasd_2014_vis,
        SUM(IF(sedd_2014 IS NOT NULL, 1, 0)) sedd_2014_pat,
		SUM(IF(sedd_2014_ct IS NOT NULL, sedd_2014_ct, 0)) sedd_2014_vis,
        SUM(IF(sid_2014 IS NOT NULL, 1, 0)) sid_2014_pat,
		SUM(IF(sid_2014_ct IS NOT NULL, sid_2014_ct, 0)) sid_2014_vis
FROM 	hcup_pat_visitlink_uni
WHERE	visitlink IS NOT NULL
) temp;

####### Stage 3 :: Creating the Master Patient Table (first pieces, and then all in one)

# ------------- table to hold pat age
CREATE TABLE `hcup_pat_age` 
(
  `visitlink` varchar(16) DEFAULT NULL,
  
  `age_min_sasd_2013` int(3) DEFAULT NULL,
  `age_max_sasd_2013` int(3) DEFAULT NULL,
  `age_avg_sasd_2013` decimal(5,2) DEFAULT NULL,
  `age_std_sasd_2013` decimal(5,2) DEFAULT NULL,
  
  `age_min_sasd_2014` int(3) DEFAULT NULL,
  `age_max_sasd_2014` int(3) DEFAULT NULL,
  `age_avg_sasd_2014` decimal(5,2) DEFAULT NULL,
  `age_std_sasd_2014` decimal(5,2) DEFAULT NULL,
  
  `age_min_sedd_2013` int(3) DEFAULT NULL,
  `age_max_sedd_2013` int(3) DEFAULT NULL,
  `age_avg_sedd_2013` decimal(5,2) DEFAULT NULL,
  `age_std_sedd_2013` decimal(5,2) DEFAULT NULL,
  
  `age_min_sedd_2014` int(3) DEFAULT NULL,
  `age_max_sedd_2014` int(3) DEFAULT NULL,
  `age_avg_sedd_2014` decimal(5,2) DEFAULT NULL,
  `age_std_sedd_2014` decimal(5,2) DEFAULT NULL,
  
  `age_min_sid_2013` int(3) DEFAULT NULL,
  `age_max_sid_2013` int(3) DEFAULT NULL,
  `age_avg_sid_2013` decimal(5,2) DEFAULT NULL,
  `age_std_sid_2013` decimal(5,2) DEFAULT NULL,
  
  `age_min_sid_2014` int(3) DEFAULT NULL,
  `age_max_sid_2014` int(3) DEFAULT NULL,
  `age_avg_sid_2014` decimal(5,2) DEFAULT NULL,
  `age_std_sid_2014` decimal(5,2) DEFAULT NULL,
  
  `age_min_2013` int(3) DEFAULT NULL,
  `age_max_2013` int(3) DEFAULT NULL,
  `age_avg_2013` decimal(5,2) DEFAULT NULL,
  `age_std_2013` decimal(5,2) DEFAULT NULL,
  
  `age_min_2014` int(3) DEFAULT NULL,
  `age_max_2014` int(3) DEFAULT NULL,
  `age_avg_2014` decimal(5,2) DEFAULT NULL,
  `age_std_2014` decimal(5,2) DEFAULT NULL,
  
  `age_min` int(3) DEFAULT NULL,
  `age_max` int(3) DEFAULT NULL,
  `age_avg` decimal(5,2) DEFAULT NULL,
  `age_std` decimal(5,2) DEFAULT NULL,
  
  `age_flag` int(1) DEFAULT NULL,
  
  UNIQUE KEY `visitlink_UNIQUE` (`visitlink`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT 	INTO hcup_pat_age (visitlink)
SELECT 	visitlink 
FROM 	hcup_pat_visitlink_uni; -- 314 sec

UPDATE 		hcup_pat_age
INNER JOIN	(SELECT visitlink, MIN(age) age_min, MAX(age) age_max, ROUND(AVG(age), 2) age_avg, ROUND(STD(age), 2) age_std FROM 2013_sasd_core GROUP BY visitlink) temp
ON			temp.visitlink = hcup_pat_age.visitlink
SET			age_min_sasd_2013 = temp.age_min, 
			age_max_sasd_2013 = temp.age_max, 
			age_avg_sasd_2013 = temp.age_avg, 
            age_std_sasd_2013 = temp.age_std; -- 156 sec

UPDATE 		hcup_pat_age
INNER JOIN	(SELECT visitlink, MIN(age) age_min, MAX(age) age_max, ROUND(AVG(age), 2) age_avg, ROUND(STD(age), 2) age_std FROM 2014_sasd_core GROUP BY visitlink) temp
ON			temp.visitlink = hcup_pat_age.visitlink
SET			age_min_sasd_2014 = temp.age_min, 
			age_max_sasd_2014 = temp.age_max, 
			age_avg_sasd_2014 = temp.age_avg, 
            age_std_sasd_2014 = temp.age_std; -- 161 sec
            
UPDATE 		hcup_pat_age
INNER JOIN	(SELECT visitlink, MIN(age) age_min, MAX(age) age_max, ROUND(AVG(age), 2) age_avg, ROUND(STD(age), 2) age_std FROM 2013_sedd_core GROUP BY visitlink) temp
ON			temp.visitlink = hcup_pat_age.visitlink
SET			age_min_sedd_2013 = temp.age_min, 
			age_max_sedd_2013 = temp.age_max, 
			age_avg_sedd_2013 = temp.age_avg, 
            age_std_sedd_2013 = temp.age_std; -- 165 sec
            
UPDATE 		hcup_pat_age
INNER JOIN	(SELECT visitlink, MIN(age) age_min, MAX(age) age_max, ROUND(AVG(age), 2) age_avg, ROUND(STD(age), 2) age_std FROM 2014_sedd_core GROUP BY visitlink) temp
ON			temp.visitlink = hcup_pat_age.visitlink
SET			age_min_sedd_2014 = temp.age_min, 
			age_max_sedd_2014 = temp.age_max, 
			age_avg_sedd_2014 = temp.age_avg, 
            age_std_sedd_2014 = temp.age_std; -- 174 sec
            
UPDATE 		hcup_pat_age
INNER JOIN	(SELECT visitlink, MIN(age) age_min, MAX(age) age_max, ROUND(AVG(age), 2) age_avg, ROUND(STD(age), 2) age_std FROM 2013_sid_core GROUP BY visitlink) temp
ON			temp.visitlink = hcup_pat_age.visitlink
SET			age_min_sid_2013 = temp.age_min, 
			age_max_sid_2013 = temp.age_max, 
			age_avg_sid_2013 = temp.age_avg, 
            age_std_sid_2013 = temp.age_std; -- 55 sec
            
UPDATE 		hcup_pat_age
INNER JOIN	(SELECT visitlink, MIN(age) age_min, MAX(age) age_max, ROUND(AVG(age), 2) age_avg, ROUND(STD(age), 2) age_std FROM 2014_sid_core GROUP BY visitlink) temp
ON			temp.visitlink = hcup_pat_age.visitlink
SET			age_min_sid_2014 = temp.age_min, 
			age_max_sid_2014 = temp.age_max, 
			age_avg_sid_2014 = temp.age_avg, 
            age_std_sid_2014 = temp.age_std; -- 58

# handling the visitlink == NULL
UPDATE 		hcup_pat_age
SET			age_min_sasd_2013 = (SELECT MIN(age) age_min FROM 2013_sasd_core WHERE visitlink IS NULL GROUP BY visitlink),
			age_max_sasd_2013 = (SELECT MAX(age) age_max FROM 2013_sasd_core WHERE visitlink IS NULL GROUP BY visitlink),
            age_avg_sasd_2013 = (SELECT ROUND(AVG(age), 2) age_avg FROM 2013_sasd_core WHERE visitlink IS NULL GROUP BY visitlink),
            age_std_sasd_2013 = (SELECT ROUND(STD(age), 2) age_std FROM 2013_sasd_core WHERE visitlink IS NULL GROUP BY visitlink),
            age_min_sasd_2014 = (SELECT MIN(age) age_min FROM 2014_sasd_core WHERE visitlink IS NULL GROUP BY visitlink),
			age_max_sasd_2014 = (SELECT MAX(age) age_max FROM 2014_sasd_core WHERE visitlink IS NULL GROUP BY visitlink),
            age_avg_sasd_2014 = (SELECT ROUND(AVG(age), 2) age_avg FROM 2014_sasd_core WHERE visitlink IS NULL GROUP BY visitlink),
            age_std_sasd_2014 = (SELECT ROUND(STD(age), 2) age_std FROM 2014_sasd_core WHERE visitlink IS NULL GROUP BY visitlink),
            age_min_sedd_2013 = (SELECT MIN(age) age_min FROM 2013_sedd_core WHERE visitlink IS NULL GROUP BY visitlink),
			age_max_sedd_2013 = (SELECT MAX(age) age_max FROM 2013_sedd_core WHERE visitlink IS NULL GROUP BY visitlink),
            age_avg_sedd_2013 = (SELECT ROUND(AVG(age), 2) age_avg FROM 2013_sedd_core WHERE visitlink IS NULL GROUP BY visitlink),
            age_std_sedd_2013 = (SELECT ROUND(STD(age), 2) age_std FROM 2013_sedd_core WHERE visitlink IS NULL GROUP BY visitlink),
            age_min_sedd_2014 = (SELECT MIN(age) age_min FROM 2014_sedd_core WHERE visitlink IS NULL GROUP BY visitlink),
			age_max_sedd_2014 = (SELECT MAX(age) age_max FROM 2014_sedd_core WHERE visitlink IS NULL GROUP BY visitlink),
            age_avg_sedd_2014 = (SELECT ROUND(AVG(age), 2) age_avg FROM 2014_sedd_core WHERE visitlink IS NULL GROUP BY visitlink),
            age_std_sedd_2014 = (SELECT ROUND(STD(age), 2) age_std FROM 2014_sedd_core WHERE visitlink IS NULL GROUP BY visitlink),
            age_min_sid_2013 = (SELECT MIN(age) age_min FROM 2013_sid_core WHERE visitlink IS NULL GROUP BY visitlink),
			age_max_sid_2013 = (SELECT MAX(age) age_max FROM 2013_sid_core WHERE visitlink IS NULL GROUP BY visitlink),
            age_avg_sid_2013 = (SELECT ROUND(AVG(age), 2) age_avg FROM 2013_sid_core WHERE visitlink IS NULL GROUP BY visitlink),
            age_std_sid_2013 = (SELECT ROUND(STD(age), 2) age_std FROM 2013_sid_core WHERE visitlink IS NULL GROUP BY visitlink),
            age_min_sid_2014 = (SELECT MIN(age) age_min FROM 2014_sid_core WHERE visitlink IS NULL GROUP BY visitlink),
			age_max_sid_2014 = (SELECT MAX(age) age_max FROM 2014_sid_core WHERE visitlink IS NULL GROUP BY visitlink),
            age_avg_sid_2014 = (SELECT ROUND(AVG(age), 2) age_avg FROM 2014_sid_core WHERE visitlink IS NULL GROUP BY visitlink),
            age_std_sid_2014 = (SELECT ROUND(STD(age), 2) age_std FROM 2014_sid_core WHERE visitlink IS NULL GROUP BY visitlink)
WHERE		visitlink IS NULL; -- 273 sec

CREATE TABLE hcup_pat_age_backup SELECT * FROM hcup_pat_age;
            
UPDATE 		hcup_pat_age
SET			age_min_2013 = IF(age_min_sasd_2013 IS NULL AND age_min_sedd_2013 IS NULL AND age_min_sid_2013 IS NULL, NULL, LEAST(IFNULL(age_min_sasd_2013, 99999), IFNULL(age_min_sedd_2013, 99999), IFNULL(age_min_sid_2013, 99999))),
			age_max_2013 = IF(age_max_sasd_2013 IS NULL AND age_max_sedd_2013 IS NULL AND age_max_sid_2013 IS NULL, NULL, GREATEST(IFNULL(age_max_sasd_2013, -99999), IFNULL(age_max_sedd_2013, -99999), IFNULL(age_max_sid_2013, -99999))),
			age_avg_2013 = IF(age_avg_sasd_2013 IS NULL AND age_avg_sedd_2013 IS NULL AND age_avg_sid_2013 IS NULL, NULL, ROUND( (IFNULL(age_avg_sid_2013,0) + IFNULL(age_avg_sedd_2013,0) + IFNULL(age_avg_sasd_2013,0)) / (IF(age_avg_sid_2013 IS NULL,0, 1) + IF(age_avg_sedd_2013 IS NULL,0,1) + IF(age_avg_sasd_2013 IS NULL,0,1)), 2)),
			age_std_2013 = IF(age_std_sasd_2013 IS NULL AND age_std_sedd_2013 IS NULL AND age_std_sid_2013 IS NULL, NULL, ROUND( (IFNULL(age_std_sid_2013,0) + IFNULL(age_std_sedd_2013,0) + IFNULL(age_std_sasd_2013,0)) / (IF(age_std_sid_2013 IS NULL,0, 1) + IF(age_std_sedd_2013 IS NULL,0,1) + IF(age_std_sasd_2013 IS NULL,0,1)), 2)),
			age_min_2014 = IF(age_min_sasd_2014 IS NULL AND age_min_sedd_2014 IS NULL AND age_min_sid_2014 IS NULL, NULL, LEAST(IFNULL(age_min_sasd_2014, 99999), IFNULL(age_min_sedd_2014, 99999), IFNULL(age_min_sid_2014, 99999))),
			age_max_2014 = IF(age_max_sasd_2014 IS NULL AND age_max_sedd_2014 IS NULL AND age_max_sid_2014 IS NULL, NULL, GREATEST(IFNULL(age_max_sasd_2014, -99999), IFNULL(age_max_sedd_2014, -99999), IFNULL(age_max_sid_2014, -99999))),
			age_avg_2014 = IF(age_avg_sasd_2014 IS NULL AND age_avg_sedd_2014 IS NULL AND age_avg_sid_2014 IS NULL, NULL, ROUND( (IFNULL(age_avg_sid_2014,0) + IFNULL(age_avg_sedd_2014,0) + IFNULL(age_avg_sasd_2014,0)) / (IF(age_avg_sid_2014 IS NULL,0, 1) + IF(age_avg_sedd_2014 IS NULL,0,1) + IF(age_avg_sasd_2014 IS NULL,0,1)), 2)),
			age_std_2014 = IF(age_std_sasd_2014 IS NULL AND age_std_sedd_2014 IS NULL AND age_std_sid_2014 IS NULL, NULL, ROUND( (IFNULL(age_std_sid_2014,0) + IFNULL(age_std_sedd_2014,0) + IFNULL(age_std_sasd_2014,0)) / (IF(age_std_sid_2014 IS NULL,0, 1) + IF(age_std_sedd_2014 IS NULL,0,1) + IF(age_std_sasd_2014 IS NULL,0,1)), 2)),
			age_min = IF(age_min_2013 IS NULL AND age_min_2014 IS NULL, NULL, LEAST(IFNULL(age_min_2013, 99999), IFNULL(age_min_2014, 99999))),
			age_max = IF(age_max_2013 IS NULL AND age_max_2014 IS NULL, NULL, GREATEST(IFNULL(age_max_2013, -99999), IFNULL(age_max_2014, -99999))),
			age_avg = IF(age_avg_2013 IS NULL AND age_avg_2014 IS NULL, NULL, ROUND((IFNULL(age_avg_2013, 0) + IFNULL(age_avg_2014, 0))/ (IF(age_avg_2013 IS NULL, 0, 1) + IF(age_avg_2014 IS NULL, 0, 1)), 2)),
			age_std = IF(age_std_2013 IS NULL AND age_std_2014 IS NULL, NULL, ROUND((IFNULL(age_std_2013, 0) + IFNULL(age_std_2014, 0))/ (IF(age_std_2013 IS NULL, 0, 1) + IF(age_std_2014 IS NULL, 0, 1)), 2)); -- 90 sec
            
UPDATE    hcup_pat_age
SET     age_flag = IF((age_avg IS NULL) OR (age_avg < 0) OR (age_avg > 120) OR (age_max_2013 - age_min_2013 >= 2) OR (age_max_2014 - age_min_2014 >= 2) OR (age_max - age_min >= 3) OR (age_std > .5), 1, 0); -- 18 sec; 

SELECT 		COUNT(*) 
FROM 		hcup_pat_age 
WHERE 		age_flag = 1; -- 352 rows/patients

SELECT		COUNT(*) pat_ct, IF(age_avg < 1, '0-1', IF(age_avg < 10, '1-10', IF(age_avg < 20, '10-20', IF(age_avg < 30, '20-30', IF(age_avg < 40, '30-40', IF(age_avg < 50, '40-50', IF(age_avg < 60, '50-60', IF(age_avg < 70, '60-70', IF(age_avg < 80, '70-80', IF(age_avg < 90, '80-90', '90+')))))))))) age_range
FROM		hcup_pat_age
WHERE		age_flag = 0
GROUP BY	age_range; -- 12 sec

/*

	+ ------------- + -------------- +
	| pat_ct        | age_range      |
	+ ------------- + -------------- +
	| 162,851       | 0-1            |
	| 293,192       | 1-10           |
	| 304,207       | 10-20          |
	| 446,413       | 20-30          |
	| 410,937       | 30-40          |
	| 412,026       | 40-50          |
	| 460,538       | 50-60          |
	| 368,648       | 60-70          |
	| 235,262       | 70-80          |
	| 134,75        | 80-90          |
	| 34,610        | 90+            |
	+ ------------- + -------------- +

*/

CREATE TABLE	hcup_pat_age_stat
SELECT			COUNT(*) pat_ct, IF(age_avg < 1, '0-1', IF(age_avg < 10, '1-10', IF(age_avg < 20, '10-20', IF(age_avg < 30, '20-30', IF(age_avg < 40, '30-40', IF(age_avg < 50, '40-50', IF(age_avg < 60, '50-60', IF(age_avg < 70, '60-70', IF(age_avg < 80, '70-80', IF(age_avg < 90, '80-90', '90+')))))))))) age_range
FROM			hcup_pat_age
WHERE			age_flag = 0
GROUP BY		age_range;


# ------------- table to hold pat dead information
CREATE TABLE `hcup_pat_died` 
(
  `visitlink` varchar(16) DEFAULT NULL,
  
  `died_sasd_2013` int(1) DEFAULT NULL,
  `died_sasd_2014` int(1) DEFAULT NULL,
  `died_sedd_2013` int(1) DEFAULT NULL,
  `died_sedd_2014` int(1) DEFAULT NULL,
  `died_sid_2013` int(1) DEFAULT NULL,
  `died_sid_2014` int(1) DEFAULT NULL,
  
  `died_2013` int(1) DEFAULT NULL,
  `died_2014` int(1) DEFAULT NULL,
  
  `died_flag` int(1) DEFAULT NULL,
  
  UNIQUE KEY `visitlink_UNIQUE` (`visitlink`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT 		INTO hcup_pat_died (visitlink)
SELECT 		visitlink 
FROM 		hcup_pat_visitlink_uni; -- 314 sec

UPDATE 		hcup_pat_died
INNER JOIN	(SELECT visitlink, MAX(died) died FROM 2013_sasd_core GROUP BY visitlink) temp
ON			temp.visitlink = hcup_pat_died.visitlink
SET			died_sasd_2013 = temp.died; -- 114 sec

UPDATE 		hcup_pat_died
INNER JOIN	(SELECT visitlink, MAX(died) died FROM 2014_sasd_core GROUP BY visitlink) temp
ON			temp.visitlink = hcup_pat_died.visitlink
SET			died_sasd_2014 = temp.died; -- 116 sec

UPDATE 		hcup_pat_died
INNER JOIN	(SELECT visitlink, MAX(died) died FROM 2013_sedd_core GROUP BY visitlink) temp
ON			temp.visitlink = hcup_pat_died.visitlink
SET			died_sedd_2013 = temp.died; -- 131 sec

UPDATE 		hcup_pat_died
INNER JOIN	(SELECT visitlink, MAX(died) died FROM 2014_sedd_core GROUP BY visitlink) temp
ON			temp.visitlink = hcup_pat_died.visitlink
SET			died_sedd_2014 = temp.died; -- 137 sec

UPDATE 		hcup_pat_died
INNER JOIN	(SELECT visitlink, MAX(died) died FROM 2013_sid_core GROUP BY visitlink) temp
ON			temp.visitlink = hcup_pat_died.visitlink
SET			died_sid_2013 = temp.died; -- 43 sec

UPDATE 		hcup_pat_died
INNER JOIN	(SELECT visitlink, MAX(died) died FROM 2014_sid_core GROUP BY visitlink) temp
ON			temp.visitlink = hcup_pat_died.visitlink
SET			died_sid_2014 = temp.died; -- 46 sec

UPDATE		hcup_pat_died
SET			died_2013 = IF(died_sasd_2013 IS NULL AND died_sedd_2013 IS NULL AND died_sid_2013 IS NULL, 0, GREATEST(IFNULL(died_sasd_2013, -99999), IFNULL(died_sedd_2013, -99999), IFNULL(died_sid_2013, -99999))),
			died_2014 = IF(died_sasd_2014 IS NULL AND died_sedd_2014 IS NULL AND died_sid_2014 IS NULL, 0, GREATEST(IFNULL(died_sasd_2014, -99999), IFNULL(died_sedd_2014, -99999), IFNULL(died_sid_2014, -99999))); -- 16 sec
            
UPDATE		hcup_pat_died
SET			died_flag = IF(died_2013 = 1 OR died_2014 = 1, 1, 0); -- 16

SELECT 		COUNT(*) 
FROM 		hcup_pat_died 
WHERE 		died_flag = 1; -- 33,642 rows/patients

ALTER TABLE hcup_pat_died
ADD COLUMN post_death_flag int(1) AFTER died_flag;

UPDATE hcup_pat_died
SET post_death_flag = IF(died_2013 = 1 AND (died_sasd_2014 IS NOT NULL OR died_sid_2014 IS NOT NULL
  OR died_sedd_2014 IS NOT NULL), 1, 0);

#patients where they were dead in 2013 but alive in 2014
SELECT COUNT(*) FROM hcup_pat_died WHERE died_2013 = 1 AND (died_sasd_2014 IS NOT NULL 
OR died_sid_2014 IS NOT NULL OR died_sedd_2014 IS NOT NULL); -- 37 patients

# age range of patients who died
CREATE TABLE	hcup_pat_died_stat
SELECT		temp.age_range age_range, temp.died_ct died_ct, hcup_pat_age_stat.pat_ct pat_ct, ROUND((temp.died_ct/hcup_pat_age_stat.pat_ct)*100, 2) died_per
FROM
(
	SELECT		COUNT(*) died_ct, IF(age_avg < 1, '0-1', IF(age_avg < 10, '1-10', IF(age_avg < 20, '10-20', IF(age_avg < 30, '20-30', IF(age_avg < 40, '30-40', IF(age_avg < 50, '40-50', IF(age_avg < 60, '50-60', IF(age_avg < 70, '60-70', IF(age_avg < 80, '70-80', IF(age_avg < 90, '80-90', '90+')))))))))) age_range
	FROM		hcup_pat_age, hcup_pat_died
	WHERE		hcup_pat_age.visitlink = hcup_pat_died.visitlink
	AND			age_flag = 0 
	AND 		died_flag = 1
	GROUP BY	age_range
) temp,
hcup_pat_age_stat
WHERE	hcup_pat_age_stat.age_range = temp.age_range;


# ------------- table to hold pat sex information
CREATE TABLE `hcup_pat_sex` 
(
  `visitlink` varchar(16) DEFAULT NULL,
  
  `sex_sasd_2013` int(1) DEFAULT NULL,
  `sex_sasd_2014` int(1) DEFAULT NULL,
  `sex_sedd_2013` int(1) DEFAULT NULL,
  `sex_sedd_2014` int(1) DEFAULT NULL,
  `sex_sid_2013` int(1) DEFAULT NULL,
  `sex_sid_2014` int(1) DEFAULT NULL,
  
  `sex_2013` int(1) DEFAULT NULL,
  `sex_2014` int(1) DEFAULT NULL,
  
  `sex_2013_flag` int(1) DEFAULT NULL,
  `sex_2014_flag` int(1) DEFAULT NULL,
  
  `sex` int(1) DEFAULT NULL,
  `sex_flag` int(1) DEFAULT NULL,
  
  UNIQUE KEY `visitlink_UNIQUE` (`visitlink`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT 		INTO hcup_pat_sex (visitlink)
SELECT 		visitlink 
FROM 		hcup_pat_visitlink_uni; -- 314 sec

UPDATE 		hcup_pat_sex
INNER JOIN	(SELECT visitlink, MAX(female) sex FROM 2013_sasd_core GROUP BY visitlink) temp # MAX used (don't check for issues within a year)
ON			temp.visitlink = hcup_pat_sex.visitlink
SET			sex_sasd_2013 = temp.sex; -- 116 sec

UPDATE 		hcup_pat_sex
INNER JOIN	(SELECT visitlink, MAX(female) sex FROM 2014_sasd_core GROUP BY visitlink) temp # MAX used (don't check for issues within a year)
ON			temp.visitlink = hcup_pat_sex.visitlink
SET			sex_sasd_2014 = temp.sex; -- 117 sec

UPDATE 		hcup_pat_sex
INNER JOIN	(SELECT visitlink, MAX(female) sex FROM 2013_sedd_core GROUP BY visitlink) temp # MAX used (don't check for issues within a year)
ON			temp.visitlink = hcup_pat_sex.visitlink
SET			sex_sedd_2013 = temp.sex; -- 134 sec

UPDATE 		hcup_pat_sex
INNER JOIN	(SELECT visitlink, MAX(female) sex FROM 2014_sedd_core GROUP BY visitlink) temp # MAX used (don't check for issues within a year)
ON			temp.visitlink = hcup_pat_sex.visitlink
SET			sex_sedd_2014 = temp.sex; -- 142 sec

UPDATE 		hcup_pat_sex
INNER JOIN	(SELECT visitlink, MAX(female) sex FROM 2013_sid_core GROUP BY visitlink) temp # MAX used (don't check for issues within a year)
ON			temp.visitlink = hcup_pat_sex.visitlink
SET			sex_sid_2013 = temp.sex; -- 45 sec

UPDATE 		hcup_pat_sex
INNER JOIN	(SELECT visitlink, MAX(female) sex FROM 2014_sid_core GROUP BY visitlink) temp # MAX used (don't check for issues within a year)
ON			temp.visitlink = hcup_pat_sex.visitlink
SET			sex_sid_2014 = temp.sex; -- 48 sec

UPDATE		hcup_pat_sex
SET			sex_2013_flag = IF(	(sex_sasd_2013 IS NOT NULL AND sex_sedd_2013 IS NOT NULL AND sex_sasd_2013 <> sex_sedd_2013) OR 
                                (sex_sasd_2013 IS NOT NULL AND sex_sid_2013 IS NOT NULL AND sex_sasd_2013 <> sex_sid_2013) OR
                                (sex_sedd_2013 IS NOT NULL AND sex_sid_2013 IS NOT NULL AND sex_sedd_2013 <> sex_sid_2013), 1, 0),
            sex_2014_flag = IF(	(sex_sasd_2014 IS NOT NULL AND sex_sedd_2014 IS NOT NULL AND sex_sasd_2014 <> sex_sedd_2014) OR 
                                (sex_sasd_2014 IS NOT NULL AND sex_sid_2014 IS NOT NULL AND sex_sasd_2014 <> sex_sid_2014) OR
                                (sex_sedd_2014 IS NOT NULL AND sex_sid_2014 IS NOT NULL AND sex_sedd_2014 <> sex_sid_2014), 1, 0); -- 23 sec                    
                                
UPDATE		hcup_pat_sex
SET			sex_2013 = IF(sex_sasd_2013 IS NULL AND sex_sedd_2013 IS NULL AND sex_sid_2013 IS NULL, NULL, GREATEST(IFNULL(sex_sasd_2013, -99999), IFNULL(sex_sedd_2013, -99999), IFNULL(sex_sid_2013, -99999))),
			sex_2014 = IF(sex_sasd_2014 IS NULL AND sex_sedd_2014 IS NULL AND sex_sid_2014 IS NULL, NULL, GREATEST(IFNULL(sex_sasd_2014, -99999), IFNULL(sex_sedd_2014, -99999), IFNULL(sex_sid_2014, -99999))); -- 30 sec
            
UPDATE		hcup_pat_sex
SET			sex = IF(sex_2013 IS NULL AND sex_2014 IS NULL, NULL, GREATEST(IFNULL(sex_2013, -99999), IFNULL(sex_2014, -99999))); -- 37 sec

UPDATE		hcup_pat_sex
SET			sex_flag = IF(sex_2013_flag = 1 OR sex_2014_flag = 1 OR (sex_2013 IS NOT NULL AND sex_2014 IS NOT NULL AND sex_2013 <> sex_2014) OR sex IS NULL, 1, 0); -- 34

SELECT 		COUNT(*) 
FROM 		hcup_pat_sex 
WHERE 		sex_flag = 1; -- 1 rows/patients

# age range of patients split by sex
CREATE TABLE	hcup_pat_sex_stat
SELECT		temp.age_range age_range, temp.sex sex, temp.sex_ct sex_ct, hcup_pat_age_stat.pat_ct pat_ct, ROUND((temp.sex_ct/hcup_pat_age_stat.pat_ct)*100, 2) sex_per
FROM
(
	SELECT		COUNT(*) sex_ct, sex, IF(age_avg < 1, '0-1', IF(age_avg < 10, '1-10', IF(age_avg < 20, '10-20', IF(age_avg < 30, '20-30', IF(age_avg < 40, '30-40', IF(age_avg < 50, '40-50', IF(age_avg < 60, '50-60', IF(age_avg < 70, '60-70', IF(age_avg < 80, '70-80', IF(age_avg < 90, '80-90', '90+')))))))))) age_range
	FROM		hcup_pat_age, hcup_pat_sex
	WHERE		hcup_pat_age.visitlink = hcup_pat_sex.visitlink
	AND			age_flag = 0 
	AND 		sex_flag = 0
	GROUP BY	age_range, sex
) temp,
hcup_pat_age_stat
WHERE	hcup_pat_age_stat.age_range = temp.age_range; -- 278 sec


# ------------- table to hold pat marital status information
CREATE TABLE `hcup_pat_marital` 
(
  `visitlink` varchar(16) DEFAULT NULL,
  
  `marital_2013_N_ct` int(4) DEFAULT NULL,
  `marital_2013_D_ct` int(4) DEFAULT NULL,
  `marital_2013_I_ct` int(4) DEFAULT NULL,
  `marital_2013_M_ct` int(4) DEFAULT NULL,
  `marital_2013_W_ct` int(4) DEFAULT NULL,
  `marital_2013_X_ct` int(4) DEFAULT NULL,
  `marital_2013_ct` int(4) DEFAULT NULL,

  `marital_2014_N_ct` int(4) DEFAULT NULL,
  `marital_2014_D_ct` int(4) DEFAULT NULL,
  `marital_2014_I_ct` int(4) DEFAULT NULL,
  `marital_2014_M_ct` int(4) DEFAULT NULL,
  `marital_2014_W_ct` int(4) DEFAULT NULL,
  `marital_2014_X_ct` int(4) DEFAULT NULL,
  `marital_2014_ct` int(4) DEFAULT NULL,

  `marital_2013_freq` varchar(1) DEFAULT NULL,
  `marital_2014_freq` varchar(1) DEFAULT NULL,
  `marital_freq` varchar(1) DEFAULT NULL,
  
  `marital_2013_lastday` int(6) DEFAULT NULL,
  `marital_2013_last` varchar(1) DEFAULT NULL,
  `marital_2014_lastday` int(6) DEFAULT NULL,
  `marital_2014_last` varchar(1) DEFAULT NULL,
  `marital_last` varchar(1) DEFAULT NULL,
  
  `marital_2013_flag` int(1) DEFAULT NULL,
  `marital_2014_flag` int(1) DEFAULT NULL,
  `marital_age_flag` int(1) DEFAULT NULL,
  `marital_flag` int(1) DEFAULT NULL,
  
  UNIQUE KEY `visitlink_UNIQUE` (`visitlink`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*
	I		Single
	M		Married
	A		Common Law
	B		Registered Domestic Partner
	S		Separated
	X		Legally Separated
	D		Divorced
	W		Widowed
	U		Unmarried (single or divorced or widowed)
	Blank	Unknown, not applicable, missing, or invalid
*/

INSERT 		INTO hcup_pat_marital (visitlink)
SELECT 		visitlink 
FROM 		hcup_pat_visitlink_uni; -- 306 sec

UPDATE 		hcup_pat_marital
INNER JOIN	(
	SELECT		visitlink, 
				MAX(marital_N_ct) marital_N_ct, 
				MAX(marital_D_ct) marital_D_ct,
				MAX(marital_I_ct) marital_I_ct,
				MAX(marital_M_ct) marital_M_ct,
				MAX(marital_W_ct) marital_W_ct,
				MAX(marital_X_ct) marital_X_ct,
                SUM(marital_ct) marital_ct
	FROM
	(
		SELECT 		visitlink, 
					IF(maritalstatusub04 IS NULL OR maritalstatusub04 = '', COUNT(`key`), 0) marital_N_ct, 
					IF(maritalstatusub04 = 'D', COUNT(`key`), 0) marital_D_ct, 
					IF(maritalstatusub04 = 'I', COUNT(`key`), 0) marital_I_ct, 
					IF(maritalstatusub04 = 'M', COUNT(`key`), 0) marital_M_ct, 
					IF(maritalstatusub04 = 'W', COUNT(`key`), 0) marital_W_ct, 
					IF(maritalstatusub04 = 'X', COUNT(`key`), 0) marital_X_ct,
                    COUNT(`key`) marital_ct
		FROM 		2013_sid_core 
		GROUP BY 	visitlink, maritalstatusub04
	) temp1
	GROUP BY visitlink
) temp2
ON			(hcup_pat_marital.visitlink = temp2.visitlink)
			OR 
            (hcup_pat_marital.visitlink IS NULL AND temp2.visitlink IS NULL) -- in order to include the NULL visitlink
SET			marital_2013_N_ct = IF(marital_2013_N_ct IS NULL, 0, marital_2013_N_ct) + temp2.marital_N_ct,
			marital_2013_D_ct = IF(marital_2013_D_ct IS NULL, 0, marital_2013_D_ct) + temp2.marital_D_ct,
			marital_2013_I_ct = IF(marital_2013_I_ct IS NULL, 0, marital_2013_I_ct) + temp2.marital_I_ct,
			marital_2013_M_ct = IF(marital_2013_M_ct IS NULL, 0, marital_2013_M_ct) + temp2.marital_M_ct,
			marital_2013_W_ct = IF(marital_2013_W_ct IS NULL, 0, marital_2013_W_ct) + temp2.marital_W_ct,
			marital_2013_X_ct = IF(marital_2013_X_ct IS NULL, 0, marital_2013_X_ct) + temp2.marital_X_ct,
			marital_2013_ct = IF(marital_2013_ct IS NULL, 0, marital_2013_ct) + temp2.marital_ct
; -- 57 sec

UPDATE 		hcup_pat_marital
INNER JOIN	(
	SELECT		visitlink, 
				MAX(marital_N_ct) marital_N_ct, 
				MAX(marital_D_ct) marital_D_ct,
				MAX(marital_I_ct) marital_I_ct,
				MAX(marital_M_ct) marital_M_ct,
				MAX(marital_W_ct) marital_W_ct,
				MAX(marital_X_ct) marital_X_ct,
                SUM(marital_ct) marital_ct
	FROM
	(
		SELECT 		visitlink, 
					IF(maritalstatusub04 IS NULL OR maritalstatusub04 = '', COUNT(`key`), 0) marital_N_ct, 
					IF(maritalstatusub04 = 'D', COUNT(`key`), 0) marital_D_ct, 
					IF(maritalstatusub04 = 'I', COUNT(`key`), 0) marital_I_ct, 
					IF(maritalstatusub04 = 'M', COUNT(`key`), 0) marital_M_ct, 
					IF(maritalstatusub04 = 'W', COUNT(`key`), 0) marital_W_ct, 
					IF(maritalstatusub04 = 'X', COUNT(`key`), 0) marital_X_ct,
                    COUNT(`key`) marital_ct
		FROM 		2013_sedd_core 
		GROUP BY 	visitlink, maritalstatusub04
	) temp1
	GROUP BY visitlink
) temp2
ON			(hcup_pat_marital.visitlink = temp2.visitlink)
			OR 
            (hcup_pat_marital.visitlink IS NULL AND temp2.visitlink IS NULL) -- in order to include the NULL visitlink
SET			marital_2013_N_ct = IF(marital_2013_N_ct IS NULL, 0, marital_2013_N_ct) + temp2.marital_N_ct,
			marital_2013_D_ct = IF(marital_2013_D_ct IS NULL, 0, marital_2013_D_ct) + temp2.marital_D_ct,
			marital_2013_I_ct = IF(marital_2013_I_ct IS NULL, 0, marital_2013_I_ct) + temp2.marital_I_ct,
			marital_2013_M_ct = IF(marital_2013_M_ct IS NULL, 0, marital_2013_M_ct) + temp2.marital_M_ct,
			marital_2013_W_ct = IF(marital_2013_W_ct IS NULL, 0, marital_2013_W_ct) + temp2.marital_W_ct,
			marital_2013_X_ct = IF(marital_2013_X_ct IS NULL, 0, marital_2013_X_ct) + temp2.marital_X_ct,
			marital_2013_ct = IF(marital_2013_ct IS NULL, 0, marital_2013_ct) + temp2.marital_ct
; -- 189 sec

UPDATE 		hcup_pat_marital
INNER JOIN	(
	SELECT		visitlink, 
				MAX(marital_N_ct) marital_N_ct, 
				MAX(marital_D_ct) marital_D_ct,
				MAX(marital_I_ct) marital_I_ct,
				MAX(marital_M_ct) marital_M_ct,
				MAX(marital_W_ct) marital_W_ct,
				MAX(marital_X_ct) marital_X_ct,
                SUM(marital_ct) marital_ct
	FROM
	(
		SELECT 		visitlink, 
					IF(maritalstatusub04 IS NULL OR maritalstatusub04 = '', COUNT(`key`), 0) marital_N_ct, 
					IF(maritalstatusub04 = 'D', COUNT(`key`), 0) marital_D_ct, 
					IF(maritalstatusub04 = 'I', COUNT(`key`), 0) marital_I_ct, 
					IF(maritalstatusub04 = 'M', COUNT(`key`), 0) marital_M_ct, 
					IF(maritalstatusub04 = 'W', COUNT(`key`), 0) marital_W_ct, 
					IF(maritalstatusub04 = 'X', COUNT(`key`), 0) marital_X_ct,
                    COUNT(`key`) marital_ct
		FROM 		2013_sasd_core 
		GROUP BY 	visitlink, maritalstatusub04
	) temp1
	GROUP BY visitlink
) temp2
ON			(hcup_pat_marital.visitlink = temp2.visitlink)
			OR 
            (hcup_pat_marital.visitlink IS NULL AND temp2.visitlink IS NULL) -- in order to include the NULL visitlink
SET			marital_2013_N_ct = IF(marital_2013_N_ct IS NULL, 0, marital_2013_N_ct) + temp2.marital_N_ct,
			marital_2013_D_ct = IF(marital_2013_D_ct IS NULL, 0, marital_2013_D_ct) + temp2.marital_D_ct,
			marital_2013_I_ct = IF(marital_2013_I_ct IS NULL, 0, marital_2013_I_ct) + temp2.marital_I_ct,
			marital_2013_M_ct = IF(marital_2013_M_ct IS NULL, 0, marital_2013_M_ct) + temp2.marital_M_ct,
			marital_2013_W_ct = IF(marital_2013_W_ct IS NULL, 0, marital_2013_W_ct) + temp2.marital_W_ct,
			marital_2013_X_ct = IF(marital_2013_X_ct IS NULL, 0, marital_2013_X_ct) + temp2.marital_X_ct,
			marital_2013_ct = IF(marital_2013_ct IS NULL, 0, marital_2013_ct) + temp2.marital_ct
; -- 188 sec

UPDATE 		hcup_pat_marital
INNER JOIN	(
	SELECT		visitlink, 
				MAX(marital_N_ct) marital_N_ct, 
				MAX(marital_D_ct) marital_D_ct,
				MAX(marital_I_ct) marital_I_ct,
				MAX(marital_M_ct) marital_M_ct,
				MAX(marital_W_ct) marital_W_ct,
				MAX(marital_X_ct) marital_X_ct,
                SUM(marital_ct) marital_ct
	FROM
	(
		SELECT 		visitlink, 
					IF(maritalstatusub04 IS NULL OR maritalstatusub04 = '', COUNT(`key`), 0) marital_N_ct, 
					IF(maritalstatusub04 = 'D', COUNT(`key`), 0) marital_D_ct, 
					IF(maritalstatusub04 = 'I', COUNT(`key`), 0) marital_I_ct, 
					IF(maritalstatusub04 = 'M', COUNT(`key`), 0) marital_M_ct, 
					IF(maritalstatusub04 = 'W', COUNT(`key`), 0) marital_W_ct, 
					IF(maritalstatusub04 = 'X', COUNT(`key`), 0) marital_X_ct,
                    COUNT(`key`) marital_ct
		FROM 		2014_sid_core 
		GROUP BY 	visitlink, maritalstatusub04
	) temp1
	GROUP BY visitlink
) temp2
ON			(hcup_pat_marital.visitlink = temp2.visitlink)
			OR 
            (hcup_pat_marital.visitlink IS NULL AND temp2.visitlink IS NULL) -- in order to include the NULL visitlink
SET			marital_2014_N_ct = IF(marital_2014_N_ct IS NULL, 0, marital_2014_N_ct) + temp2.marital_N_ct,
			marital_2014_D_ct = IF(marital_2014_D_ct IS NULL, 0, marital_2014_D_ct) + temp2.marital_D_ct,
			marital_2014_I_ct = IF(marital_2014_I_ct IS NULL, 0, marital_2014_I_ct) + temp2.marital_I_ct,
			marital_2014_M_ct = IF(marital_2014_M_ct IS NULL, 0, marital_2014_M_ct) + temp2.marital_M_ct,
			marital_2014_W_ct = IF(marital_2014_W_ct IS NULL, 0, marital_2014_W_ct) + temp2.marital_W_ct,
			marital_2014_X_ct = IF(marital_2014_X_ct IS NULL, 0, marital_2014_X_ct) + temp2.marital_X_ct,
			marital_2014_ct = IF(marital_2014_ct IS NULL, 0, marital_2014_ct) + temp2.marital_ct
; -- 64 sec

UPDATE 		hcup_pat_marital
INNER JOIN	(
	SELECT		visitlink, 
				MAX(marital_N_ct) marital_N_ct, 
				MAX(marital_D_ct) marital_D_ct,
				MAX(marital_I_ct) marital_I_ct,
				MAX(marital_M_ct) marital_M_ct,
				MAX(marital_W_ct) marital_W_ct,
				MAX(marital_X_ct) marital_X_ct,
                SUM(marital_ct) marital_ct
	FROM
	(
		SELECT 		visitlink, 
					IF(maritalstatusub04 IS NULL OR maritalstatusub04 = '', COUNT(`key`), 0) marital_N_ct, 
					IF(maritalstatusub04 = 'D', COUNT(`key`), 0) marital_D_ct, 
					IF(maritalstatusub04 = 'I', COUNT(`key`), 0) marital_I_ct, 
					IF(maritalstatusub04 = 'M', COUNT(`key`), 0) marital_M_ct, 
					IF(maritalstatusub04 = 'W', COUNT(`key`), 0) marital_W_ct, 
					IF(maritalstatusub04 = 'X', COUNT(`key`), 0) marital_X_ct,
                    COUNT(`key`) marital_ct
		FROM 		2014_sedd_core 
		GROUP BY 	visitlink, maritalstatusub04
	) temp1
	GROUP BY visitlink
) temp2
ON			(hcup_pat_marital.visitlink = temp2.visitlink)
			OR 
            (hcup_pat_marital.visitlink IS NULL AND temp2.visitlink IS NULL) -- in order to include the NULL visitlink
SET			marital_2014_N_ct = IF(marital_2014_N_ct IS NULL, 0, marital_2014_N_ct) + temp2.marital_N_ct,
			marital_2014_D_ct = IF(marital_2014_D_ct IS NULL, 0, marital_2014_D_ct) + temp2.marital_D_ct,
			marital_2014_I_ct = IF(marital_2014_I_ct IS NULL, 0, marital_2014_I_ct) + temp2.marital_I_ct,
			marital_2014_M_ct = IF(marital_2014_M_ct IS NULL, 0, marital_2014_M_ct) + temp2.marital_M_ct,
			marital_2014_W_ct = IF(marital_2014_W_ct IS NULL, 0, marital_2014_W_ct) + temp2.marital_W_ct,
			marital_2014_X_ct = IF(marital_2014_X_ct IS NULL, 0, marital_2014_X_ct) + temp2.marital_X_ct,
			marital_2014_ct = IF(marital_2014_ct IS NULL, 0, marital_2014_ct) + temp2.marital_ct
; -- 210 sec

UPDATE 		hcup_pat_marital
INNER JOIN	(
	SELECT		visitlink, 
				MAX(marital_N_ct) marital_N_ct, 
				MAX(marital_D_ct) marital_D_ct,
				MAX(marital_I_ct) marital_I_ct,
				MAX(marital_M_ct) marital_M_ct,
				MAX(marital_W_ct) marital_W_ct,
				MAX(marital_X_ct) marital_X_ct,
                SUM(marital_ct) marital_ct
	FROM
	(
		SELECT 		visitlink, 
					IF(maritalstatusub04 IS NULL OR maritalstatusub04 = '', COUNT(`key`), 0) marital_N_ct, 
					IF(maritalstatusub04 = 'D', COUNT(`key`), 0) marital_D_ct, 
					IF(maritalstatusub04 = 'I', COUNT(`key`), 0) marital_I_ct, 
					IF(maritalstatusub04 = 'M', COUNT(`key`), 0) marital_M_ct, 
					IF(maritalstatusub04 = 'W', COUNT(`key`), 0) marital_W_ct, 
					IF(maritalstatusub04 = 'X', COUNT(`key`), 0) marital_X_ct,
                    COUNT(`key`) marital_ct
		FROM 		2014_sasd_core 
		GROUP BY 	visitlink, maritalstatusub04
	) temp1
	GROUP BY visitlink
) temp2
ON			(hcup_pat_marital.visitlink = temp2.visitlink)
			OR 
            (hcup_pat_marital.visitlink IS NULL AND temp2.visitlink IS NULL) -- in order to include the NULL visitlink
SET			marital_2014_N_ct = IF(marital_2014_N_ct IS NULL, 0, marital_2014_N_ct) + temp2.marital_N_ct,
			marital_2014_D_ct = IF(marital_2014_D_ct IS NULL, 0, marital_2014_D_ct) + temp2.marital_D_ct,
			marital_2014_I_ct = IF(marital_2014_I_ct IS NULL, 0, marital_2014_I_ct) + temp2.marital_I_ct,
			marital_2014_M_ct = IF(marital_2014_M_ct IS NULL, 0, marital_2014_M_ct) + temp2.marital_M_ct,
			marital_2014_W_ct = IF(marital_2014_W_ct IS NULL, 0, marital_2014_W_ct) + temp2.marital_W_ct,
			marital_2014_X_ct = IF(marital_2014_X_ct IS NULL, 0, marital_2014_X_ct) + temp2.marital_X_ct,
			marital_2014_ct = IF(marital_2014_ct IS NULL, 0, marital_2014_ct) + temp2.marital_ct
; -- 211 sec


# - max freq of marital status (note that if there are 2 or more that have the same freq, the order will be N, D, I, M, W)
UPDATE 		hcup_pat_marital
INNER JOIN	(
	SELECT 	visitlink,
			IF(marital_2013_N_ct IS NULL, NULL, -- if N is null, the entire row will be null (meaning that the patient didn't have any records in that year)
			IF(marital_2013_N_ct >= marital_2013_D_ct AND marital_2013_N_ct >= marital_2013_I_ct AND marital_2013_N_ct >= marital_2013_M_ct AND marital_2013_N_ct >= marital_2013_W_ct AND marital_2013_N_ct >= marital_2013_X_ct, 'N',
			IF(marital_2013_D_ct >= marital_2013_I_ct AND marital_2013_D_ct >= marital_2013_M_ct AND marital_2013_D_ct >= marital_2013_W_ct AND marital_2013_D_ct >= marital_2013_X_ct, 'D',
			IF(marital_2013_I_ct >= marital_2013_M_ct AND marital_2013_I_ct >= marital_2013_W_ct AND marital_2013_I_ct >= marital_2013_X_ct, 'I',
			IF(marital_2013_M_ct >= marital_2013_W_ct AND marital_2013_M_ct >= marital_2013_X_ct, 'M',
			IF(marital_2013_W_ct >= marital_2013_X_ct, 'M', 'X')))))) marital_2013_max,
			IF(marital_2014_N_ct IS NULL, NULL, -- if N is null, the entire row will be null (meaning that the patient didn't have any records in that year)
			IF(marital_2014_N_ct >= marital_2014_D_ct AND marital_2014_N_ct >= marital_2014_I_ct AND marital_2014_N_ct >= marital_2014_M_ct AND marital_2014_N_ct >= marital_2014_W_ct AND marital_2014_N_ct >= marital_2014_X_ct, 'N',
			IF(marital_2014_D_ct >= marital_2014_I_ct AND marital_2014_D_ct >= marital_2014_M_ct AND marital_2014_D_ct >= marital_2014_W_ct AND marital_2014_D_ct >= marital_2014_X_ct, 'D',
			IF(marital_2014_I_ct >= marital_2014_M_ct AND marital_2014_I_ct >= marital_2014_W_ct AND marital_2014_I_ct >= marital_2014_X_ct, 'I',
			IF(marital_2014_M_ct >= marital_2014_W_ct AND marital_2014_M_ct >= marital_2014_X_ct, 'M',
			IF(marital_2014_W_ct >= marital_2014_X_ct, 'M', 'X')))))) marital_2014_max
	FROM 	hcup_pat_marital
) temp
ON		(hcup_pat_marital.visitlink = temp.visitlink OR hcup_pat_marital.visitlink IS NULL AND temp.visitlink IS NULL) -- in order to include the NULL visitlink
SET		marital_2013_freq = temp.marital_2013_max,
		marital_2014_freq = temp.marital_2014_max; -- 403 sec

UPDATE 		hcup_pat_marital
INNER JOIN	(
	SELECT 	visitlink,
			IF(marital_N_ct >= marital_D_ct AND marital_N_ct >= marital_I_ct AND marital_N_ct >= marital_M_ct AND marital_N_ct >= marital_W_ct AND marital_N_ct >= marital_X_ct, 'N',
			IF(marital_D_ct >= marital_I_ct AND marital_D_ct >= marital_M_ct AND marital_D_ct >= marital_W_ct AND marital_D_ct >= marital_X_ct, 'D',
			IF(marital_I_ct >= marital_M_ct AND marital_I_ct >= marital_W_ct AND marital_I_ct >= marital_X_ct, 'I',
			IF(marital_M_ct >= marital_W_ct AND marital_M_ct >= marital_X_ct, 'M',
			IF(marital_W_ct >= marital_X_ct, 'M', 'X'))))) marital_max
	FROM
	(
		SELECT	visitlink,
				(marital_2013_N_ct + marital_2014_N_ct) marital_N_ct,
				(marital_2013_D_ct + marital_2014_D_ct) marital_D_ct,
				(marital_2013_I_ct + marital_2014_I_ct) marital_I_ct,
				(marital_2013_M_ct + marital_2014_M_ct) marital_M_ct,
				(marital_2013_W_ct + marital_2014_W_ct) marital_W_ct,
				(marital_2013_X_ct + marital_2014_X_ct) marital_X_ct
		FROM
		(
		SELECT 	visitlink,
				IF(marital_2013_N_ct IS NULL, 0, marital_2013_N_ct) marital_2013_N_ct,
				IF(marital_2013_D_ct IS NULL, 0, marital_2013_D_ct) marital_2013_D_ct,
				IF(marital_2013_I_ct IS NULL, 0, marital_2013_I_ct) marital_2013_I_ct,
				IF(marital_2013_M_ct IS NULL, 0, marital_2013_M_ct) marital_2013_M_ct,
				IF(marital_2013_W_ct IS NULL, 0, marital_2013_W_ct) marital_2013_W_ct,
				IF(marital_2013_X_ct IS NULL, 0, marital_2013_X_ct) marital_2013_X_ct,
				IF(marital_2014_N_ct IS NULL, 0, marital_2014_N_ct) marital_2014_N_ct,
				IF(marital_2014_D_ct IS NULL, 0, marital_2014_D_ct) marital_2014_D_ct,
				IF(marital_2014_I_ct IS NULL, 0, marital_2014_I_ct) marital_2014_I_ct,
				IF(marital_2014_M_ct IS NULL, 0, marital_2014_M_ct) marital_2014_M_ct,
				IF(marital_2014_W_ct IS NULL, 0, marital_2014_W_ct) marital_2014_W_ct,
				IF(marital_2014_X_ct IS NULL, 0, marital_2014_X_ct) marital_2014_X_ct
		FROM 	hcup_pat_marital
		) temp1
	) temp2
) temp3
ON		(hcup_pat_marital.visitlink = temp3.visitlink OR hcup_pat_marital.visitlink IS NULL AND temp3.visitlink IS NULL) -- in order to include the NULL visitlink
SET		marital_freq = temp3.marital_max; -- 404 sec


# -- last marital status (assuming all daystoevent are absolute across SID, SEDD and SASD)
UPDATE 		hcup_pat_marital
INNER JOIN	(
	SELECT		visitlink, 
				maritalstatusub04 marital_last,
				CAST(daystoevent AS UNSIGNED) daystoevent
	FROM		2013_sid_core temp1
	INNER JOIN
	(
		SELECT 		visitlink visitlink_max, 
					MAX(daystoevent) date_max
		FROM 		2013_sid_core 
		GROUP BY 	visitlink
	) temp2
	ON	(temp1.visitlink = temp2.visitlink_max AND temp1.daystoevent = temp2.date_max)
    OR	(temp1.visitlink IS NULL AND temp2.visitlink_max IS NULL AND temp1.daystoevent = temp2.date_max)
) temp3
ON		(hcup_pat_marital.visitlink = temp3.visitlink OR hcup_pat_marital.visitlink IS NULL AND temp3.visitlink IS NULL) -- in order to include the NULL visitlink
SET		marital_2013_last = IF(marital_2013_lastday IS NULL, temp3.marital_last, IF(marital_2013_lastday < temp3.daystoevent, temp3.marital_last, marital_2013_last)),
		marital_2013_lastday = IF(marital_2013_lastday IS NULL, temp3.daystoevent, IF(marital_2013_lastday < temp3.daystoevent, temp3.daystoevent, marital_2013_lastday))
; -- 53 sec

UPDATE 		hcup_pat_marital
INNER JOIN	(
	SELECT		visitlink, 
				maritalstatusub04 marital_last,
				CAST(daystoevent AS UNSIGNED) daystoevent
	FROM		2013_sedd_core temp1
	INNER JOIN
	(
		SELECT 		visitlink visitlink_max, 
					MAX(daystoevent) date_max
		FROM 		2013_sedd_core 
		GROUP BY 	visitlink
	) temp2
	ON	(temp1.visitlink = temp2.visitlink_max AND temp1.daystoevent = temp2.date_max)
    OR	(temp1.visitlink IS NULL AND temp2.visitlink_max IS NULL AND temp1.daystoevent = temp2.date_max)
) temp3
ON		(hcup_pat_marital.visitlink = temp3.visitlink OR hcup_pat_marital.visitlink IS NULL AND temp3.visitlink IS NULL) -- in order to include the NULL visitlink
SET		marital_2013_last = IF(marital_2013_lastday IS NULL, temp3.marital_last, IF(marital_2013_lastday < temp3.daystoevent, temp3.marital_last, marital_2013_last)),
		marital_2013_lastday = IF(marital_2013_lastday IS NULL, temp3.daystoevent, IF(marital_2013_lastday < temp3.daystoevent, temp3.daystoevent, marital_2013_lastday))
; -- 187 sec

UPDATE 		hcup_pat_marital
INNER JOIN	(
	SELECT		visitlink, 
				maritalstatusub04 marital_last,
				CAST(daystoevent AS UNSIGNED) daystoevent
	FROM		2013_sasd_core temp1
	INNER JOIN
	(
		SELECT 		visitlink visitlink_max, 
					MAX(daystoevent) date_max
		FROM 		2013_sasd_core 
		GROUP BY 	visitlink
	) temp2
	ON	(temp1.visitlink = temp2.visitlink_max AND temp1.daystoevent = temp2.date_max)
    OR	(temp1.visitlink IS NULL AND temp2.visitlink_max IS NULL AND temp1.daystoevent = temp2.date_max)
) temp3
ON		(hcup_pat_marital.visitlink = temp3.visitlink OR hcup_pat_marital.visitlink IS NULL AND temp3.visitlink IS NULL) -- in order to include the NULL visitlink
SET		marital_2013_last = IF(marital_2013_lastday IS NULL, temp3.marital_last, IF(marital_2013_lastday < temp3.daystoevent, temp3.marital_last, marital_2013_last)),
		marital_2013_lastday = IF(marital_2013_lastday IS NULL, temp3.daystoevent, IF(marital_2013_lastday < temp3.daystoevent, temp3.daystoevent, marital_2013_lastday))
; -- 179 sec


UPDATE 		hcup_pat_marital
INNER JOIN	(
	SELECT		visitlink, 
				maritalstatusub04 marital_last,
				CAST(daystoevent AS UNSIGNED) daystoevent
	FROM		2014_sid_core temp1
	INNER JOIN
	(
		SELECT 		visitlink visitlink_max, 
					MAX(daystoevent) date_max
		FROM 		2014_sid_core 
		GROUP BY 	visitlink
	) temp2
	ON	(temp1.visitlink = temp2.visitlink_max AND temp1.daystoevent = temp2.date_max)
    OR	(temp1.visitlink IS NULL AND temp2.visitlink_max IS NULL AND temp1.daystoevent = temp2.date_max)
) temp3
ON		(hcup_pat_marital.visitlink = temp3.visitlink OR hcup_pat_marital.visitlink IS NULL AND temp3.visitlink IS NULL) -- in order to include the NULL visitlink
SET		marital_2014_last = IF(marital_2014_lastday IS NULL, temp3.marital_last, IF(marital_2014_lastday < temp3.daystoevent, temp3.marital_last, marital_2014_last)),
		marital_2014_lastday = IF(marital_2014_lastday IS NULL, temp3.daystoevent, IF(marital_2014_lastday < temp3.daystoevent, temp3.daystoevent, marital_2014_lastday))
; -- 59 sec

UPDATE 		hcup_pat_marital
INNER JOIN	(
	SELECT		visitlink, 
				maritalstatusub04 marital_last,
				CAST(daystoevent AS UNSIGNED) daystoevent
	FROM		2014_sedd_core temp1
	INNER JOIN
	(
		SELECT 		visitlink visitlink_max, 
					MAX(daystoevent) date_max
		FROM 		2014_sedd_core 
		GROUP BY 	visitlink
	) temp2
	ON	(temp1.visitlink = temp2.visitlink_max AND temp1.daystoevent = temp2.date_max)
    OR	(temp1.visitlink IS NULL AND temp2.visitlink_max IS NULL AND temp1.daystoevent = temp2.date_max)
) temp3
ON		(hcup_pat_marital.visitlink = temp3.visitlink OR hcup_pat_marital.visitlink IS NULL AND temp3.visitlink IS NULL) -- in order to include the NULL visitlink
SET		marital_2014_last = IF(marital_2014_lastday IS NULL, temp3.marital_last, IF(marital_2014_lastday < temp3.daystoevent, temp3.marital_last, marital_2014_last)),
		marital_2014_lastday = IF(marital_2014_lastday IS NULL, temp3.daystoevent, IF(marital_2014_lastday < temp3.daystoevent, temp3.daystoevent, marital_2014_lastday))
; -- 200 sec

UPDATE 		hcup_pat_marital
INNER JOIN	(
	SELECT		visitlink, 
				maritalstatusub04 marital_last,
				CAST(daystoevent AS UNSIGNED) daystoevent
	FROM		2014_sasd_core temp1
	INNER JOIN
	(
		SELECT 		visitlink visitlink_max, 
					MAX(daystoevent) date_max
		FROM 		2014_sasd_core 
		GROUP BY 	visitlink
	) temp2
	ON	(temp1.visitlink = temp2.visitlink_max AND temp1.daystoevent = temp2.date_max)
    OR	(temp1.visitlink IS NULL AND temp2.visitlink_max IS NULL AND temp1.daystoevent = temp2.date_max)
) temp3
ON		(hcup_pat_marital.visitlink = temp3.visitlink OR hcup_pat_marital.visitlink IS NULL AND temp3.visitlink IS NULL) -- in order to include the NULL visitlink
SET		marital_2014_last = IF(marital_2014_lastday IS NULL, temp3.marital_last, IF(marital_2014_lastday < temp3.daystoevent, temp3.marital_last, marital_2014_last)),
		marital_2014_lastday = IF(marital_2014_lastday IS NULL, temp3.daystoevent, IF(marital_2014_lastday < temp3.daystoevent, temp3.daystoevent, marital_2014_lastday))
; -- 187 sec


UPDATE 		hcup_pat_marital
INNER JOIN	(
	SELECT 	visitlink, 
			IF(marital_2014_last IS NOT NULL AND marital_2014_last <> '', marital_2014_last, marital_2013_last) marital_last
	FROM 	hcup_pat_marital
) temp
ON		(hcup_pat_marital.visitlink = temp.visitlink OR hcup_pat_marital.visitlink IS NULL AND temp.visitlink IS NULL) -- in order to include the NULL visitlink
SET		hcup_pat_marital.marital_last = temp.marital_last
; -- 371 sec

SELECT COUNT(*) FROM hcup.hcup_pat_marital WHERE marital_last IS NULL OR marital_last = ''; -- 25 sec | 60,805 patients

SELECT COUNT(*) FROM hcup.hcup_pat_marital WHERE marital_freq IS NULL OR marital_freq = ''; -- 24 sec | 0 patients

# fixing records where the last recorded marital status has been null or empty (replacing them with the most frequent one) ... ideally should be replace with the last one before the last one)
UPDATE 		hcup_pat_marital
SET			marital_last = IF(marital_last IS NULL OR marital_last = '', marital_freq, marital_last); -- 24 sec

UPDATE 		hcup_pat_marital
SET			marital_2013_flag = IF((marital_2013_freq <> marital_2013_last) OR (marital_2013_freq IS NULL AND marital_2013_last IS NOT NULL) OR (marital_2013_freq IS NOT NULL AND marital_2013_last IS NULL), 1, 0),
			marital_2014_flag = IF((marital_2014_freq <> marital_2014_last) OR (marital_2014_freq IS NULL AND marital_2014_last IS NOT NULL) OR (marital_2014_freq IS NOT NULL AND marital_2014_last IS NULL), 1, 0); -- 41 sec
            
UPDATE 		hcup_pat_marital
SET			marital_flag = IF((marital_2013_flag = 1 OR marital_2013_flag = 1), 1, 0); -- 41 sec

#find patients who are less than 15, then see if they are married, if so, flag them
#This rule was chosen because that is the law in Maryland
SET SQL_SAFE_UPDATES=0;

#marital_age_flag is triggered when the patient is less than 15 y/o, they don't have an age flag,
#and their marriage status is not individual or null

UPDATE hcup_pat_marital
INNER JOIN (
	SELECT visitlink, age_min, age_flag
	FROM hcup_pat_age 
	GROUP BY visitlink
) temp
ON 	(hcup_pat_marital.visitlink = temp.visitlink)
OR 	(hcup_pat_marital.visitlink IS NULL AND temp.visitlink IS NULL)
SET 	marital_age_flag = IF(temp.age_min < 15 AND temp.age_flag != 1 AND marital_last != 'I'
								AND marital_last != 'N', 1, 0);

#SELECT * FROM hcup_pat_marital WHERE visitlink = 1825371;
SELECT COUNT(*), marital_age_flag FROM hcup_pat_marital
GROUP BY marital_age_flag;
/* 
	The number of patients who are younger than 15 with a marriage status other than individual or null
	+ ------- + ----------------- +
	| ct      | marital_age_flag  |
	+ ------- + ----------------- +
	| 3261830 | 0                 |
	| 2035    | 1                 |
	+ ------- + ----------------- +

*/

SELECT 		COUNT(*) ct, marital_flag 
FROM 		hcup_pat_marital
GROUP BY	marital_flag; -- 23 sec  (shows that around 205k of patients had an issue such as changing marital status in these 2 years)

/*

	+ ------- + ----------------- +
	| ct      | marital_flag      |
	+ ------- + ----------------- +
	| 3058474 | 0                 |
	| 205391  | 1                 |
	+ ------- + ----------------- +

*/

# age range of patients based on their marital status
CREATE TABLE	hcup_pat_marital_stat
SELECT		temp.age_range age_range, temp.marital_last marital_last, temp.marital_ct marital_ct, hcup_pat_age_stat.pat_ct pat_ct, ROUND((temp.marital_ct/hcup_pat_age_stat.pat_ct)*100, 2) marital_per
FROM
(	SELECT		IF(age_avg < 1, '0-1', IF(age_avg < 10, '1-10', IF(age_avg < 20, '10-20', IF(age_avg < 30, '20-30', IF(age_avg < 40, '30-40', IF(age_avg < 50, '40-50', IF(age_avg < 60, '50-60', IF(age_avg < 70, '60-70', IF(age_avg < 80, '70-80', IF(age_avg < 90, '80-90', '90+')))))))))) age_range,
				marital_last,
                COUNT(*) marital_ct
	FROM		hcup_pat_age, hcup_pat_marital
	WHERE		hcup_pat_age.visitlink = hcup_pat_marital.visitlink
	AND			age_flag = 0 
	-- AND 		marital_flag = 0  -- optional as this might be simply a change in marital status
	GROUP BY	age_range, marital_last
    ) temp,
hcup_pat_age_stat
WHERE	hcup_pat_age_stat.age_range = temp.age_range; -- 294 sec

# issue with marital status of M, W, D for low ages?

SELECT	COUNT(*)
FROM	hcup_pat_marital, hcup_pat_age
WHERE	hcup_pat_marital.visitlink = hcup_pat_age.visitlink
AND		hcup_pat_age.age_flag = 0
AND		hcup_pat_age.age_max < 16 -- see: http://statelaws.findlaw.com/maryland-law/maryland-marriage-age-requirements-laws.html
AND		hcup_pat_marital.marital_last IN ('M', 'A', 'B', 'S', 'X', 'D', 'W'); -- 61 sec; 2153 patients

UPDATE 		hcup_pat_marital
INNER JOIN	(
				SELECT	*
				FROM	hcup_pat_age
				WHERE	hcup_pat_age.age_flag = 0
				AND		hcup_pat_age.age_max < 16
            ) temp
ON			hcup_pat_marital.visitlink = temp.visitlink
AND			hcup_pat_marital.marital_last IN ('M', 'A', 'B', 'S', 'X', 'D', 'W')
SET			marital_age_flag = 1, marital_flag = 1; -- 65 sec; 2153 patients in age_flag & 2018 patients in [main] flag





# ------------- table to hold pat race status information
CREATE TABLE `hcup_pat_race` 
(
  `visitlink` varchar(16) DEFAULT NULL,
  
  #number of reported races for each patient in each year
  `race_2013_N_ct` int(4) DEFAULT NULL, 
  `race_2013_W_ct` int(4) DEFAULT NULL,
  `race_2013_B_ct` int(4) DEFAULT NULL,
  `race_2013_H_ct` int(4) DEFAULT NULL,
  `race_2013_A_ct` int(4) DEFAULT NULL,
  `race_2013_T_ct` int(4) DEFAULT NULL,
  `race_2013_O_ct` int(4) DEFAULT NULL,
  `race_2013_ct` int(4) DEFAULT NULL,

  `race_2014_N_ct` int(4) DEFAULT NULL, 
  `race_2014_W_ct` int(4) DEFAULT NULL,
  `race_2014_B_ct` int(4) DEFAULT NULL,
  `race_2014_H_ct` int(4) DEFAULT NULL,
  `race_2014_A_ct` int(4) DEFAULT NULL,
  `race_2014_T_ct` int(4) DEFAULT NULL,
  `race_2014_O_ct` int(4) DEFAULT NULL,
  `race_2014_ct` int(4) DEFAULT NULL,

  #total number of reported races for each patient across both years
  `N_ct` int(4) DEFAULT NULL,
  `W_ct` int(4) DEFAULT NULL,
  `B_ct` int(4) DEFAULT NULL,
  `H_ct` int(4) DEFAULT NULL,
  `A_ct` int(4) DEFAULT NULL,
  `T_ct` int(4) DEFAULT NULL,
  `O_ct` int(4) DEFAULT NULL,
  
  `race_2013_lastday` int(6) DEFAULT NULL,
  `race_2013_last` varchar(1) DEFAULT NULL,
  `race_2014_lastday` int(6) DEFAULT NULL,
  `race_2014_last` varchar(1) DEFAULT NULL,
  `race_last` varchar(1) DEFAULT NULL,

  `race_flag` int(1) DEFAULT NULL,
  
  UNIQUE KEY `visitlink_UNIQUE` (`visitlink`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*
	1	White
	2	Black
	3	Hispanic
	4	Asian or Pacific Islander
	5	Native American
	6	Other
	.	Missing
	.A	Invalid
	.B	Unavailable from source (coded in 1988-1997 data only)
*/


INSERT INTO hcup_pat_race (visitlink)
SELECT 		visitlink 
FROM 		hcup_pat_visitlink_uni; -- 301 sec

UPDATE 		hcup_pat_race
INNER JOIN	(
	SELECT		visitlink, 
				MAX(race_N_ct) race_N_ct, 
				MAX(race_W_ct) race_W_ct,
				MAX(race_B_ct) race_B_ct,
				MAX(race_H_ct) race_H_ct,
				MAX(race_A_ct) race_A_ct,
				MAX(race_T_ct) race_T_ct,
				MAX(race_O_ct) race_O_ct,
                SUM(race_ct) race_ct
	FROM
	(
		SELECT 		visitlink, 
					IF(race IS NULL OR race = '', COUNT(`key`), 0) race_N_ct, 
					IF(race = '1', COUNT(`key`), 0) race_W_ct, 
					IF(race = '2', COUNT(`key`), 0) race_B_ct, 
					IF(race = '3', COUNT(`key`), 0) race_H_ct, 
					IF(race = '4', COUNT(`key`), 0) race_A_ct, 
					IF(race = '5', COUNT(`key`), 0) race_T_ct,
					IF(race = '6', COUNT(`key`), 0) race_O_ct,
                    COUNT(`key`) race_ct
		FROM 		2013_sid_core 
		GROUP BY 	visitlink, race
	) temp1
	GROUP BY visitlink
) temp2
ON			(hcup_pat_race.visitlink = temp2.visitlink)
			OR 
            (hcup_pat_race.visitlink IS NULL AND temp2.visitlink IS NULL) -- in order to include the NULL visitlink
SET			race_2013_N_ct = IF(race_2013_N_ct IS NULL, 0, race_2013_N_ct) + temp2.race_N_ct,
			race_2013_W_ct = IF(race_2013_W_ct IS NULL, 0, race_2013_W_ct) + temp2.race_W_ct,
			race_2013_B_ct = IF(race_2013_B_ct IS NULL, 0, race_2013_B_ct) + temp2.race_B_ct,
			race_2013_H_ct = IF(race_2013_H_ct IS NULL, 0, race_2013_H_ct) + temp2.race_H_ct,
			race_2013_A_ct = IF(race_2013_A_ct IS NULL, 0, race_2013_A_ct) + temp2.race_A_ct,
			race_2013_T_ct = IF(race_2013_T_ct IS NULL, 0, race_2013_T_ct) + temp2.race_T_ct,
			race_2013_O_ct = IF(race_2013_O_ct IS NULL, 0, race_2013_O_ct) + temp2.race_O_ct,
			race_2013_ct = IF(race_2013_ct IS NULL, 0, race_2013_ct) + temp2.race_ct
; -- 60 sec


UPDATE 		hcup_pat_race
INNER JOIN	(
	SELECT		visitlink, 
				MAX(race_N_ct) race_N_ct, 
				MAX(race_W_ct) race_W_ct,
				MAX(race_B_ct) race_B_ct,
				MAX(race_H_ct) race_H_ct,
				MAX(race_A_ct) race_A_ct,
				MAX(race_T_ct) race_T_ct,
				MAX(race_O_ct) race_O_ct,
                SUM(race_ct) race_ct
	FROM
	(
		SELECT 		visitlink, 
					IF(race IS NULL OR race = '', COUNT(`key`), 0) race_N_ct, 
					IF(race = '1', COUNT(`key`), 0) race_W_ct, 
					IF(race = '2', COUNT(`key`), 0) race_B_ct, 
					IF(race = '3', COUNT(`key`), 0) race_H_ct, 
					IF(race = '4', COUNT(`key`), 0) race_A_ct, 
					IF(race = '5', COUNT(`key`), 0) race_T_ct,
					IF(race = '6', COUNT(`key`), 0) race_O_ct,
                    COUNT(`key`) race_ct
		FROM 		2013_sedd_core 
		GROUP BY 	visitlink, race
	) temp1
	GROUP BY visitlink
) temp2
ON			(hcup_pat_race.visitlink = temp2.visitlink)
			OR 
            (hcup_pat_race.visitlink IS NULL AND temp2.visitlink IS NULL) -- in order to include the NULL visitlink
SET			race_2013_N_ct = IF(race_2013_N_ct IS NULL, 0, race_2013_N_ct) + temp2.race_N_ct,
			race_2013_W_ct = IF(race_2013_W_ct IS NULL, 0, race_2013_W_ct) + temp2.race_W_ct,
			race_2013_B_ct = IF(race_2013_B_ct IS NULL, 0, race_2013_B_ct) + temp2.race_B_ct,
			race_2013_H_ct = IF(race_2013_H_ct IS NULL, 0, race_2013_H_ct) + temp2.race_H_ct,
			race_2013_A_ct = IF(race_2013_A_ct IS NULL, 0, race_2013_A_ct) + temp2.race_A_ct,
			race_2013_T_ct = IF(race_2013_T_ct IS NULL, 0, race_2013_T_ct) + temp2.race_T_ct,
			race_2013_O_ct = IF(race_2013_O_ct IS NULL, 0, race_2013_O_ct) + temp2.race_O_ct,
			race_2013_ct = IF(race_2013_ct IS NULL, 0, race_2013_ct) + temp2.race_ct
; -- 196 sec


UPDATE 		hcup_pat_race
INNER JOIN	(
	SELECT		visitlink, 
				MAX(race_N_ct) race_N_ct, 
				MAX(race_W_ct) race_W_ct,
				MAX(race_B_ct) race_B_ct,
				MAX(race_H_ct) race_H_ct,
				MAX(race_A_ct) race_A_ct,
				MAX(race_T_ct) race_T_ct,
				MAX(race_O_ct) race_O_ct,
                SUM(race_ct) race_ct
	FROM
	(
		SELECT 		visitlink, 
					IF(race IS NULL OR race = '', COUNT(`key`), 0) race_N_ct, 
					IF(race = '1', COUNT(`key`), 0) race_W_ct, 
					IF(race = '2', COUNT(`key`), 0) race_B_ct, 
					IF(race = '3', COUNT(`key`), 0) race_H_ct, 
					IF(race = '4', COUNT(`key`), 0) race_A_ct, 
					IF(race = '5', COUNT(`key`), 0) race_T_ct,
					IF(race = '6', COUNT(`key`), 0) race_O_ct,
                    COUNT(`key`) race_ct
		FROM 		2013_sasd_core 
		GROUP BY 	visitlink, race
	) temp1
	GROUP BY visitlink
) temp2
ON			(hcup_pat_race.visitlink = temp2.visitlink)
			OR 
            (hcup_pat_race.visitlink IS NULL AND temp2.visitlink IS NULL) -- in order to include the NULL visitlink
SET			race_2013_N_ct = IF(race_2013_N_ct IS NULL, 0, race_2013_N_ct) + temp2.race_N_ct,
			race_2013_W_ct = IF(race_2013_W_ct IS NULL, 0, race_2013_W_ct) + temp2.race_W_ct,
			race_2013_B_ct = IF(race_2013_B_ct IS NULL, 0, race_2013_B_ct) + temp2.race_B_ct,
			race_2013_H_ct = IF(race_2013_H_ct IS NULL, 0, race_2013_H_ct) + temp2.race_H_ct,
			race_2013_A_ct = IF(race_2013_A_ct IS NULL, 0, race_2013_A_ct) + temp2.race_A_ct,
			race_2013_T_ct = IF(race_2013_T_ct IS NULL, 0, race_2013_T_ct) + temp2.race_T_ct,
			race_2013_O_ct = IF(race_2013_O_ct IS NULL, 0, race_2013_O_ct) + temp2.race_O_ct,
			race_2013_ct = IF(race_2013_ct IS NULL, 0, race_2013_ct) + temp2.race_ct
; -- 193 sec


UPDATE 		hcup_pat_race
INNER JOIN	(
	SELECT		visitlink, 
				MAX(race_N_ct) race_N_ct, 
				MAX(race_W_ct) race_W_ct,
				MAX(race_B_ct) race_B_ct,
				MAX(race_H_ct) race_H_ct,
				MAX(race_A_ct) race_A_ct,
				MAX(race_T_ct) race_T_ct,
				MAX(race_O_ct) race_O_ct,
                SUM(race_ct) race_ct
	FROM
	(
		SELECT 		visitlink, 
					IF(race IS NULL OR race = '', COUNT(`key`), 0) race_N_ct, 
					IF(race = '1', COUNT(`key`), 0) race_W_ct, 
					IF(race = '2', COUNT(`key`), 0) race_B_ct, 
					IF(race = '3', COUNT(`key`), 0) race_H_ct, 
					IF(race = '4', COUNT(`key`), 0) race_A_ct, 
					IF(race = '5', COUNT(`key`), 0) race_T_ct,
					IF(race = '6', COUNT(`key`), 0) race_O_ct,
                    COUNT(`key`) race_ct
		FROM 		2014_sid_core 
		GROUP BY 	visitlink, race
	) temp1
	GROUP BY visitlink
) temp2
ON			(hcup_pat_race.visitlink = temp2.visitlink)
			OR 
            (hcup_pat_race.visitlink IS NULL AND temp2.visitlink IS NULL) -- in order to include the NULL visitlink
SET			race_2014_N_ct = IF(race_2014_N_ct IS NULL, 0, race_2014_N_ct) + temp2.race_N_ct,
			race_2014_W_ct = IF(race_2014_W_ct IS NULL, 0, race_2014_W_ct) + temp2.race_W_ct,
			race_2014_B_ct = IF(race_2014_B_ct IS NULL, 0, race_2014_B_ct) + temp2.race_B_ct,
			race_2014_H_ct = IF(race_2014_H_ct IS NULL, 0, race_2014_H_ct) + temp2.race_H_ct,
			race_2014_A_ct = IF(race_2014_A_ct IS NULL, 0, race_2014_A_ct) + temp2.race_A_ct,
			race_2014_T_ct = IF(race_2014_T_ct IS NULL, 0, race_2014_T_ct) + temp2.race_T_ct,
			race_2014_O_ct = IF(race_2014_O_ct IS NULL, 0, race_2014_O_ct) + temp2.race_O_ct,
			race_2014_ct = IF(race_2014_ct IS NULL, 0, race_2014_ct) + temp2.race_ct
; -- 66 sec


UPDATE 		hcup_pat_race
INNER JOIN	(
	SELECT		visitlink, 
				MAX(race_N_ct) race_N_ct, 
				MAX(race_W_ct) race_W_ct,
				MAX(race_B_ct) race_B_ct,
				MAX(race_H_ct) race_H_ct,
				MAX(race_A_ct) race_A_ct,
				MAX(race_T_ct) race_T_ct,
				MAX(race_O_ct) race_O_ct,
                SUM(race_ct) race_ct
	FROM
	(
		SELECT 		visitlink, 
					IF(race IS NULL OR race = '', COUNT(`key`), 0) race_N_ct, 
					IF(race = '1', COUNT(`key`), 0) race_W_ct, 
					IF(race = '2', COUNT(`key`), 0) race_B_ct, 
					IF(race = '3', COUNT(`key`), 0) race_H_ct, 
					IF(race = '4', COUNT(`key`), 0) race_A_ct, 
					IF(race = '5', COUNT(`key`), 0) race_T_ct,
					IF(race = '6', COUNT(`key`), 0) race_O_ct,
                    COUNT(`key`) race_ct
		FROM 		2014_sedd_core 
		GROUP BY 	visitlink, race
	) temp1
	GROUP BY visitlink
) temp2
ON			(hcup_pat_race.visitlink = temp2.visitlink)
			OR 
            (hcup_pat_race.visitlink IS NULL AND temp2.visitlink IS NULL) -- in order to include the NULL visitlink
SET			race_2014_N_ct = IF(race_2014_N_ct IS NULL, 0, race_2014_N_ct) + temp2.race_N_ct,
			race_2014_W_ct = IF(race_2014_W_ct IS NULL, 0, race_2014_W_ct) + temp2.race_W_ct,
			race_2014_B_ct = IF(race_2014_B_ct IS NULL, 0, race_2014_B_ct) + temp2.race_B_ct,
			race_2014_H_ct = IF(race_2014_H_ct IS NULL, 0, race_2014_H_ct) + temp2.race_H_ct,
			race_2014_A_ct = IF(race_2014_A_ct IS NULL, 0, race_2014_A_ct) + temp2.race_A_ct,
			race_2014_T_ct = IF(race_2014_T_ct IS NULL, 0, race_2014_T_ct) + temp2.race_T_ct,
			race_2014_O_ct = IF(race_2014_O_ct IS NULL, 0, race_2014_O_ct) + temp2.race_O_ct,
			race_2014_ct = IF(race_2014_ct IS NULL, 0, race_2014_ct) + temp2.race_ct
; -- 217 sec


UPDATE 		hcup_pat_race
INNER JOIN	(
	SELECT		visitlink, 
				MAX(race_N_ct) race_N_ct, 
				MAX(race_W_ct) race_W_ct,
				MAX(race_B_ct) race_B_ct,
				MAX(race_H_ct) race_H_ct,
				MAX(race_A_ct) race_A_ct,
				MAX(race_T_ct) race_T_ct,
				MAX(race_O_ct) race_O_ct,
                SUM(race_ct) race_ct
	FROM
	(
		SELECT 		visitlink, 
					IF(race IS NULL OR race = '', COUNT(`key`), 0) race_N_ct, 
					IF(race = '1', COUNT(`key`), 0) race_W_ct, 
					IF(race = '2', COUNT(`key`), 0) race_B_ct, 
					IF(race = '3', COUNT(`key`), 0) race_H_ct, 
					IF(race = '4', COUNT(`key`), 0) race_A_ct, 
					IF(race = '5', COUNT(`key`), 0) race_T_ct,
					IF(race = '6', COUNT(`key`), 0) race_O_ct,
                    COUNT(`key`) race_ct
		FROM 		2014_sasd_core 
		GROUP BY 	visitlink, race
	) temp1
	GROUP BY visitlink
) temp2
ON			(hcup_pat_race.visitlink = temp2.visitlink)
			OR 
            (hcup_pat_race.visitlink IS NULL AND temp2.visitlink IS NULL) -- in order to include the NULL visitlink
SET			race_2014_N_ct = IF(race_2014_N_ct IS NULL, 0, race_2014_N_ct) + temp2.race_N_ct,
			race_2014_W_ct = IF(race_2014_W_ct IS NULL, 0, race_2014_W_ct) + temp2.race_W_ct,
			race_2014_B_ct = IF(race_2014_B_ct IS NULL, 0, race_2014_B_ct) + temp2.race_B_ct,
			race_2014_H_ct = IF(race_2014_H_ct IS NULL, 0, race_2014_H_ct) + temp2.race_H_ct,
			race_2014_A_ct = IF(race_2014_A_ct IS NULL, 0, race_2014_A_ct) + temp2.race_A_ct,
			race_2014_T_ct = IF(race_2014_T_ct IS NULL, 0, race_2014_T_ct) + temp2.race_T_ct,
			race_2014_O_ct = IF(race_2014_O_ct IS NULL, 0, race_2014_O_ct) + temp2.race_O_ct,
			race_2014_ct = IF(race_2014_ct IS NULL, 0, race_2014_ct) + temp2.race_ct
; -- 206 sec

# -- last race status (assuming all daystoevent are absolute across SID, SEDD and SASD)
UPDATE 		hcup_pat_race
INNER JOIN	(
	SELECT		visitlink, 
				race race_last,
				CAST(daystoevent AS UNSIGNED) daystoevent
	FROM		2013_sid_core temp1
	INNER JOIN
	(
		SELECT 		visitlink visitlink_max, 
					MAX(daystoevent) date_max
		FROM 		2013_sid_core 
		GROUP BY 	visitlink
	) temp2
	ON	(temp1.visitlink = temp2.visitlink_max AND temp1.daystoevent = temp2.date_max)
    OR	(temp1.visitlink IS NULL AND temp2.visitlink_max IS NULL AND temp1.daystoevent = temp2.date_max)
) temp3
ON		(hcup_pat_race.visitlink = temp3.visitlink OR hcup_pat_race.visitlink IS NULL AND temp3.visitlink IS NULL) -- in order to include the NULL visitlink
SET		race_2013_last = IF(race_2013_lastday IS NULL, temp3.race_last, IF(race_2013_lastday < temp3.daystoevent, temp3.race_last, race_2013_last)),
		race_2013_lastday = IF(race_2013_lastday IS NULL, temp3.daystoevent, IF(race_2013_lastday < temp3.daystoevent, temp3.daystoevent, race_2013_lastday))
; -- 65 sec

UPDATE 		hcup_pat_race
INNER JOIN	(
	SELECT		visitlink, 
				race race_last,
				CAST(daystoevent AS UNSIGNED) daystoevent
	FROM		2013_sedd_core temp1
	INNER JOIN
	(
		SELECT 		visitlink visitlink_max, 
					MAX(daystoevent) date_max
		FROM 		2013_sedd_core 
		GROUP BY 	visitlink
	) temp2
	ON	(temp1.visitlink = temp2.visitlink_max AND temp1.daystoevent = temp2.date_max)
    OR	(temp1.visitlink IS NULL AND temp2.visitlink_max IS NULL AND temp1.daystoevent = temp2.date_max)
) temp3
ON		(hcup_pat_race.visitlink = temp3.visitlink OR hcup_pat_race.visitlink IS NULL AND temp3.visitlink IS NULL) -- in order to include the NULL visitlink
SET		race_2013_last = IF(race_2013_lastday IS NULL, temp3.race_last, IF(race_2013_lastday < temp3.daystoevent, temp3.race_last, race_2013_last)),
		race_2013_lastday = IF(race_2013_lastday IS NULL, temp3.daystoevent, IF(race_2013_lastday < temp3.daystoevent, temp3.daystoevent, race_2013_lastday))
; -- 225 sec

UPDATE 		hcup_pat_race
INNER JOIN	(
	SELECT		visitlink, 
				race race_last,
				CAST(daystoevent AS UNSIGNED) daystoevent
	FROM		2013_sasd_core temp1
	INNER JOIN
	(
		SELECT 		visitlink visitlink_max, 
					MAX(daystoevent) date_max
		FROM 		2013_sasd_core 
		GROUP BY 	visitlink
	) temp2
	ON	(temp1.visitlink = temp2.visitlink_max AND temp1.daystoevent = temp2.date_max)
    OR	(temp1.visitlink IS NULL AND temp2.visitlink_max IS NULL AND temp1.daystoevent = temp2.date_max)
) temp3
ON		(hcup_pat_race.visitlink = temp3.visitlink OR hcup_pat_race.visitlink IS NULL AND temp3.visitlink IS NULL) -- in order to include the NULL visitlink
SET		race_2013_last = IF(race_2013_lastday IS NULL, temp3.race_last, IF(race_2013_lastday < temp3.daystoevent, temp3.race_last, race_2013_last)),
		race_2013_lastday = IF(race_2013_lastday IS NULL, temp3.daystoevent, IF(race_2013_lastday < temp3.daystoevent, temp3.daystoevent, race_2013_lastday))
; -- 211 sec

UPDATE 		hcup_pat_race -- need to convert internal coding of 1, 2, ... to W, B, ...
SET			race_2013_last = IF(race_2013_last IS NULL OR race_2013_last = '', 'N', IF(race_2013_last = '1', 'W', IF(race_2013_last = '2', 'B', IF(race_2013_last = '3', 'H', IF(race_2013_last = '4', 'A', IF(race_2013_last = '5', 'T', IF(race_2013_last = '6', 'O', 'N')))))))
; -- 88 sec


UPDATE 		hcup_pat_race
INNER JOIN	(
	SELECT		visitlink, 
				race race_last,
				CAST(daystoevent AS UNSIGNED) daystoevent
	FROM		2014_sid_core temp1
	INNER JOIN
	(
		SELECT 		visitlink visitlink_max, 
					MAX(daystoevent) date_max
		FROM 		2014_sid_core 
		GROUP BY 	visitlink
	) temp2
	ON	(temp1.visitlink = temp2.visitlink_max AND temp1.daystoevent = temp2.date_max)
    OR	(temp1.visitlink IS NULL AND temp2.visitlink_max IS NULL AND temp1.daystoevent = temp2.date_max)
) temp3
ON		(hcup_pat_race.visitlink = temp3.visitlink OR hcup_pat_race.visitlink IS NULL AND temp3.visitlink IS NULL) -- in order to include the NULL visitlink
SET		race_2014_last = IF(race_2014_lastday IS NULL, temp3.race_last, IF(race_2014_lastday < temp3.daystoevent, temp3.race_last, race_2014_last)),
		race_2014_lastday = IF(race_2014_lastday IS NULL, temp3.daystoevent, IF(race_2014_lastday < temp3.daystoevent, temp3.daystoevent, race_2014_lastday))
; -- 74 sec

UPDATE 		hcup_pat_race
INNER JOIN	(
	SELECT		visitlink, 
				race race_last,
				CAST(daystoevent AS UNSIGNED) daystoevent
	FROM		2014_sedd_core temp1
	INNER JOIN
	(
		SELECT 		visitlink visitlink_max, 
					MAX(daystoevent) date_max
		FROM 		2014_sedd_core 
		GROUP BY 	visitlink
	) temp2
	ON	(temp1.visitlink = temp2.visitlink_max AND temp1.daystoevent = temp2.date_max)
    OR	(temp1.visitlink IS NULL AND temp2.visitlink_max IS NULL AND temp1.daystoevent = temp2.date_max)
) temp3
ON		(hcup_pat_race.visitlink = temp3.visitlink OR hcup_pat_race.visitlink IS NULL AND temp3.visitlink IS NULL) -- in order to include the NULL visitlink
SET		race_2014_last = IF(race_2014_lastday IS NULL, temp3.race_last, IF(race_2014_lastday < temp3.daystoevent, temp3.race_last, race_2014_last)),
		race_2014_lastday = IF(race_2014_lastday IS NULL, temp3.daystoevent, IF(race_2014_lastday < temp3.daystoevent, temp3.daystoevent, race_2014_lastday))
; -- 242 sec

UPDATE 		hcup_pat_race
INNER JOIN	(
	SELECT		visitlink, 
				race race_last,
				CAST(daystoevent AS UNSIGNED) daystoevent
	FROM		2014_sasd_core temp1
	INNER JOIN
	(
		SELECT 		visitlink visitlink_max, 
					MAX(daystoevent) date_max
		FROM 		2014_sasd_core 
		GROUP BY 	visitlink
	) temp2
	ON	(temp1.visitlink = temp2.visitlink_max AND temp1.daystoevent = temp2.date_max)
    OR	(temp1.visitlink IS NULL AND temp2.visitlink_max IS NULL AND temp1.daystoevent = temp2.date_max)
) temp3
ON		(hcup_pat_race.visitlink = temp3.visitlink OR hcup_pat_race.visitlink IS NULL AND temp3.visitlink IS NULL) -- in order to include the NULL visitlink
SET		race_2014_last = IF(race_2014_lastday IS NULL, temp3.race_last, IF(race_2014_lastday < temp3.daystoevent, temp3.race_last, race_2014_last)),
		race_2014_lastday = IF(race_2014_lastday IS NULL, temp3.daystoevent, IF(race_2014_lastday < temp3.daystoevent, temp3.daystoevent, race_2014_lastday))
; -- 220 sec

UPDATE 		hcup_pat_race -- need to convert internal coding of 1, 2, ... to W, B, ...
SET			race_2014_last = IF(race_2014_last IS NULL OR race_2014_last = '', 'N', IF(race_2014_last = '1', 'W', IF(race_2014_last = '2', 'B', IF(race_2014_last = '3', 'H', IF(race_2014_last = '4', 'A', IF(race_2014_last = '5', 'T', IF(race_2014_last = '6', 'O', 'N')))))))
; -- 90 sec


UPDATE 		hcup_pat_race
INNER JOIN	(
	SELECT 	visitlink, 
			IF(race_2014_last IS NOT NULL AND race_2014_last <> '' AND race_2014_last <> 'N', race_2014_last, race_2013_last) race_last
	FROM 	hcup_pat_race
) temp
ON		(hcup_pat_race.visitlink = temp.visitlink OR hcup_pat_race.visitlink IS NULL AND temp.visitlink IS NULL) -- in order to include the NULL visitlink
SET		hcup_pat_race.race_last = temp.race_last
; -- 448 sec

SELECT COUNT(*) FROM hcup.hcup_pat_race WHERE race_last IS NULL OR race_last = '' OR race_last = 'N'; -- 2 sec | 215,577 patients


SELECT COUNT(*) FROM hcup.hcup_pat_race WHERE race_last IS NULL OR race_last = ''; -- 0 patients



# fixing records where the last recorded race status has been null or empty (replacing them with the most frequent one) ... ideally should be replaced with the last one before the last one)
UPDATE 		hcup_pat_race
SET			race_last = IF(race_last IS NULL OR race_last = '' OR race_last = 'N', race_freq, race_last); -- 15,028 patients changed


UPDATE hcup_pat_race
SET 
  N_ct = race_2013_N_ct + race_2014_N_ct,
  W_ct = race_2013_W_ct + race_2014_W_ct,
  B_ct = race_2013_B_ct + race_2014_B_ct,
  A_ct = race_2013_A_ct + race_2014_A_ct,
  H_ct = race_2013_H_ct + race_2014_H_ct,
  T_ct = race_2013_T_ct + race_2014_T_ct,
  O_ct = race_2013_O_ct + race_2014_O_ct;

#patient is flagged if they have more than 2 reported races
UPDATE 		hcup_pat_race
SET			race_flag = IF((
    (W_ct > 0 AND A_ct > 0) OR
    (W_ct > 0 AND O_ct > 0) OR
    (W_ct > 0 AND B_ct > 0) OR
    (W_ct > 0 AND H_ct > 0) OR
    (W_ct > 0 AND T_ct > 0) OR
    
    (A_ct > 0 AND O_ct > 0) OR
    (A_ct > 0 AND B_ct > 0) OR
    (A_ct > 0 AND H_ct > 0) OR
    (A_ct > 0 AND T_ct > 0) OR
    
    (B_ct > 0 AND O_ct > 0) OR
    (B_ct > 0 AND H_ct > 0) OR
    (B_ct > 0 AND T_ct > 0) OR
    
    (H_ct > 0 AND O_ct > 0) OR 
    (H_ct > 0 AND T_ct > 0) OR
    
    (T_ct > 0 AND O_ct > 0)), 1, 0); -- 89 sec

SELECT 		COUNT(*) ct, race_flag 
FROM 		hcup_pat_race
GROUP BY	race_flag; -- 10 sec  (shows that around 60k of patients had an issue such as changing race status in these 2 years) (note that a number of them have a race of N [null])

/*
	+ ------- + -------------- +
	| ct      | race_flag      |
	+ ------- + -------------- +
	| 3153176 | 0              |
	| 110688  | 1              |
	+ ------- + -------------- +
*/

# race distribution
SELECT race_last, COUNT(visitlink) ct FROM hcup.hcup_pat_race GROUP BY race_last ORDER BY ct DESC; -- 11 sec

/*
	+ -------------- + ------- +
	| race_last      | ct      |
	+ -------------- + ------- +
	| W              | 1650551 |
	| B              | 1001974 |
	| N              | 200549  | -- NULLs (missing) around 200k
	| H              | 190348  |
	| O              | 126325  |
	| A              | 76996   |
	| T              | 17121   |
	+ -------------- + ------- +
*/

# age range of patients based on their race status
CREATE TABLE	hcup_pat_race_stat
SELECT			temp.age_range age_range, temp.race_last race_last, temp.race_ct race_ct, hcup_pat_age_stat.pat_ct pat_ct, ROUND((temp.race_ct/hcup_pat_age_stat.pat_ct)*100, 2) race_per
FROM
(	SELECT		IF(age_avg < 1, '0-1', IF(age_avg < 10, '1-10', IF(age_avg < 20, '10-20', IF(age_avg < 30, '20-30', IF(age_avg < 40, '30-40', IF(age_avg < 50, '40-50', IF(age_avg < 60, '50-60', IF(age_avg < 70, '60-70', IF(age_avg < 80, '70-80', IF(age_avg < 90, '80-90', '90+')))))))))) age_range,
				race_last,
                COUNT(*) race_ct
	FROM		hcup_pat_age, hcup_pat_race
	WHERE		hcup_pat_age.visitlink = hcup_pat_race.visitlink
	AND			age_flag = 0 
	-- AND 		race_flag = 0  -- optional as this might be simply a change in race status
	GROUP BY	age_range, race_last
    ) temp,
hcup_pat_age_stat
WHERE	hcup_pat_age_stat.age_range = temp.age_range; -- 288 sec

SELECT * FROM hcup.hcup_pat_race_stat ORDER BY age_range, race_per DESC; -- look at distribution of W/B/H among age groups


CREATE TABLE `hcup_chgs` 
(
  `visitlink` varchar(16) DEFAULT NULL,
  #completeness measures
  `2013_sedd_chg` int(16) NOT NULL DEFAULT 0,
  `2013_sedd_visits` int(16) NOT NULL DEFAULT 0,
  `2013_sasd_chg` int(16) NOT NULL DEFAULT 0,
  `2013_sasd_visits` int(16) NOT NULL DEFAULT 0,
  `2013_sid_chg` int(16) NOT NULL DEFAULT 0,
  `2013_sid_visits` int(16) NOT NULL DEFAULT 0,

  `2014_sedd_chg` int(16) NOT NULL DEFAULT 0,
  `2014_sedd_visits` int(16) NOT NULL DEFAULT 0,
  `2014_sasd_chg` int(16) NOT NULL DEFAULT 0,
  `2014_sasd_visits` int(16) NOT NULL DEFAULT 0,
  `2014_sid_chg` int(16) NOT NULL DEFAULT 0,
  `2014_sid_visits` int(16) NOT NULL DEFAULT 0
  
  UNIQUE KEY `visitlink_UNIQUE` (`visitlink`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#inserting visitlinks
INSERT INTO hcup_chgs (visitlink)
SELECT    visitlink 
FROM    hcup_pat_visitlink_uni; 

SET SQL_SAFE_UPDATES = 0;

#Total charges for 2013 ED
UPDATE hcup_chgs
INNER JOIN (  
  SELECT visitlink, IFNULL(SUM(totchg),0) as chg
  FROM 2013_sedd_core 
  GROUP BY visitlink) t
ON hcup_chgs.visitlink = t.visitlink
OR hcup_chgs.visitlink IS NULL AND t.visitlink IS NULL
SET 2013_sedd_chg = t.chg;

#Total charges for 2013 Ambulatory services
UPDATE hcup_chgs
INNER JOIN (  
  SELECT visitlink, IFNULL(SUM(totchg),0) as chg
  FROM 2013_sasd_core 
  GROUP BY visitlink) t
ON hcup_chgs.visitlink = t.visitlink
OR hcup_chgs.visitlink IS NULL AND t.visitlink IS NULL
SET 2013_sasd_chg = t.chg;

#Total Charges for 2013 inpatient services
UPDATE hcup_chgs
INNER JOIN (  
  SELECT visitlink, IFNULL(SUM(totchg),0) as chg
  FROM 2013_sid_core 
  GROUP BY visitlink) t
ON hcup_chgs.visitlink = t.visitlink
OR hcup_chgs.visitlink IS NULL AND t.visitlink IS NULL
SET 2013_sid_chg = t.chg;

#Total charges for 2014 ED
UPDATE hcup_chgs
INNER JOIN (  
  SELECT visitlink, IFNULL(SUM(totchg),0) as chg
  FROM 2014_sedd_core 
  GROUP BY visitlink) t
ON hcup_chgs.visitlink = t.visitlink
OR hcup_chgs.visitlink IS NULL AND t.visitlink IS NULL
SET 2014_sedd_chg = t.chg;

#Total charges for 2014 ambulatory services
UPDATE hcup_chgs
INNER JOIN (  
  SELECT visitlink, IFNULL(SUM(totchg),0) as chg
  FROM 2014_sasd_core 
  GROUP BY visitlink) t
ON hcup_chgs.visitlink = t.visitlink
OR hcup_chgs.visitlink IS NULL AND t.visitlink IS NULL
SET 2014_sasd_chg = t.chg;

#Total charges for inpatient services 2014
UPDATE hcup_chgs
INNER JOIN (  
  SELECT visitlink, IFNULL(SUM(totchg),0) as chg
  FROM 2014_sid_core 
  GROUP BY visitlink) t
ON hcup_chgs.visitlink = t.visitlink
OR hcup_chgs.visitlink IS NULL AND t.visitlink IS NULL
SET 2014_sid_chg = t.chg;

#Total visits for ambulatory patients in 2013
UPDATE hcup_chgs
INNER JOIN (
  SELECT visitlink, IFNULL(sasd_2013_ct,0) AS ct
  FROM hcup_pat_visitlink_uni
  GROUP BY visitlink) t
ON hcup_chgs.visitlink = t.visitlink
OR hcup_chgs.visitlink IS NULL AND t.visitlink IS NULL
SET 2013_sasd_visits = t.ct;

#Total visits for emergency patients in 2013
UPDATE hcup_chgs
INNER JOIN (
  SELECT visitlink, IFNULL(sedd_2013_ct,0) AS ct
  FROM hcup_pat_visitlink_uni
  GROUP BY visitlink) t
ON hcup_chgs.visitlink = t.visitlink
OR hcup_chgs.visitlink IS NULL AND t.visitlink IS NULL
SET 2013_sedd_visits = t.ct;

#Total visits for inpatients in 2013
UPDATE hcup_chgs
INNER JOIN (
  SELECT visitlink, IFNULL(sid_2013_ct,0) AS ct
  FROM hcup_pat_visitlink_uni
  GROUP BY visitlink) t
ON hcup_chgs.visitlink = t.visitlink
OR hcup_chgs.visitlink IS NULL AND t.visitlink IS NULL
SET 2013_sid_visits = t.ct;

#Total visits for ambulatory services in 2014
UPDATE hcup_chgs
INNER JOIN (
  SELECT visitlink, IFNULL(sasd_2014_ct,0) AS ct
  FROM hcup_pat_visitlink_uni
  GROUP BY visitlink) t
ON hcup_chgs.visitlink = t.visitlink
OR hcup_chgs.visitlink IS NULL AND t.visitlink IS NULL
SET 2014_sasd_visits = t.ct;

#Total visits for emergency services in 2014
UPDATE hcup_chgs
INNER JOIN (
  SELECT visitlink, IFNULL(sedd_2014_ct,0) AS ct
  FROM hcup_pat_visitlink_uni
  GROUP BY visitlink) t
ON hcup_chgs.visitlink = t.visitlink
OR hcup_chgs.visitlink IS NULL AND t.visitlink IS NULL
SET 2014_sedd_visits = t.ct;

#Total visits for inpatients in 2014
UPDATE hcup_chgs
INNER JOIN (
  SELECT visitlink, IFNULL(sid_2014_ct,0) AS ct
  FROM hcup_pat_visitlink_uni
  GROUP BY visitlink) t
ON hcup_chgs.visitlink = t.visitlink
OR hcup_chgs.visitlink IS NULL AND t.visitlink IS NULL
SET 2014_sid_visits = t.ct;

#------------Total population---------------------------------------------------------------
#Average cost / patient 2013 emergency services
SELECT AVG(2013_sedd_chg), STD(2013_sedd_chg) FROM hcup_chgs WHERE visitlink IS NOT NULL;
#Average total cost / patient 2013: 567.9509
#STD total cost / patient 2013: 1745.455016362989
SELECT AVG(totchg), STD(totchg) FROM 2013_sedd_core WHERE visitlink IS NOT NULL;
#Average chg / visit 2013: 885.3710419039841
#STD cost / visit 2013: 1360.314110322438
SELECT AVG(2013_sedd_chg / 2013_sedd_visits), STD(2013_sedd_chg / 2013_sedd_visits) FROM hcup_chgs WHERE 2013_sedd_visits > 0 AND visitlink IS NOT NULL;
#Average cost per visit / patient 2013: 847.36894026
#STD cost per visit / patietn 2013: 1305.3960864790768

#Average cost / patient 2013 inpatient services
SELECT AVG(2013_sid_chg), STD(2013_sid_chg) FROM hcup_chgs WHERE visitlink IS NOT NULL;
#Average total cost / patient 2013 inpatient: 2814.1640
#STD total cost / patient 2013 inpatient: 15779.061819036444
SELECT AVG(totchg), STD(totchg) FROM 2013_sid_core WHERE visitlink IS NOT NULL;
#Average cost / visit 2013 inpatient: 13823.43232875063
#STD cost / visit 2013 inpatient: 24452.455244684726
SELECT AVG(2013_sid_chg / 2013_sid_visits), STD(2013_sid_chg / 2013_sid_visits) FROM hcup_chgs WHERE 2013_sid_visits > 0 AND visitlink IS NOT NULL;
#Average cost per visit / patient 2013 inpatient: 12576.60080617
#STD cost per visit / patietn 2013 inpatient: 19835.49679779506

#Average cost / patient 2013 ambulatory services
SELECT AVG(2013_sasd_chg), STD(2013_sasd_chg) FROM hcup_chgs WHERE visitlink IS NOT NULL;
#Average total cost / patient 2013 ambulatory: 1301.4541
#STD total cost / patient 2013 ambulatory: 6649.57493511208
SELECT AVG(totchg), STD(totchg) FROM 2013_sasd_core WHERE visitlink IS NOT NULL;
#AVERAGE cost / visit 2013 ambulatory: 1526.48205747043
#STD cost / visit 2013 ambulatory: 3506.1270069356274
SELECT AVG(2013_sasd_chg / 2013_sasd_visits), STD(2013_sasd_chg / 2013_sasd_visits) FROM hcup_chgs WHERE 2013_sasd_visits > 0 AND visitlink IS NOT NULL;
#Average cost per visit / patient 2013 ambulatory: 1460.75228416
#STD cost per visit / patietn 2013 ambulatory: 2808.5798920167845

#Average cost / patient 2014 emergency department
SELECT AVG(2014_sedd_chg), STD(2014_sedd_chg) FROM hcup_chgs WHERE visitlink IS NOT NULL;
#Average total cost / patient 2014 ED: 613.7340
#STD total cost / patient 2014 ED: 1826.7842961208278
SELECT AVG(totchg), STD(totchg) FROM 2014_sedd_core WHERE visitlink IS NOT NULL;
#Average chg / visit in 2014 ED: 941.39229072685
#STD chg / patient visit 2014 ED: 1365.2101373483915
SELECT AVG(2014_sedd_chg / 2014_sedd_visits), STD(2014_sedd_chg / 2014_sedd_visits) FROM hcup_chgs WHERE 2014_sedd_visits > 0 AND visitlink IS NOT NULL;
#Average cost per visit / patient 2014 ED: 902.16291108
#STD cost per visit / patietn 2014 ED: 1252.5992200285816

#Average cost / patient 2014 inpatient services
SELECT AVG(2014_sid_chg), STD(2014_sid_chg) FROM hcup_chgs WHERE visitlink IS NOT NULL;
#Average total cost / patient 2014 inpatient: 2797.7847
#STD total cost / patient 2014 inpatient: 15810.721150750749
SELECT AVG(totchg), STD(totchg) FROM 2014_sid_core WHERE visitlink IS NOT NULL;
#Average cost / visit 2014 inpatient: 14225.56418
#STD cost / visit 2014 inpatient: 24589.066103308847
SELECT AVG(2014_sid_chg / 2014_sid_visits), STD(2014_sid_chg / 2014_sid_visits) FROM hcup_chgs WHERE 2014_sid_visits > 0 AND visitlink IS NOT NULL;
#Average cost per visit / patient 2014 inaptient: 12933.44205536
#STD cost per visit / patietn 2014 inpatient: 19937.655442773957

#Average cost / patient 2014 ambulatory services
SELECT AVG(2014_sasd_chg), STD(2014_sasd_chg) FROM hcup_chgs WHERE visitlink IS NOT NULL;
#Average total cost / patient 2014 ambulatory: 1363.1508
#STD total cost / patient 2014 ambulatory: 6931.602968472699
SELECT AVG(totchg), STD(totchg) FROM 2014_sasd_core WHERE visitlink IS NOT NULL;
#Avg cost / visit ambulatory 2014: 1589.610026771356
#STD cost / visit ambulatory 2014: 3745.6269489030656
SELECT AVG(2014_sasd_chg / 2014_sasd_visits), STD(2014_sasd_chg / 2014_sasd_visits) FROM hcup_chgs WHERE 2014_sasd_visits > 0 AND visitlink IS NOT NULL;
#Average cost per visit / patient 2014 ambulatory: 1520.67674710
#STD cost per visit / patient 2014 ambulatory: 3021.2686439315044

#Avg Cost / patient 2013 - 2014 inpatient services
SELECT AVG(IFNULL(2014_sid_chg, 0) + IFNULL(2013_sid_chg, 0)) as Average,
  STD(IFNULL(2014_sid_chg, 0) + IFNULL(2013_sid_chg, 0)) as stdev      
FROM hcup_chgs WHERE visitlink IS NOT NULL;
#Avg total cost / patient 2013 - 2014 inpatient: 5611.9488
#STD: 24125.615279793612
SELECT AVG(t.totchg), STD(t.totchg)
FROM ((SELECT totchg FROM 2013_sid_core WHERE visitlink IS NOT NULL) 
UNION ALL (SELECT totchg FROM 2014_sid_core WHERE visitlink IS NOT NULL)) t
#Avg cost / visit: 14021.02892444631 
#STD cost / visit: 24520.501326760757

#Avg Cost / patient 2013 - 2014 ambulatory services
SELECT AVG(IFNULL(2014_sasd_chg, 0) + IFNULL(2013_sasd_chg, 0)) as Average,
  STD(IFNULL(2014_sasd_chg, 0) + IFNULL(2013_sasd_chg, 0)) as stdev      
FROM hcup_chgs WHERE visitlink IS NOT NULL;
#Avg total cost / patient 2013 - 2014 ambulatory: 2664.6049
#STD: 11422.384708006508
SELECT AVG(t.totchg), STD(t.totchg)
FROM ((SELECT totchg FROM 2013_sasd_core WHERE visitlink IS NOT NULL) 
UNION ALL (SELECT totchg FROM 2014_sasd_core WHERE visitlink IS NOT NULL)) t;
#Avg cost / visit: 1558.1374778401553
#STD cost / visit: 3628.3378835005674

#Avg Cost / patient 2013 - 2014 emergency 
SELECT AVG(IFNULL(2014_sedd_chg, 0) + IFNULL(2013_sedd_chg, 0)) as Average,
  STD(IFNULL(2014_sedd_chg, 0) + IFNULL(2013_sedd_chg, 0)) as stdev      
FROM hcup_chgs WHERE visitlink IS NOT NULL;
#Avg total cost / patient 2013 - 2014 emergency: 1181.6850
#STD: 2879.8476075255776
SELECT AVG(t.totchg), STD(t.totchg)
FROM ((SELECT totchg FROM 2013_sedd_core WHERE visitlink IS NOT NULL) 
UNION ALL (SELECT totchg FROM 2014_sedd_core WHERE visitlink IS NOT NULL)) t
#Avg cost / visit: 913.6081761563476
#STD cost / visit: 1363.0719340015926

#Avg combined cost / patient 2013
SELECT AVG(IFNULL(2013_sid_chg, 0) + IFNULL(2013_sedd_chg, 0) + IFNULL(2013_sasd_chg, 0)) as Average,
  STD(IFNULL(2013_sid_chg, 0) + IFNULL(2013_sedd_chg, 0) + IFNULL(2013_sasd_chg,0)) as stdev      
FROM hcup_chgs WHERE visitlink IS NOT NULL;
#Avg total cost / patient 2013 combined: 4683.5691
#STD total cost / patient 2013 combined: 18179.06937941893
SELECT AVG(t.totchg), STD(t.totchg)
FROM ((SELECT totchg FROM 2013_sedd_core WHERE visitlink IS NOT NULL)
UNION ALL (SELECT totchg FROM 2013_sasd_core WHERE visitlink IS NOT NULL)
UNION ALL (SELECT totchg FROM 2013_sid_core WHERE visitlink IS NOT NULL)) t;
#Avg cost / visit 2013 combined: 2758.859673214835
#Avg cost / visit 2013 combined: 9764.472881595844

#Avg combined cost / patient 2014
SELECT AVG(IFNULL(2014_sid_chg, 0) + IFNULL(2014_sedd_chg, 0) + IFNULL(2014_sasd_chg, 0)) as Average,
  STD(IFNULL(2014_sid_chg, 0) + IFNULL(2014_sedd_chg, 0) + IFNULL(2014_sasd_chg,0)) as stdev      
FROM hcup_chgs WHERE visitlink IS NOT NULL;
#Avg total cost / patient 2014 combined: 4774.6695
#STD total cost / patient 2014 combined: 18394.116825207395
SELECT AVG(t.totchg), STD(t.totchg)
FROM ((SELECT totchg FROM 2014_sedd_core WHERE visitlink IS NOT NULL)
UNION ALL (SELECT totchg FROM 2014_sasd_core WHERE visitlink IS NOT NULL)
UNION ALL (SELECT totchg FROM 2014_sid_core WHERE visitlink IS NOT NULL)) t;
#Avg cost / visit 2014 combined: 2798.498893269361
#Avg cost / visit 2014 combined: 9724.433950851691

#Avg combined cost / patient 2013 - 2014
SELECT AVG(IFNULL(2014_sid_chg, 0) + IFNULL(2014_sedd_chg, 0) + IFNULL(2014_sasd_chg, 0) + IFNULL(2013_sid_chg, 0) + IFNULL(2013_sedd_chg, 0) + IFNULL(2013_sasd_chg, 0)) as Average,
  STD(IFNULL(2014_sid_chg, 0) + IFNULL(2014_sedd_chg, 0) + IFNULL(2014_sasd_chg,0) + IFNULL(2013_sid_chg, 0) + IFNULL(2013_sedd_chg, 0) + IFNULL(2013_sasd_chg,0)) as stdev      
FROM hcup_chgs WHERE visitlink IS NOT NULL;
#Avg total cost / patient 2013 - 2014 combined: 9458.2386
#STD total cost / patient 2013 - 2014 combined: 28799.128865012408
SELECT AVG(t.totchg), STD(t.totchg)
FROM ((SELECT totchg FROM 2014_sedd_core WHERE visitlink IS NOT NULL)
UNION ALL (SELECT totchg FROM 2014_sasd_core WHERE visitlink IS NOT NULL)
UNION ALL (SELECT totchg FROM 2014_sid_core WHERE visitlink IS NOT NULL)
UNION ALL (SELECT totchg FROM 2013_sedd_core WHERE visitlink IS NOT NULL)
UNION ALL (SELECT totchg FROM 2013_sasd_core WHERE visitlink IS NOT NULL)
UNION ALL (SELECT totchg FROM 2013_sid_core WHERE visitlink IS NOT NULL)) t;
#Avg cost / visit 2013 - 2014 combined: 2778.7288182537136
#Avg cost / visit 2013 - 2014 combined: 9744.444101977117

ALTER TABLE hcup_chgs
ADD COLUMN dq_flag int(1) NOT NULL DEFAULT 0 AFTER visitlink;
UPDATE hcup_chgs
INNER JOIN (
  SELECT visitlink
  FROM hcup_pat_pop_spec_flagged
  GROUP BY visitlink) t
ON hcup_chgs.visitlink = t.visitlink
OR hcup_chgs.visitlink IS NULL AND t.visitlink IS NULL
SET dq_flag = 1;

#------------FLAGGED POPULATION----------------------------------------------------------
#Average cost / patient 2013 emergency services
SELECT AVG(2013_sedd_chg), STD(2013_sedd_chg) FROM hcup_chgs WHERE visitlink is NOT null and dq_flag = 1;
#Average total cost / patient 2013: 580.7637
#STD total cost / patient 2013: 2090.0328432943975
SELECT AVG(totchg), STD(totchg) FROM 2013_sedd_core 
WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL)
#Average cost / visit 2013: 897.9465905375154
#STD cost / visit 2013: 1259.6955521636544
SELECT AVG(2013_sedd_chg / 2013_sedd_visits), STD(2013_sedd_chg / 2013_sedd_visits) FROM hcup_chgs WHERE 2013_sedd_visits > 0 AND visitlink IS NOT NULL and dq_flag = 1;
#Average cost per visit / patient 2013: 845.69369604
#STD cost per visit / patietn 2013: 1135.9075994267075

#Average cost / patient 2013 inpatient services
SELECT AVG(2013_sid_chg), STD(2013_sid_chg) FROM hcup_chgs WHERE visitlink IS NOT NULL and dq_flag = 1;
#Average total cost / patient 2013 inpatient: 3041.8306
#STD total cost / patient 2013 inpatient: 18863.159537857184
SELECT AVG(totchg), STD(totchg) FROM 2013_sid_core 
WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL)
#Average cost / visit 2013 inpatient: 13894.770929627703
#STD cost / visit 2013 inpatient: 28467.652595251548
SELECT AVG(2013_sid_chg / 2013_sid_visits), STD(2013_sid_chg / 2013_sid_visits) FROM hcup_chgs WHERE 2013_sid_visits > 0 AND visitlink IS NOT NULL and dq_flag = 1;
#Average cost per visit / patient 2013 inpatient: 12346.95164990
#STD cost per visit / patietn 2013 inpatient: 21082.007155325042

#Average cost / patient 2013 ambulatory services
SELECT AVG(2013_sasd_chg), STD(2013_sasd_chg) FROM hcup_chgs WHERE visitlink IS NOT NULL and dq_flag = 1;
#Average total cost / patient 2013 ambulatory: 1238.9792
#STD total cost / patient 2013 ambulatory: 6312.277895633319
SELECT AVG(totchg), STD(totchg) FROM 2013_sasd_core 
WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL)
#Average cost / visit 2013 ambulatory: 1382.7064665236037
#STD cost / visit 2013 ambulatory: 3385.0362473789874
SELECT AVG(2013_sasd_chg / 2013_sasd_visits), STD(2013_sasd_chg / 2013_sasd_visits) FROM hcup_chgs WHERE 2013_sasd_visits > 0 AND visitlink IS NOT NULL and dq_flag = 1;
#Average cost per visit / patient 2013 ambulatory: 1283.15815700
#STD cost per visit / patietn 2013 ambulatory: 2705.1476545243763

#Average cost / patient 2014 emergency department
SELECT AVG(2014_sedd_chg), STD(2014_sedd_chg) FROM hcup_chgs WHERE visitlink IS NOT NULL and dq_flag = 1;
#Average total cost / patient 2014 ED: 710.0211
#STD total cost / patient 2014 ED: 2215.038063917164
SELECT AVG(totchg), STD(totchg) FROM 2014_sedd_core 
WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL)
#Average cost / visit 2014 ED: 932.9871654553228
#STD cost / visit 2014 ED: 1367.474644421541
SELECT AVG(2014_sedd_chg / 2014_sedd_visits), STD(2014_sedd_chg / 2014_sedd_visits) FROM hcup_chgs WHERE 2014_sedd_visits > 0 AND visitlink IS NOT NULL and dq_flag = 1;
#Average cost per visit / patient 2014 ED: 876.12654193
#STD cost per visit / patietn 2014 ED: 1260.9236676873095

#Average cost / patient 2014 inpatient services
SELECT AVG(2014_sid_chg), STD(2014_sid_chg) FROM hcup_chgs WHERE visitlink IS NOT NULL and dq_flag = 1;
#Average total cost / patient 2014 inpatient: 3511.9364
#STD total cost / patient 2014 inpatient: 19049.296968894283
SELECT AVG(totchg), STD(totchg) FROM 2014_sid_core 
WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL)
#Average cost / visit 2014 inpatient: 14612.513686576054
#STD cost / visit 2014 inpatient: 26711.3552628656
SELECT AVG(2014_sid_chg / 2014_sid_visits), STD(2014_sid_chg / 2014_sid_visits) FROM hcup_chgs WHERE 2014_sid_visits > 0 AND visitlink IS NOT NULL and dq_flag = 1;
#Average cost per visit / patient 2014 inpatient: 13247.79197348
#STD cost per visit / patietn 2014 inpatient: 21654.739904865473

#Average cost / patient 2014 ambulatory services
SELECT AVG(2014_sasd_chg), STD(2014_sasd_chg) FROM hcup_chgs WHERE visitlink IS NOT NULL and dq_flag = 1;
#Average total cost / patient 2014 ambulatory: 1522.8671
#STD total cost / patient 2014 ambulatory: 7202.64842863386
SELECT AVG(totchg), STD(totchg) FROM 2014_sasd_core 
WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL)
#Average cost / visit 2014 ambulatory: 1481.0170866610133
#STD cost / visit 2014 ambulatory:  3992.9482194287284
SELECT AVG(2014_sasd_chg / 2014_sasd_visits), STD(2014_sasd_chg / 2014_sasd_visits) FROM hcup_chgs WHERE 2014_sasd_visits > 0 AND visitlink IS NOT NULL and dq_flag = 1;
#Average cost per visit / patient 2014 ambulatory: 1462.11358322
#STD cost per visit / patient 2014 ambulatory: 3204.0302205269495

#Avg Cost / patient 2013 - 2014 inpatient services
SELECT AVG(IFNULL(2014_sid_chg, 0) + IFNULL(2013_sid_chg, 0)) as Average,
  STD(IFNULL(2014_sid_chg, 0) + IFNULL(2013_sid_chg, 0)) as stdev      
FROM hcup_chgs WHERE visitlink IS NOT NULL AND dq_flag = 1;
#Avg total cost / patient 2013 - 2014 inpatient: 6553.7669
#STD: 29027.99091534098
SELECT AVG(t.totchg), STD(t.totchg)
FROM ((SELECT totchg FROM 2013_sid_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL)) 
UNION ALL (SELECT totchg FROM 2014_sid_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL))) t
#Avg cost / visit: 14270.379113444276
#STD cost / visit: 27564.842665371987

#Avg Cost / patient 2013 - 2014 ambulatory services
SELECT AVG(IFNULL(2014_sasd_chg, 0) + IFNULL(2013_sasd_chg, 0)) as Average,
  STD(IFNULL(2014_sasd_chg, 0) + IFNULL(2013_sasd_chg, 0)) as stdev      
FROM hcup_chgs WHERE visitlink IS NOT NULL AND dq_flag = 1;
#Avg total cost / patient 2013 - 2014 inpatient: 2761.8463
#STD: 11313.491457555727
SELECT AVG(t.totchg), STD(t.totchg)
FROM ((SELECT totchg FROM 2013_sasd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL)) 
UNION ALL (SELECT totchg FROM 2014_sasd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL))) t
#Avg cost / visit: 1435.238841199177
#STD cost / visit: 3722.570016194676

#Avg Cost / patient 2013 - 2014 emergency services
SELECT AVG(IFNULL(2014_sedd_chg, 0) + IFNULL(2013_sedd_chg, 0)) as Average,
  STD(IFNULL(2014_sedd_chg, 0) + IFNULL(2013_sedd_chg, 0)) as stdev      
FROM hcup_chgs WHERE visitlink IS NOT NULL AND dq_flag = 1;
#Avg total cost / patient 2013 - 2014 inpatient: 1290.7848
#STD: 3606.2439431073626
SELECT AVG(t.totchg), STD(t.totchg)
FROM ((SELECT totchg FROM 2013_sedd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL)) 
UNION ALL (SELECT totchg FROM 2014_sedd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL))) t
#Avg cost / visit: 916.888755098975
#STD cost / visit: 1319.1681702650121

#Avg combined cost / patient 2013
SELECT AVG(IFNULL(2013_sid_chg, 0) + IFNULL(2013_sedd_chg, 0) + IFNULL(2013_sasd_chg, 0)) as Average,
  STD(IFNULL(2013_sid_chg, 0) + IFNULL(2013_sedd_chg, 0) + IFNULL(2013_sasd_chg,0)) as stdev      
FROM hcup_chgs WHERE visitlink IS NOT NULL and dq_flag = 1;
#Avg total cost / patient 2013 combined: 4861.5735
#STD total cost / patient 2013 combined: 21159.23491480663
SELECT AVG(t.totchg), STD(t.totchg)
FROM ((SELECT totchg FROM 2013_sedd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL))
UNION ALL (SELECT totchg FROM 2013_sasd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL))
UNION ALL (SELECT totchg FROM 2013_sid_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL))) t;
#Avg cost / visit 2013 combined: 2759.527222964026
#Avg cost / visit 2013 combined: 11169.525393839933

#Avg combined cost / patient 2014
SELECT AVG(IFNULL(2014_sid_chg, 0) + IFNULL(2014_sedd_chg, 0) + IFNULL(2014_sasd_chg, 0)) as Average,
  STD(IFNULL(2014_sid_chg, 0) + IFNULL(2014_sedd_chg, 0) + IFNULL(2014_sasd_chg,0)) as stdev      
FROM hcup_chgs WHERE visitlink IS NOT NULL and dq_flag = 1;
#Avg total cost / patient 2014 combined: 5744.8246
#STD total cost / patient 2014 combined: 21635.92726307274
SELECT AVG(t.totchg), STD(t.totchg)
FROM ((SELECT totchg FROM 2014_sedd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL))
UNION ALL (SELECT totchg FROM 2014_sasd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL))
UNION ALL (SELECT totchg FROM 2014_sid_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL))) t;
#Avg cost / visit 2014 combined: 2830.5005658828736
#Avg cost / visit 2014 combined: 10581.976477733937

#Avg combined cost / patient 2013 - 2014
SELECT AVG(IFNULL(2013_sid_chg, 0) + IFNULL(2013_sedd_chg, 0) + IFNULL(2013_sasd_chg, 0) + IFNULL(2014_sid_chg, 0) + IFNULL(2014_sedd_chg, 0) + IFNULL(2014_sasd_chg, 0)) as Average,
  STD(IFNULL(2013_sid_chg, 0) + IFNULL(2013_sedd_chg, 0) + IFNULL(2013_sasd_chg, 0) + IFNULL(2014_sid_chg, 0) + IFNULL(2014_sedd_chg, 0) + IFNULL(2014_sasd_chg,0)) as stdev      
FROM hcup_chgs WHERE visitlink IS NOT NULL and dq_flag = 1;
#Avg total cost / patient 2013 - 2014 combined: 10606.3981
#STD total cost / patient 2013 - 2014 combined: 33682.771420861005
SELECT AVG(t.totchg), STD(t.totchg)
FROM ((SELECT totchg FROM 2014_sedd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL))
UNION ALL (SELECT totchg FROM 2014_sasd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL))
UNION ALL (SELECT totchg FROM 2014_sid_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL))
UNION ALL (SELECT totchg FROM 2013_sasd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL))
UNION ALL (SELECT totchg FROM 2013_sid_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL))
UNION ALL (SELECT totchg FROM 2013_sedd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_flagged WHERE visitlink IS NOT NULL))) t;
#Avg cost / visit 2013 - 2014 combined: 2797.5211550558365
#Avg cost / visit 2013 - 2014 combined: 10859.00713348035

#------------------Remaining population----------------
#Average cost / patient 2013 emergency services
SELECT AVG(2013_sedd_chg), STD(2013_sedd_chg) FROM hcup_chgs WHERE visitlink is NOT null and dq_flag = 0;
#Average total cost / patient 2013: 566.6190
#STD total cost / patient 2013: 1705.6382829046202
SELECT AVG(totchg), STD(totchg) FROM 2013_sedd_core 
WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL)
#Avg cost / visit 2013 ED: 884.0518214393144
#STD cost / visit 2013 ED: 1370.4346046953099
SELECT AVG(2013_sedd_chg / 2013_sedd_visits), STD(2013_sedd_chg / 2013_sedd_visits) FROM hcup_chgs WHERE 2013_sedd_visits > 0 AND visitlink IS NOT NULL and dq_flag = 0;
#Average cost per visit / patient 2013: 847.51587784
#STD cost per visit / patietn 2013: 1319.223697209965

#Average cost / patient 2013 inpatient services
SELECT AVG(2013_sid_chg), STD(2013_sid_chg) FROM hcup_chgs WHERE visitlink IS NOT NULL and dq_flag = 0;
#Average total cost / patient 2013 inpatient: 2790.4965
#STD total cost / patient 2013 inpatient: 15422.906660254988
SELECT AVG(totchg), STD(totchg) FROM 2013_sid_core 
WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL)
#Avg cost / visit 2013 inpatient: 13815.39439
#STD cost / visit 2013 inpatient: 23957.892655634078
SELECT AVG(2013_sid_chg / 2013_sid_visits), STD(2013_sid_chg / 2013_sid_visits) FROM hcup_chgs WHERE 2013_sid_visits > 0 AND visitlink IS NOT NULL and dq_flag = 0;
#Average cost per visit / patient 2013 inpatient: 12600.23114421
#STD cost per visit / patietn 2013 inpatient: 19702.607731561064

#Average cost / patient 2013 ambulatory services
SELECT AVG(2013_sasd_chg), STD(2013_sasd_chg) FROM hcup_chgs WHERE visitlink IS NOT NULL and dq_flag = 0;
#Average total cost / patient 2013 ambulatory: 1307.9488
#STD total cost / patient 2013 ambulatory: 6683.629214136353
SELECT AVG(totchg), STD(totchg) FROM 2013_sasd_core 
WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL)
#Avg cost / visit 2013 ambulatory: 1542.2743408785893
#STD cost / visit 2013 ambulatory: 3518.8155899761546
SELECT AVG(2013_sasd_chg / 2013_sasd_visits), STD(2013_sasd_chg / 2013_sasd_visits) FROM hcup_chgs WHERE 2013_sasd_visits > 0 AND visitlink IS NOT NULL and dq_flag = 0;
#Average cost per visit / patient 2013 ambulatory: 1479.98802429
#STD cost per visit / patietn 2013 ambulatory: 2818.883715131215

#Average cost / patient 2014 emergency department
SELECT AVG(2014_sedd_chg), STD(2014_sedd_chg) FROM hcup_chgs WHERE visitlink IS NOT NULL and dq_flag = 0;
#Average total cost / patient 2014 ED: 603.7243
#STD total cost / patient 2014 ED: 1781.2752843834223
SELECT AVG(totchg), STD(totchg) FROM 2014_sedd_core 
WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL)
#Avg cost / visit 2014 ED: 942.4303095166564
#STD cost / visit 2014 ED: 1364.9266231826568
SELECT AVG(2014_sedd_chg / 2014_sedd_visits), STD(2014_sedd_chg / 2014_sedd_visits) FROM hcup_chgs WHERE 2014_sedd_visits > 0 AND visitlink IS NOT NULL and dq_flag = 0;
#Average cost per visit / patient 2014 ED: 905.07205012
#STD cost per visit / patietn 2014 ED: 1251.6320221613148

#Average cost / patient 2014 inpatient services
SELECT AVG(2014_sid_chg), STD(2014_sid_chg) FROM hcup_chgs WHERE visitlink IS NOT NULL and dq_flag = 0;
#Average total cost / patient 2014 inpatient: 2723.5435
#STD total cost / patient 2014 inpatient: 15433.208624991405
SELECT AVG(totchg), STD(totchg) FROM 2014_sid_core 
WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL)
#Avg cost / visit 2014 inpatient: 14175.245747703531
#STD cost / visit 2014 inpatient: 24299.01821226819
SELECT AVG(2014_sid_chg / 2014_sid_visits), STD(2014_sid_chg / 2014_sid_visits) FROM hcup_chgs WHERE 2014_sid_visits > 0 AND visitlink IS NOT NULL and dq_flag = 0;
#Average cost per visit / patient 2014 inaptient: 12894.26154906
#STD cost per visit / patietn 2014 inpatient: 19712.807303097154

#Average cost / patient 2014 ambulatory services
SELECT AVG(2014_sasd_chg), STD(2014_sasd_chg) FROM hcup_chgs WHERE visitlink IS NOT NULL and dq_flag = 0;
#Average total cost / patient 2014 ambulatory: 1346.5471
#STD total cost / patient 2014 ambulatory: 6902.603082548056
SELECT AVG(totchg), STD(totchg) FROM 2014_sasd_core 
WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL)
#Avg cost / visit 2014 ambulatory: 1603.4325551553436
#STD cost / visit 2014 ambulatory: 3712.7364180932977
SELECT AVG(2014_sasd_chg / 2014_sasd_visits), STD(2014_sasd_chg / 2014_sasd_visits) FROM hcup_chgs WHERE 2014_sasd_visits > 0 AND visitlink IS NOT NULL and dq_flag = 0;
#Average cost per visit / patient 2014 ambulatory: 1527.86290083
#STD cost per visit / patient 2014 ambulatory: 2997.996257250748



#Avg Cost / patient 2013 - 2014 inpatient services
SELECT AVG(IFNULL(2014_sid_chg, 0) + IFNULL(2013_sid_chg, 0)) as Average,
  STD(IFNULL(2014_sid_chg, 0) + IFNULL(2013_sid_chg, 0)) as stdev      
FROM hcup_chgs WHERE visitlink IS NOT NULL AND dq_flag = 0;
#Avg total cost / patient 2013 - 2014 inpatient: 5514.0399
#STD: 23555.34844381814
SELECT AVG(t.totchg), STD(t.totchg)
FROM ((SELECT totchg FROM 2013_sid_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL)) 
UNION ALL (SELECT totchg FROM 2014_sid_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL))) t
#Avg cost / visit: 13990.822913519896
#STD cost / visit: 24125.465398703574

#Avg Cost / patient 2013 - 2014 ambulatory services
SELECT AVG(IFNULL(2014_sasd_chg, 0) + IFNULL(2013_sasd_chg, 0)) as Average,
  STD(IFNULL(2014_sasd_chg, 0) + IFNULL(2013_sasd_chg, 0)) as stdev      
FROM hcup_chgs WHERE visitlink IS NOT NULL AND dq_flag = 0;
#Avg total cost / patient 2013 - 2014 inpatient: 2654.4959
#STD: 11433.597985330573
SELECT AVG(t.totchg), STD(t.totchg)
FROM ((SELECT totchg FROM 2013_sasd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL)) 
UNION ALL (SELECT totchg FROM 2014_sasd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL))) t
#Avg cost / visit: 1572.7035357140423
#STD cost / visit: 3616.7298801895704

#Avg Cost / patient 2013 - 2014 emergency services
SELECT AVG(IFNULL(2014_sedd_chg, 0) + IFNULL(2013_sedd_chg, 0)) as Average,
  STD(IFNULL(2014_sedd_chg, 0) + IFNULL(2013_sedd_chg, 0)) as stdev      
FROM hcup_chgs WHERE visitlink IS NOT NULL AND dq_flag = 0;
#Avg total cost / patient 2013 - 2014 inpatient: 1170.3432
#STD: 2793.2712676707933
SELECT AVG(t.totchg), STD(t.totchg)
FROM ((SELECT totchg FROM 2013_sedd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL)) 
UNION ALL (SELECT totchg FROM 2014_sedd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL))) t
#Avg cost / visit: 913.2335387500749
#STD cost / visit: 1367.9955411543272

#Avg combined cost / patient 2013
SELECT AVG(IFNULL(2013_sid_chg, 0) + IFNULL(2013_sedd_chg, 0) + IFNULL(2013_sasd_chg, 0)) as Average,
  STD(IFNULL(2013_sid_chg, 0) + IFNULL(2013_sedd_chg, 0) + IFNULL(2013_sasd_chg,0)) as stdev      
FROM hcup_chgs WHERE visitlink IS NOT NULL and dq_flag = 0;
#Avg total cost / patient 2013 combined: 4665.0642
#STD total cost / patient 2013 combined: 17840.61469962334
SELECT AVG(t.totchg), STD(t.totchg)
FROM ((SELECT totchg FROM 2013_sedd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL))
UNION ALL (SELECT totchg FROM 2013_sasd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL))
UNION ALL (SELECT totchg FROM 2013_sid_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL))) t;
#Avg cost / visit 2013 combined: 2758.7873727226083
#Avg cost / visit 2013 combined: 9599.960889375905

#Avg combined cost / patient 2014
SELECT AVG(IFNULL(2014_sid_chg, 0) + IFNULL(2014_sedd_chg, 0) + IFNULL(2014_sasd_chg, 0)) as Average,
  STD(IFNULL(2014_sid_chg, 0) + IFNULL(2014_sedd_chg, 0) + IFNULL(2014_sasd_chg,0)) as stdev      
FROM hcup_chgs WHERE visitlink IS NOT NULL and dq_flag = 0;
#Avg total cost / patient 2014 combined: 4673.8149
#STD total cost / patient 2014 combined: 18020.682636317797
SELECT AVG(t.totchg), STD(t.totchg)
FROM ((SELECT totchg FROM 2014_sedd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL))
UNION ALL (SELECT totchg FROM 2014_sasd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL))
UNION ALL (SELECT totchg FROM 2014_sid_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL))) t;
#Avg cost / visit 2014 combined: 2794.461809963902
#Avg cost / visit 2014 combined: 9610.811821193456

#Avg combined cost / patient 2013 - 2014
SELECT AVG(IFNULL(2013_sid_chg, 0) + IFNULL(2013_sedd_chg, 0) + IFNULL(2013_sasd_chg, 0) + IFNULL(2014_sid_chg, 0) + IFNULL(2014_sedd_chg, 0) + IFNULL(2014_sasd_chg, 0)) as Average,
  STD(IFNULL(2013_sid_chg, 0) + IFNULL(2013_sedd_chg, 0) + IFNULL(2013_sasd_chg, 0) + IFNULL(2014_sid_chg, 0) + IFNULL(2014_sedd_chg, 0) + IFNULL(2014_sasd_chg,0)) as stdev      
FROM hcup_chgs WHERE visitlink IS NOT NULL and dq_flag = 0;
#Avg total cost / patient 2013 - 2014 combined: 9338.8791
#STD total cost / patient 2013 - 2014 combined: 28240.34507270337
SELECT AVG(t.totchg), STD(t.totchg)
FROM ((SELECT totchg FROM 2014_sedd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL))
UNION ALL (SELECT totchg FROM 2014_sasd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL))
UNION ALL (SELECT totchg FROM 2014_sid_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL))
UNION ALL (SELECT totchg FROM 2013_sasd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL))
UNION ALL (SELECT totchg FROM 2013_sid_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL))
UNION ALL (SELECT totchg FROM 2013_sedd_core WHERE visitlink IN (SELECT visitlink FROM hcup_pat_pop_spec_remainder WHERE visitlink IS NOT NULL))) t;
#Avg cost / visit 2013 - 2014 combined: 2776.5267170575203
#Avg cost / visit 2013 - 2014 combined: 9605.374678921533

#-----------------------Creating totchg tables -----------------------------------------------
CREATE TABLE `hcup_totchgs` 
(
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `visitlink` varchar(16) DEFAULT NULL,
  `dq_flag` int(1) NOT NULL DEFAULT 0,
  #completeness measures
  `2013_sedd_totchg` int(16) NOT NULL DEFAULT 0,
  `2013_sasd_totchg` int(16) NOT NULL DEFAULT 0,
  `2013_sid_totchg` int(16) NOT NULL DEFAULT 0,

  `2014_sedd_totchg` int(16) NOT NULL DEFAULT 0,
  `2014_sasd_totchg` int(16) NOT NULL DEFAULT 0,
  `2014_sid_totchg` int(16) NOT NULL DEFAULT 0,
  
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


SET SQL_SAFE_UPDATES = 0;

INSERT INTO hcup_totchgs (visitlink, 2013_sedd_totchg)
SELECT visitlink, totchg
FROM 2013_sedd_core
WHERE visitlink IS NOT NULL;

INSERT INTO hcup_totchgs (visitlink, 2013_sasd_totchg)
SELECT visitlink, totchg
FROM 2013_sasd_core
WHERE visitlink IS NOT NULL;

INSERT INTO hcup_totchgs (visitlink, 2013_sid_totchg)
SELECT visitlink, totchg
FROM 2013_sid_core
WHERE visitlink IS NOT NULL;

INSERT INTO hcup_totchgs (visitlink, 2014_sedd_totchg)
SELECT visitlink, totchg
FROM 2014_sedd_core
WHERE visitlink IS NOT NULL;

INSERT INTO hcup_totchgs (visitlink, 2014_sasd_totchg)
SELECT visitlink, totchg
FROM 2014_sasd_core
WHERE visitlink IS NOT NULL;

INSERT INTO hcup_totchgs (visitlink, 2014_sid_totchg)
SELECT visitlink, totchg
FROM 2014_sid_core
WHERE visitlink IS NOT NULL;

UPDATE hcup_totchgs
INNER JOIN (
  SELECT visitlink
  FROM hcup_pat_pop_spec_flagged
  GROUP BY visitlink) t
ON hcup_chgs.visitlink = t.visitlink
OR hcup_chgs.visitlink IS NULL AND t.visitlink IS NULL
SET dq_flag = 1;

DELETE FROM hcup_totchgs WHERE 
  2013_sedd_totchg = 0 AND
  2013_sasd_totchg = 0 AND
  2013_sid_totchg = 0 AND

  2014_sedd_totchg = 0 AND
  2014_sasd_totchg = 0 AND
  2014_sid_totchg = 0;

SELECT visitlink, dq_flag, 2013_sedd_totchg, 2013_sid_totchg, 2013_sasd_totchg, 2014_sedd_totchg, 2014_sid_totchg, 2014_sasd_totchg 
FROM hcup_totchgs
WHERE visitlink IS NOT NULL
INTO OUTFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/hcup_totchgs.csv'
FIELDS ENCLOSED BY '"'
TERMINATED BY ','
LINES TERMINATED BY '\n';

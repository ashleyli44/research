
/*
  This is intended to find the number of distinct zipcodes across multiple tables
*/

CREATE TABLE `hcup_pat_zip3_chngs` 
(
  `visitlink` varchar(16) DEFAULT NULL,
  
  `num_distinct2013` int(4)   DEFAULT NULL,
  `num_distinct2014` int(4)   DEFAULT NULL,
  `num_distinctTotal` int(4)   DEFAULT NULL,

  UNIQUE KEY `visitlink_UNIQUE` (`visitlink`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#inserting visitlinks
INSERT    INTO hcup_pat_zip3_chngs (visitlink)
SELECT    visitlink 
FROM    hcup_pat_visitlink_uni; 

SET SQL_SAFE_UPDATES = 0;

#filling the col num_distinct2013 -- executed
UPDATE hcup_pat_zip3_chngs
INNER JOIN ( 
    SELECT visitlink, COUNT(DISTINCT zip3) as num_distinct FROM (
    SELECT visitlink, zip3 FROM 2013_sid_core
    UNION SELECT visitlink, zip3 FROM 2013_sedd_core
    UNION SELECT visitlink, zip3 FROM 2013_sasd_core) t
    GROUP BY visitlink) t1
ON  (hcup_pat_zip3_chngs.visitlink = t1.visitlink)
OR  (hcup_pat_zip3_chngs.visitlink IS NULL AND t1.visitlink IS NULL) #not sure if line is necessary?
#wouldn't this line assign the most recent null patient's zipcode to the null row at the beginning?
SET   num_distinct2013 = t1.num_distinct; 

#filling the col num_distinct2014 
UPDATE hcup_pat_zip3_chngs
INNER JOIN ( 
    SELECT visitlink, COUNT(DISTINCT zip3) as num_distinct FROM (
    SELECT visitlink, zip3 FROM 2014_sid_core
    UNION SELECT visitlink, zip3 FROM 2014_sedd_core
    UNION SELECT visitlink, zip3 FROM 2014_sasd_core) t
    GROUP BY visitlink) t1
ON  (hcup_pat_zip3_chngs.visitlink = t1.visitlink)
OR  (hcup_pat_zip3_chngs.visitlink IS NULL AND t1.visitlink IS NULL) #not sure if line is necessary?
#wouldn't this line assign the most recent null patient's zipcode to the null row at the beginning?
SET   num_distinct2014 = t1.num_distinct; 

UPDATE hcup_pat_zip3_chngs
INNER JOIN ( 
    SELECT visitlink, COUNT(DISTINCT zip3) as num_distinct FROM (
    SELECT visitlink, zip3 FROM 2014_sid_core
    UNION SELECT visitlink, zip3 FROM 2014_sedd_core
    UNION SELECT visitlink, zip3 FROM 2014_sasd_core
    UNION SELECT visitlink, zip3 FROM 2013_sedd_core
    UNION SELECT visitlink, zip3 FROM 2013_sid_core
    UNION SELECT visitlink, zip3 FROM 2013_sasd_core) t
    GROUP BY visitlink) t1
ON  (hcup_pat_zip3_chngs.visitlink = t1.visitlink)
OR  (hcup_pat_zip3_chngs.visitlink IS NULL AND t1.visitlink IS NULL) #not sure if line is necessary?
#wouldn't this line assign the most recent null patient's zipcode to the null row at the beginning?
SET   num_distinctTotal = t1.num_distinct; 

SELECT COUNT(*), num_distinct2013 FROM hcup_pat_zip3_chngs 
GROUP BY num_distinct2013;

/* 2013 The number of patients vs the number of distinct zip codes reported
  + ------------- + -------------- +
  | pat_ct        | avg2013        |
  + ------------- + -------------- +  
  | 1,078,899     | null           |
  | 2,143,196     | 1              |
  | 39,951        | 2              |
  | 1,638         | 3              |
  | 146           | 4              |
  | 24            | 5              |
  | 7             | 6              | 
  | 2             | 7              | 
  | 1             | 8              |
  | 1             | 174            | the null visitlink
  + ------------- + -------------- +
*/

SELECT COUNT(*), num_distinct2014 FROM hcup_pat_zip3_chngs 
GROUP BY num_distinct2014;

/* 2014 The number of patients vs the number of distinct zip codes reported
  + ------------- + -------------- +
  | pat_ct        | avg2014        |
  + ------------- + -------------- +
  | 1,091,679     | null           | 
  | 2,135,263     | 1              |
  | 35,142        | 2              |
  | 1,579         | 3              |
  | 161           | 4              |
  | 29            | 5              |
  | 8             | 6              | 
  | 2             | 7              | 
  | 1             | 11             |
  | 1             | 170            | the null visitlink
  + ------------- + -------------- +
*/

SELECT COUNT(*), num_distinctTotal FROM hcup_pat_zip3_chngs 
GROUP BY num_distinctTotal;

/*
  2013 and 2014
  + ------------- + -------------- +
  | pat_ct        | zip2013/2014   |
  + ------------- + -------------- +
  | 3,163,736     | 1              |
  | 94,248        | 2              |
  | 5,187         | 3              |
  | 565           | 4              |
  | 94            | 5              |
  | 27            | 6              | 
  | 3             | 7              | 
  | 2             | 8              |
  | 2             | 11             |
  | 1             | 254            | the null visitlink
  + ------------- + -------------- +*/
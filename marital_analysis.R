library(data.table)
library(plyr)
#setting up db
library(RMySQL)

#connectino to the database and establishment of the DF
con <- dbConnect(MySQL(), user = "root", password = "", dbname = "hcup", host = "localhost")
rs <- dbSendQuery(con, "select * from hcup_pat_marital_freq")
newDF1 <- fetch(rs, n = -1)

#making the dataframe into a data table
db <- as.data.table(newDF1)
setkey(db, visitlink) #speeds up indexing

#groups the elements by visitlink then orders by daystoevent within a visitlink
db <- db[order(visitlink, daystoevent)]
#days_max represents the upper limit of the range
db <- db[, days_max := daystoevent + 60]

#intializing empty results data table
results <- data.table (
  visitlink = integer(),
  start = integer(), 
  end = integer(),
  pattern = character()
)

#This table tells me the number of rows for each visitlink which will 
#allow me to precisily cut out the exact subset of db rather than rely on the db[visitlink == id]
freq_table <- as.data.table(count(db, "visitlink"))
freq_table <- freq_table[order(visitlink)] #ensures that the order of the visitlinks are the same as in db
# visitlink | number of rows

#counts the number of visitlinks
counter <- 0

#keeps track of the starting row in db
start_row <- 1


for(k in seq(1, freq_table[, .N]))
{
  num_rows <- freq_table[k, freq] #finds the number of rows for a specific visitlink
  
  db_pat <- db[start_row: (start_row + num_rows - 1), ] #extracts the subset of db rows for a specific visitlink
  #db_pat is now a data table
  db_pat_end <- db_pat[, .N] #number of elements in db_pat
  
  id <- as.character(freq_table[k, visitlink]) #the visitlink of the patient in db_pat --> intially a factor so must convert to char
  
  start_row <- start_row + num_rows #updates the number of rows
  
  counter <- counter + 1 #increases counter to keep track of visitlinks computed
  
  if(counter %% 100 == 0)
  {
    print(paste0(counter, " out of ", visitlink_max))
  }
  
  j <- 0
  for (i in seq(1, db_pat_end)) 
  {
    j <- j + 1
    #makes sure to break to avoid index out of bounds error
    if (j > db_pat_end)
    {
      break
    }
    
    max <- db_pat[i, days_max] #finds the max
    
    days_array <- db_pat[j:db_pat_end, ][daystoevent < max]$daystoevent #extracts all days less than the max
    
    ms_array <- as.character(db_pat[j:db_pat_end, ][daystoevent < max]$marital_status) #extracts marital status for those days
    
    if(length(ms_array) > 2)
    {
      #create temp data table to append
      temp <- data.table(visitlink = id, start = days_array[1], end = days_array[length(days_array)], pattern = paste(ms_array, collapse = ","))
      #incremement j to jump over entries all ready inputted
      j = j + length(days_array)
      #append temp to results
      results <- rbind(results, temp)
    }
  }
}


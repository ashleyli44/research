CREATE TABLE `hcup_pat_zip3_complete` 
(
  `visitlink` varchar(16) DEFAULT NULL,
  
  `NULL_ct` int(4)  DEFAULT NULL,
  `visit_ct` int(4) DEFAULT NULL, 

  `null_zip3_flag` int(1) DEFAULT NULL, 

  UNIQUE KEY `visitlink_UNIQUE` (`visitlink`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO hcup_pat_zip3_complete (visitlink)
SELECT    visitlink 
FROM    hcup_pat_visitlink_uni; -- 301 sec

UPDATE hcup_pat_zip3_complete
SET NULL_ct = 0;

#2013
UPDATE hcup_pat_zip3_complete
INNER JOIN (
  SELECT visitlink, zip3_null
  FROM (
    SELECT visitlink,
      IFNULL(SUM(zip3 = "A" OR zip3 = '' OR ISNULL(zip3)),0) zip3_null
    FROM 2013_sid_core
    GROUP BY visitlink
      ) t1
  GROUP BY visitlink) t2
ON hcup_pat_zip3_complete.visitlink = t2.visitlink 
OR hcup_pat_zip3_complete.visitlink IS NULL AND t2.visitlink IS NULL
SET NULL_ct = NULL_ct + t2.zip3_null;

UPDATE hcup_pat_zip3_complete
INNER JOIN (
  SELECT visitlink, zip3_null
  FROM (
    SELECT visitlink,
      IFNULL(SUM(zip3 = "A" OR zip3 = '' OR ISNULL(zip3)),0) zip3_null
    FROM 2013_sedd_core
    GROUP BY visitlink
      ) t1
  GROUP BY visitlink) t2
ON hcup_pat_zip3_complete.visitlink = t2.visitlink 
OR hcup_pat_zip3_complete.visitlink IS NULL AND t2.visitlink IS NULL
SET NULL_ct = NULL_ct + t2.zip3_null;

UPDATE hcup_pat_zip3_complete
INNER JOIN (
  SELECT visitlink, zip3_null
  FROM (
    SELECT visitlink,
      IFNULL(SUM(zip3 = "A" OR zip3 = '' OR ISNULL(zip3)),0) zip3_null
    FROM 2013_sasd_core
    GROUP BY visitlink
      ) t1
  GROUP BY visitlink) t2
ON hcup_pat_zip3_complete.visitlink = t2.visitlink 
OR hcup_pat_zip3_complete.visitlink IS NULL AND t2.visitlink IS NULL
SET NULL_ct = NULL_ct + t2.zip3_null;

#2014
UPDATE hcup_pat_zip3_complete
INNER JOIN (
  SELECT visitlink, zip3_null
  FROM (
    SELECT visitlink,
      IFNULL(SUM(zip3 = "A" OR zip3 = '' OR ISNULL(zip3)),0) zip3_null
    FROM 2014_sid_core
    GROUP BY visitlink
      ) t1
  GROUP BY visitlink) t2
ON hcup_pat_zip3_complete.visitlink = t2.visitlink 
OR hcup_pat_zip3_complete.visitlink IS NULL AND t2.visitlink IS NULL
SET NULL_ct = NULL_ct + t2.zip3_null;

UPDATE hcup_pat_zip3_complete
INNER JOIN (
  SELECT visitlink, zip3_null
  FROM (
    SELECT visitlink,
      IFNULL(SUM(zip3 = "A" OR zip3 = '' OR ISNULL(zip3)),0) zip3_null
    FROM 2014_sedd_core
    GROUP BY visitlink
      ) t1
  GROUP BY visitlink) t2
ON hcup_pat_zip3_complete.visitlink = t2.visitlink 
OR hcup_pat_zip3_complete.visitlink IS NULL AND t2.visitlink IS NULL
SET NULL_ct = NULL_ct + t2.zip3_null;

UPDATE hcup_pat_zip3_complete
INNER JOIN (
  SELECT visitlink, zip3_null
  FROM (
    SELECT visitlink,
      IFNULL(SUM(zip3 = "A" OR zip3 = '' OR ISNULL(zip3)),0) zip3_null
    FROM 2014_sasd_core
    GROUP BY visitlink
      ) t1
  GROUP BY visitlink) t2
ON hcup_pat_zip3_complete.visitlink = t2.visitlink 
OR hcup_pat_zip3_complete.visitlink IS NULL AND t2.visitlink IS NULL
SET NULL_ct = NULL_ct + t2.zip3_null;

#visit ct
UPDATE hcup_pat_zip3_complete
SET visit_ct = 0;

#calculating visit_ct
UPDATE hcup_pat_zip3_complete
INNER JOIN (
  SELECT  visitlink,
          IF(sasd_2013_ct IS NULL, 0, sasd_2013_ct) sasd_2013_ct,
          IF(sedd_2013_ct IS NULL, 0, sedd_2013_ct) sedd_2013_ct,
          IF(sid_2013_ct IS NULL, 0, sid_2013_ct) sid_2013_ct,
          IF(sasd_2014_ct IS NULL, 0, sasd_2014_ct) sasd_2014_ct,
          IF(sedd_2014_ct IS NULL, 0, sedd_2014_ct) sedd_2014_ct,
          IF(sid_2014_ct IS NULL, 0, sid_2014_ct) sid_2014_ct
  FROM  hcup_pat_visitlink_uni
  GROUP BY visitlink
          ) t1
ON hcup_pat_zip3_complete.visitlink = t1.visitlink
OR hcup_pat_zip3_complete.visitlink IS NULL AND t1.visitlink IS NULL
SET visit_ct = sasd_2013_ct + sedd_2013_ct + sid_2013_ct + sasd_2014_ct + sedd_2014_ct + sid_2014_ct; 

#calculating null_sex_flag
UPDATE hcup_pat_zip3_complete
SET null_zip3_flag = IF(null_ct = visit_ct, 1, 0);

SELECT COUNT(*), null_zip3_flag FROM hcup_pat_zip3_complete GROUP BY null_zip3_flag; 
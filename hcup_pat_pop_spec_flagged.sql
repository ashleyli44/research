#population spec of the flagged population, needs at least 1 flag
CREATE TABLE `hcup_pat_pop_spec_flagged` 
(
  `visitlink` varchar(16) DEFAULT NULL,
  
  `avg_age` int(3) DEFAULT NULL,
  `std_age` decimal(5,2) DEFAULT NULL,

  `race` varchar(1) DEFAULT NULL,

  `sex` varchar(1) DEFAULT NULL,

  `marital_status` varchar (1) DEFAULT NULL,

  UNIQUE KEY `visitlink_UNIQUE` (`visitlink`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#inserting visitlinks
INSERT 	INTO hcup_pat_pop_spec_flagged (visitlink)
SELECT 	visitlink 
FROM 	dq_master
WHERE 	maritalFlag = 1 OR raceFlag = 1 OR sexFlag = 1 OR zipFlag = 1 OR ageFlag = 1; -- 314 sec

UPDATE hcup_pat_pop_spec_flagged
INNER JOIN (
	SELECT * 
	FROM hcup_pat_pop_spec_all
	)t
ON hcup_pat_pop_spec_flagged.visitlink = t.visitlink
OR hcup_pat_pop_spec_flagged.visitlink IS NULL AND t.visitlink IS NULL
SET hcup_pat_pop_spec_flagged.avg_age = t.avg_age,
	hcup_pat_pop_spec_flagged.std_age = t.std_age,
	hcup_pat_pop_spec_flagged.race = t.race,
	hcup_pat_pop_spec_flagged.sex = t.sex,
	hcup_pat_pop_spec_flagged.marital_status = t.marital_status;

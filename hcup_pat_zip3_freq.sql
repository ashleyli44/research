CREATE TABLE `hcup_pat_zip3_freq` 
(
  `ID` int NOT NULL AUTO_INCREMENT,
  `visitlink` varchar(16) DEFAULT NULL,
  `zip` varchar(16) DEFAULT NULL, 
  `daystoevent` int(10) DEFAULT NULL,

  PRIMARY KEY (ID)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*
	Creation of zip3 table similar to marital table
*/

#2013
INSERT INTO hcup_pat_zip3_freq (visitlink, zip, daystoevent)
SELECT visitlink, zip3, CAST(daystoevent AS UNSIGNED) daystoevent
FROM 2013_sid_core
WHERE zip3 IS NOT NULL AND zip3 <> '';

INSERT INTO hcup_pat_zip3_freq (visitlink, zip, daystoevent)
SELECT visitlink, zip3, CAST(daystoevent AS UNSIGNED) daystoevent
FROM 2013_sedd_core
WHERE zip3 IS NOT NULL AND zip3 <> '';

INSERT INTO hcup_pat_zip3_freq (visitlink, zip, daystoevent)
SELECT visitlink, zip3, CAST(daystoevent AS UNSIGNED) daystoevent
FROM 2013_sasd_core
WHERE zip3 IS NOT NULL AND zip3 <> '';

#2014
INSERT INTO hcup_pat_zip3_freq (visitlink, zip, daystoevent)
SELECT visitlink, zip3, CAST(daystoevent AS UNSIGNED) daystoevent
FROM 2014_sid_core
WHERE zip3 IS NOT NULL AND zip3 <> '';

INSERT INTO hcup_pat_zip3_freq (visitlink, zip, daystoevent)
SELECT visitlink, zip3, CAST(daystoevent AS UNSIGNED) daystoevent
FROM 2014_sedd_core
WHERE zip3 IS NOT NULL AND zip3 <> '';

INSERT INTO hcup_pat_zip3_freq (visitlink, zip, daystoevent)
SELECT visitlink, zip3, CAST(daystoevent AS UNSIGNED) daystoevent
FROM 2014_sasd_core
WHERE zip3 IS NOT NULL AND zip3 <> '';


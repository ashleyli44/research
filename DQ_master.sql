#Creating master DQ Table
CREATE TABLE `DQ_master` 
(
  `visitlink` varchar(16) DEFAULT NULL,
  #completeness measures
  `ageFlag_C` int(1) NOT NULL DEFAULT 0,
  `sexFlag_C` int(1) NOT NULL DEFAULT 0,
  `raceFlag_C` int(1) NOT NULL DEFAULT 0,
  `maritalFlag_C` int(1) NOT NULL DEFAULT 0,
  `zipFlag_C` int(1) NOT NULL DEFAULT 0,

  #fidelity and plausibility measures
  `ageFlag_FP` int(1) NOT NULL DEFAULT 0,
  `sexFlag_FP` int(1) NOT NULL DEFAULT 0,
  `raceFlag_FP` int(1) NOT NULL DEFAULT 0,
  `maritalFlag_FP` int(1) NOT NULL DEFAULT 0,
  `zipFlag_FP` int(1) NOT NULL DEFAULT 0,

  UNIQUE KEY `visitlink_UNIQUE` (`visitlink`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#inserting visitlinks
INSERT INTO DQ_master (visitlink)
SELECT    visitlink 
FROM    hcup_pat_visitlink_uni; 

SET SQL_SAFE_UPDATES = 0;

#sex_flag - Fidelity and Plasubility 
UPDATE dq_master
INNER JOIN (
	SELECT visitlink, sex_flag 
	FROM hcup_pat_sex 
	GROUP BY visitlink) t
ON dq_master.visitlink = t.visitlink
OR dq_master.visitlink IS NULL AND t.visitlink IS NULL
SET sexFlag_FP = IF(t.sex_flag = 1, 1, sexFlag_FP);

#age flag - FP
UPDATE DQ_master
INNER JOIN ( 
  SELECT  visitlink,
          age_flag
  FROM hcup_pat_age
  GROUP BY visitlink
  ) temp
ON  (DQ_master.visitlink = temp.visitlink)
OR  (DQ_master.visitlink IS NULL AND temp.visitlink IS NULL) 
SET   ageFlag_FP = IF(temp.age_flag = 1 AND ageFlag_C = 0, 1, ageFlag_FP);

UPDATE DQ_master
INNER JOIN (
	SELECT visitlink, post_death_flag
	FROM hcup_pat_died
	GROUP BY visitlink
	) temp
ON	(DQ_master.visitlink = temp.visitlink)
OR	(DQ_master.visitlink IS NULL AND temp.visitlink IS NULL)
SET ageFlag_FP = IF(temp.post_death_flag = 1 AND ageFlag_C = 0, 1, ageFlag_FP);

#race flag - FP
UPDATE DQ_master
INNER JOIN ( 
  SELECT  visitlink,
          race_flag
  FROM hcup_pat_race
  GROUP BY visitlink
  ) temp
ON  (DQ_master.visitlink = temp.visitlink)
OR  (DQ_master.visitlink IS NULL AND temp.visitlink IS NULL) 
SET   raceFlag_FP = IF(temp.race_flag = 1, 1, raceFlag_FP);


#setting the zipFlag - FP

UPDATE dq_master
INNER JOIN ( SELECT visitlink
			FROM hcup_pat_zip3_chngs
      WHERE num_distinct2013 >= 4 OR num_distinct2014 >= 4
			GROUP BY visitlink) t
ON dq_master.visitlink = t.visitlink
OR dq_master.visitlink IS NULL AND t.visitlink IS NULL
SET zipFlag_FP = 1;

UPDATE dq_master
INNER JOIN (
	SELECT visitlink, zip3_flag
	FROM hcup_pat_zip3_azn
  WHERE zip3_flag = 1
	GROUP BY visitlink) t
ON dq_master.visitlink = t.visitlink
OR dq_master.visitlink IS NULL AND t.visitlink IS NULL
SET zipFlag_FP = 1;

#setting the maritalFlag - FP

UPDATE dq_master
INNER JOIN (
  SELECT visitlink
  FROM hcup_pat_marital_azn
  WHERE marital_flag >= 2
  GROUP BY visitlink) t
ON dq_master.visitlink = t.visitlink
OR dq_master.visitlink IS NULL and t.visitlink IS NULL
SET maritalFlag_FP = 1;


UPDATE dq_master
INNER JOIN (SELECT visitlink, marital_age_flag
			FROM hcup_pat_marital GROUP BY visitlink) t
ON dq_master.visitlink = t.visitlink 
OR dq_master.visitlink IS NULL AND t.visitlink IS NULL
SET maritalFlag_FP = IF(marital_age_flag = 1, 1, maritalFlag_FP);


/*---------Adding Null Flags to dq_master------------------*/
#age null flag - C
UPDATE dq_master
INNER JOIN (SELECT visitlink, null_age_flag
			FROM hcup_pat_age_complete GROUP BY visitlink) t
ON dq_master.visitlink = t.visitlink
OR dq_master.visitlink IS NULL AND t.visitlink IS NULL
SET ageFlag_C = IF(t.null_age_flag = 1, 1, ageFlag_C);

#marital null
UPDATE dq_master
INNER JOIN (SELECT visitlink, null_marital_flag
			FROM hcup_pat_marital_complete GROUP BY visitlink) t
ON dq_master.visitlink = t.visitlink
OR dq_master.visitlink IS NULL AND t.visitlink IS NULL
SET maritalFlag_C = IF(t.null_marital_flag = 1, 1, maritalFlag_C);

#race null
UPDATE dq_master
INNER JOIN (SELECT visitlink, null_race_flag
			FROM hcup_pat_race_complete GROUP BY visitlink) t
ON dq_master.visitlink = t.visitlink
OR dq_master.visitlink IS NULL AND t.visitlink IS NULL
SET raceFlag_C = IF(t.null_race_flag = 1, 1, raceFlag_C);

#sex null
UPDATE dq_master
INNER JOIN (SELECT visitlink, null_sex_flag
			FROM hcup_pat_sex_complete GROUP BY visitlink) t
ON dq_master.visitlink = t.visitlink
OR dq_master.visitlink IS NULL AND t.visitlink IS NULL
SET sexFlag_C = IF(t.null_sex_flag = 1, 1, sexFlag_C);

#zip code null
UPDATE dq_master
INNER JOIN (
  SELECT visitlink, null_zip3_flag
  FROM hcup_pat_zip3_complete
  GROUP BY visitlink) t
ON dq_master.visitlink = t.visitlink
OR dq_master.visitlink IS NULL and t.visitlink IS NULL
SET zipFlag_C = IF(t.null_zip3_flag = 1, 1, zipFlag_C);

ALTER TABLE dq_master
ADD COLUMN sex_CFP int(1) AFTER zipFlag_FP,
ADD COLUMN age_CFP int(1) AFTER sex_CFP,
ADD COLUMN race_CFP int(1) AFTER age_CFP,
ADD COLUMN marital_CFP int(1) AFTER race_CFP,
ADD COLUMN zip_CFP int(1) AFTER marital_CFP;

#completeness, fidelity, and plausbility
UPDATE dq_master
SET 
  sex_CFP = IF(sexFlag_C = 1 OR sexFlag_FP = 1, 1, 0),
  age_CFP = IF(ageFlag_C = 1 OR ageFlag_FP = 1, 1, 0),
  race_CFP = IF(raceFlag_C = 1 OR raceFlag_FP = 1, 1, 0),
  marital_CFP = IF(maritalFlag_C = 1 OR maritalFlag_FP = 1, 1, 0),
  zip_CFP = IF(zipFlag_C = 1 OR zipFlag_FP = 1, 1, 0);

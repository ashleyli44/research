#population spec of all the patients
CREATE TABLE `hcup_pat_pop_spec_all` 
(
  `visitlink` varchar(16) DEFAULT NULL,
  
  `avg_age` int(3) DEFAULT NULL,
  `std_age` decimal(5,2) DEFAULT NULL,

  `race` varchar(1) DEFAULT NULL,

  `sex` varchar(1) DEFAULT NULL,

  UNIQUE KEY `visitlink_UNIQUE` (`visitlink`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#inserting visitlinks
INSERT 	INTO hcup_pat_pop_spec_all (visitlink)
SELECT 	visitlink 
FROM 	hcup_pat_visitlink_uni; -- 314 sec

#inserting ages and std
UPDATE hcup_pat_pop_spec_all
INNER JOIN (
	SELECT visitlink, age_avg, age_std
	FROM hcup_pat_age
	GROUP BY visitlink) t 
ON hcup_pat_pop_spec_all.visitlink = t.visitlink
OR hcup_pat_pop_spec_all.visitlink IS NULL AND t.visitlink IS NULL
SET avg_age = IF(t.age_avg IS NULL, NULL, t.age_avg), 
	std_age = IF(t.age_std IS NULL, NULL, t.age_std);

#inserting race
UPDATE hcup_pat_pop_spec_all
INNER JOIN (
	SELECT visitlink, race_last
	FROM hcup_pat_race
	GROUP BY visitlink) t
ON hcup_pat_pop_spec_all.visitlink = t.visitlink
OR hcup_pat_pop_spec_all.visitlink IS NULL AND t.visitlink IS NULL
SET race = race_last;

#inserting sex --> 0 is male 1 is female
UPDATE hcup_pat_pop_spec_all
INNER JOIN (
	SELECT visitlink, sex
	FROM hcup_pat_sex
	GROUP BY visitlink) t 
ON hcup_pat_pop_spec_all.visitlink = t.visitlink
OR hcup_pat_pop_spec_all.visitlink IS NULL AND t.visitlink IS NULL
SET hcup_pat_pop_spec_all.sex = IF(t.sex IS NULL, NULL, t.sex);

ALTER TABLE hcup_pat_pop_spec_all
ADD COLUMN marital_status varchar(1) AFTER sex;

#finds last reported non null marital status
UPDATE hcup_pat_pop_spec_all
INNER JOIN (
	SELECT visitlink, marital_status
    FROM hcup_pat_marital_last_reported
    WHERE visitlink IS NOT NULL
    ) T
    
ON hcup_pat_pop_spec_all.visitlink = T.visitlink
OR hcup_pat_pop_spec_all.visitlink IS NULL AND T.visitlink IS NULL
SET hcup_pat_pop_spec_all.marital_status = T.marital_status;

#finds all null marital status entries
UPDATE hcup_pat_pop_spec_all
INNER JOIN (
	SELECT visitlink
    FROM hcup_pat_marital_complete
    WHERE null_marital_flag = 1 AND visitlink IS NOT NULL) t 
ON hcup_pat_pop_spec_all.visitlink = t.visitlink
OR hcup_pat_pop_spec_all.visitlink IS NULL AND t.visitlink IS NULL
SET marital_status = 'N';

#accounts for any remaining entries
UPDATE hcup_pat_pop_spec_all
INNER JOIN (
	SELECT visitlink, marital_freq 
	FROM hcup_pat_marital 
	WHERE visitlink IS NOT NULL) t 
ON hcup_pat_pop_spec_all.visitlink = t.visitlink
OR hcup_pat_pop_spec_all.visitlink IS NULL AND t.visitlink IS NULL
SET marital_status = IF(marital_status IS NULL, t.marital_freq, marital_status);
library(pracma)
#recognizes different patterns for zipcode
#counts the number of times a zipcode changes
#@param string is the pattern of zipcodes for a given 60 day window
pattern_recog <- function(string)
{
  str_arr <- strsplit(string, ",")[[1]]
  count <- 0
  flag <- 0
  for(i in seq(1,length(str_arr) -1 ))
  {
    if(! (strcmp(str_arr[i], str_arr[i+1])))
    {
      count <- count + 1
    }
    if(count >= 3)
    {
      flag <- 1
      break
    }

  }
  return(flag)
}

#Pattern analysis
library(data.table)
#splits the pattern by ,
results[, num_uniq := length(unique(strsplit(pattern,",")[[1]])), by = pattern]
#sets all flags to 0
results[, zip3_flag := 0L]

#patients with 3 or more unique zipcodes are automatically changed
results[num_uniq >= 3, zip3_flag := 1L]

#iterates through all patients and their 60day windows
for (i in seq(1, results[,.N]))
{
  if(i %% 100 == 0)
  {
    print(paste0(i, " out of ", results[, .N]))
    
  }

  if ((results$zip3_flag[i])==0)
  {
    results$zip3_flag[i] <- pattern_recog(results$pattern[i])
  }
}



Ashley Li
ali44@jhu.edu

# Utilization Stats for HCUP Maryland Database

A data quality analysis was performed over 6 stateside databases. Data was collected from 3 different patient sources (inpatient, outpatient, and emergency room) over 2013 and 2014. After identifying patients with data quality issues, a follow-up analysis was performed as detailed below to explore how these data quality issues effect utilization statistics. 

See [here](https://bitbucket.org/ashleyli44/research/src/master/) for all files mentioned below. 

### Statistics Collected:

1. Visits
2. LOS
3. Charges
    1. Total charges per patient (ie how much was the patient charged summed over every visit they made?)	
    2. Charge per visit (ie how much was a patient typically charged for each visit)
4. Number of patients per CCS lvl 1
5. Comorbidity - Charlson and Elixhauser

### Visits

#### Method of calculation

The average number of visits / patient and standard deviation and p-values were calculated in R (see visits_results.R). To faciliate calculation, the data was exported from mySQL as a csv file. Before exporting, a dq_flag column was added to the hcup_pat_visitlink_uni table in mySQL. This was acheived by inserting a new col and then updating the table based on the unique visitlink ids from hcup_pat_pop_spec_flagged table. After this was accomplished, the mean, standard deviation, and p-values were calculated. A t-test was used for the p-values. The results were exported as a csv file. 

### LOS

#### Method of calculation

Average LOS was calculated across all databases. Average, standard deviation, and p-values were calculated in R (see los_results.R) To faciliate exportation from mySQL, a separate table called hcup_los was created (see hcup_utilization_stats.sql). This table contained visitlinks, dq_flag, and LOS from all databases. The LOS was constrained to be 0-365 for inpatients and 0-30 for outpatients. To find out more, click [here](https://www.hcup-us.ahrq.gov/) 
After exporting the table as a csv file, it was used in los_results.R to calculate the average and standard deviation of LOS / patient. A t-test was used to find the p-values. 


### Charges

#### Method of calculation

To calculate charges, the totchg field was used. In Maryland, this data element is calculated based on charges associated with the revenue code during data processing done by HCUP. Physician charges and charges not regulated by the Health Services Cost Review Commission (ex. telephone service, tv chg, etc) are excluded from the totchg. To find out more, click [here](https://www.hcup-us.ahrq.gov/db/vars/siddistnote.jsp?var=totchg). Values were rounded to the nearest dollar and zero charges were set to missing.  

2 charge statistics were calculated:
* total charge / patient
* charge / visit

Both statistics were calculated over time (ie total charge / patient over 2013 - 2014 from a particular source), over sources (ie total charge / patient across patients from OP, IP, ED), and both. All p-values were calculated using a t-test.

To calculate total charge / patient, a separate table was created called hcup_chgs (see hcup_chgs.sql). This summed the totchg field by visitlink within each of the 6 databases producing a table with unique patients as rows and their respective summed charges for each database as the columns. This table was cleaned before analysis; there were 81,402 patients with 0 sum cost over all 6 databases. This occured because the totchg was null for all instances for each patient. Of those 81,402 patients, 13,019 were dq_flagged. The totchg could be null for multiple reasons such as a missing or zero charge.  
This table was then exported to R to calculate the average, standard deviation, and p-values (see charges_results.R). To find the sum charge over time and/or over databases, certain columns were summed over all rows (ie the 2013_sid row and the 2014_sid row were summed for each patient aka row). 

To calculate chg / visit, a separate table was created called hcup_totchgs (see hcup_chgs.sql) which contained columns for visitlink, dq_flag, and totchgs cols for each database. Rows where all totchgs were zero were omitted. The table was then exported to R to calculate the average, standard deviation, and p-values (see charges_results.R). Like before, certain columns were summed over all rows to find chg / visit over databases/time.

2 csv files were calculated for each sql table. One for results (avg, sd) and another for p-values. 

### CCS Level 1 Categories

#### Method of Calculation

The ICD codes were taken from the patients and then transformed into lvl 1 CCS categories. This was done in R. First, the data was exported from mySQL as a table containing visitlink and then 30 columns on ICD-9 codes; this was repeated for each database. 

Then, the raw data was processed in level1CCS_step1.R to create dx and pat tables. The dx tables contains 4 columns: visitlink, ICD-9 code, CCS level 1 categories, and dq_flag. To create this table, the raw data, a wide table, was gathered to create a long table. Then, this long table was joined with lookup tables for the ICD-9 --> CCS lvl 1 categories and visitlink --> dq_flag. The dx tables were exported as csvs. After initially manipulating the data, the pat table was generated which consists of 6 columns: CCS lvl, base population counts, CCS lvl, flagged population counts, CCS lvl, and remainder population counts. This table was generated by counting the number unique visitlinks by CCS for each population. The pat tables were exported as csvs.  

After preprocessing and initial analysis, level2CCS_step2.R calculates the p-values and writes them to a csv file. Further analysis methods can be incorparted in this file such as average number of patients per CCS category etc. 

### Comorbidities

#### Method of Calculation

The ICD codes were taken from patients and then used to calculate 2 forms of comorbidity scores: Charlson and Elixhauser. Calculating the comorbidity was done in R. First, the data was exported from mySQL as a table containing visitlink and then 30 columns on ICD-9 codes; this was repeated for each database. Comorbidities were calculated in steps in order to facilitate future analysis since generating the comorbidity tables took a long time. 

The raw data was processed similarly to CCS Level 1 Category calculation. comor_step1.R converts the data from wide format to long format containing only 2 columns: visitlink and ICD-9 code. The dx tables were then exported as csv files. 

After preprocessing, the comorbidity tables themselves were created in comor_step2.R. The comorbidity scores were calculated using the [comorbidity](https://cran.r-project.org/web/packages/comorbidity/comorbidity.pdf) package in R. The comorbidity tables were also merged with a data quality lookup table so flagged visitlinks could be found more easily with the inclusion of a dq_flag column in the final comorbidity table. 

Then, the comorbidity tables were analyzed for averages and standard deviations of various scores in comor_step3.R. For Elixhauser, the unweighted score, weighted score AHRQ, and weighted score VW were analyzed. For the Charlson tables, the unweighted score and weighted score were considered. The results were then exported to a csv. 

P-values were calculated in comor_step4.R. The comorbidity tables were read in and then the p-values were calculated for each statistic respective for each type of comorbidity. The p-values were then aggregated and exported to a csv. 


### Shiny App

The [shiny app](https://ashleyli44.shinyapps.io/venn_diagram/) visualizes the venn diagrams of completeness, fidelity, and plausibility. Users are able to select between various perspectives to analyze data quality such as completeness, fidelity and plausbility, and all of the above. After selecting the perspective, users then select at least 2 and up to 4 demographic variables for the venn diagram. This produces a venn diagram visualizing the interaction between data quality errors in different variables. 

The app uses the library [VennDiagram](https://cran.r-project.org/web/packages/VennDiagram/VennDiagram.pdf) to generate the figures and [grid](https://www.rdocumentation.org/packages/grid/versions/3.6.0) to visualize. Area overlaps were calculated based on a csv exported from mySQL which contains the columns for visitlink and all the possible data quality errors across all databases (inpatient, outpatient, and emergency) for 2013 - 2014. 

First, the app generates the sets based on the user input of type and demographic variables. After a list of sets is generated, the venn diagram plot is generated based on the number of sets. The plot is then stored in an output variable and displayed. See the folder venn_diagram for details. 

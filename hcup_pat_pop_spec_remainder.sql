#population spec of the remainder population, any patient with 0 flags for any variable
CREATE TABLE `hcup_pat_pop_spec_remainder` 
(
  `visitlink` varchar(16) DEFAULT NULL,
  
  `avg_age` int(3) DEFAULT NULL,
  `std_age` decimal(5,2) DEFAULT NULL,

  `race` varchar(1) DEFAULT NULL,

  `sex` varchar(1) DEFAULT NULL,
  `marital_status` varchar (1) DEFAULT NULL,

  UNIQUE KEY `visitlink_UNIQUE` (`visitlink`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#inserting visitlinks
INSERT 	INTO hcup_pat_pop_spec_remainder (visitlink)
SELECT 	visitlink 
FROM 	dq_master
WHERE 	maritalFlag = 0 AND raceFlag = 0 AND sexFlag = 0 AND zipFlag = 0 AND ageFlag = 0; -- 314 sec

UPDATE hcup_pat_pop_spec_remainder
INNER JOIN (
	SELECT * 
	FROM hcup_pat_pop_spec_all
	)t
ON hcup_pat_pop_spec_remainder.visitlink = t.visitlink
OR hcup_pat_pop_spec_remainder.visitlink IS NULL AND t.visitlink IS NULL
SET hcup_pat_pop_spec_remainder.avg_age = t.avg_age,
	hcup_pat_pop_spec_remainder.std_age = t.std_age,
	hcup_pat_pop_spec_remainder.race = t.race,
	hcup_pat_pop_spec_remainder.sex = t.sex,
	hcup_pat_pop_spec_remainder.marital_status = t.marital_status;
